package com.itheima.reggie_take_out.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie_take_out.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 林继源
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
