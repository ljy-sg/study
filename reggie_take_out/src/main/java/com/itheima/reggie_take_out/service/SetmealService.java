package com.itheima.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie_take_out.dto.SetmealDto;
import com.itheima.reggie_take_out.entity.Setmeal;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 林继源
 */
@Service
public interface SetmealService extends IService<Setmeal> {

    /**
     * 新增套餐，需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    public void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联信息
     * @param ids
     */
    public void removeWithDish(List<Long> ids);

}
