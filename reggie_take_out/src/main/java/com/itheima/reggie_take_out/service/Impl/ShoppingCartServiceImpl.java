package com.itheima.reggie_take_out.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie_take_out.entity.ShoppingCart;
import com.itheima.reggie_take_out.mapper.ShoppingCartMapper;
import com.itheima.reggie_take_out.service.ShoppingCartService;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
}
