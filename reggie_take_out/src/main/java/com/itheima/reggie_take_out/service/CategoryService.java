package com.itheima.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie_take_out.entity.Category;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public interface CategoryService extends IService<Category> {

    /**
     * 根据id删除
     * @param id
     */
    public void remove(Long id);
}
