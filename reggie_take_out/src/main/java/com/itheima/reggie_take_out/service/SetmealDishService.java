package com.itheima.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie_take_out.entity.Employee;
import com.itheima.reggie_take_out.entity.SetmealDish;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public interface SetmealDishService extends IService<SetmealDish> {
}
