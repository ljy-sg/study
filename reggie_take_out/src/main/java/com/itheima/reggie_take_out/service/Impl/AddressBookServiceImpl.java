package com.itheima.reggie_take_out.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie_take_out.entity.AddressBook;
import com.itheima.reggie_take_out.mapper.AddressBookMapper;
import com.itheima.reggie_take_out.service.AddressBookService;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}
