package com.itheima.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie_take_out.entity.AddressBook;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public interface AddressBookService extends IService<AddressBook> {
}
