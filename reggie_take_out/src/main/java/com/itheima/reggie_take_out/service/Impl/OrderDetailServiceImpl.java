package com.itheima.reggie_take_out.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie_take_out.entity.OrderDetail;
import com.itheima.reggie_take_out.mapper.OrderDetailMapper;
import com.itheima.reggie_take_out.service.OrderDetailService;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
}
