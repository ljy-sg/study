package com.itheima.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie_take_out.dto.DishDTO;
import com.itheima.reggie_take_out.entity.Dish;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public interface DishService extends IService<Dish> {

    /**
     * 新增菜品，同时插入菜品对应的口味数据，需要操作两张表：dish、dish_flavor
     */
    public void saveWithFlavor(DishDTO dishDTO);


    /**
     * 根据id查询菜品信息和对应的口味信息
     * @return
     */
    public DishDTO getByWithFlavor(Long id);

    /**
     * 更新菜品信息
     * @param dishDTO
     */
    void updateWithFlavor(DishDTO dishDTO);
}
