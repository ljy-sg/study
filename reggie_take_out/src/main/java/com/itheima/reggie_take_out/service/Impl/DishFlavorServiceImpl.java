package com.itheima.reggie_take_out.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie_take_out.entity.DishFlavor;
import com.itheima.reggie_take_out.mapper.DishFlavorMapper;
import com.itheima.reggie_take_out.service.DishFlavorService;
import org.springframework.stereotype.Service;

/**
 * @author 林继源
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
