package com.itheima.reggie_take_out;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

/**
 * @author 林继源
 * 使用jedis操作Redis
 */
public class JedisTest {

    @Test
    public void testRedis(){
        //1.获取连接
        Jedis jedis = new Jedis("localhost",6379);

        //2 执行具体操作
        jedis.set("username","xiaoming");

        //3 关闭连接
        jedis.close();
    }

}
