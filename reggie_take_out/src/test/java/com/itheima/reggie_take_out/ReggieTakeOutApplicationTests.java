package com.itheima.reggie_take_out;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
class ReggieTakeOutApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    public void testString(){
        redisTemplate.opsForValue().set("city","beijing");
        String city = redisTemplate.opsForValue().get("city");

        System.out.println(city);
        redisTemplate.opsForValue().set("key1","value1",10l, TimeUnit.SECONDS);

        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent("city", "nanjing");
        System.out.println(aBoolean);

    }


    @Test
    public void testHash(){
        //存值
        redisTemplate.opsForHash().put("001","name","李华");
        redisTemplate.opsForHash().put("001","age","20");
        redisTemplate.opsForHash().put("001","addr","beijing");


        //取值
        String name = (String) redisTemplate.opsForHash().get("001", "name");
        System.out.println(name);

        //获得哈希结构中所有字段
        Set<Object> keys = redisTemplate.opsForHash().keys("001");

        for (Object key : keys) {
            System.out.println(key);
        }

        //获得hash结构中的所有值
        List<Object> values = redisTemplate.opsForHash().values("001");

        for (Object value : values) {
            System.out.println(value);
        }
    }


    /**
     * 操作List类型的数据
     */
    @Test
    public void testList(){
        ListOperations<String, String> listOperations = redisTemplate.opsForList();

        //存值
        listOperations.leftPush("myList","a");
        listOperations.leftPushAll("myList","b","c","d");

        //取值
        List<String> myList = listOperations.range("myList", 0, -1);

        for (String s : myList) {
            System.out.println(s);
        }

        //获得列表长度
        Long size = listOperations.size("myList");
        int lSize = size.intValue();

        for (int i =0 ; i <size;i++){
            //出队列
            String element = listOperations.rightPop("myList");
            System.out.println(element);
        }
    }

    /**
     * 操作set类型的数据
     */
    @Test
    public void testSet(){
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();

        //存值
        setOperations.add("myset","a","b","c","a");

        //取值
        Set<String> myset = setOperations.members("myset");
        for (String s : myset) {
            System.out.println(s);
        }

        //删除成员
        setOperations.remove("myset","a","b");

        myset = setOperations.members("myset");
        for (String s : myset) {
            System.out.println(s);
        }
    }


    /**
     * 操作ZSet类型的数据
     */
    @Test
    public void testZSet(){
        ZSetOperations<String, String> setOperations = redisTemplate.opsForZSet();

        //存值
        setOperations.add("myZSet","a",10.0);
        setOperations.add("myZSet","b",11.0);
        setOperations.add("myZSet","c",12.0);
        setOperations.add("myZSet","a",13.0);


        //取值
        Set<String> myZSet = setOperations.range("myZSet", 0, -1);
        for (String s : myZSet) {
            System.out.println(s);
        }
        //修改分数
        setOperations.incrementScore("myZSet","b",20.0);

        //取值
        myZSet = setOperations.range("myZSet", 0, -1);
        for (String s : myZSet) {
            System.out.println(s);
        }


        //删除成员
        setOperations.remove("myZSet","a","b");

        myZSet = setOperations.range("myZSet", 0, -1);
        for (String s : myZSet) {
            System.out.println(s);
        }

    }


    /**
     * 通用操作，针对不同的数据类型都可以操作
     */
    @Test
    public void testCommon(){
        //获取Redis中所有的key
        Set<String> keys = redisTemplate.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
        //判断某个key是否存在
        Boolean itcast = redisTemplate.hasKey("itcast");
        System.out.println(itcast);

        //删除指定key
        redisTemplate.delete("myZSet6");

        //获取指定key对应的value的数据类型
        DataType dataType = redisTemplate.type("myset");
        System.out.println(dataType.name());
    }

}
