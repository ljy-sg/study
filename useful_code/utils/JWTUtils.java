package com.itheima.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class JWTUtils {

    private static String signKey = "itheima";
    private static Long expire = 43200000L;

    /**
     * 生成jwt令牌
     */
    public static String generateJwt(Map<String,Object> claims){
        String jwt = Jwts.builder()
                //自定义内容（载荷）
                .setClaims(claims)
                //签名算法
                .signWith(SignatureAlgorithm.HS256,signKey)
                //设置有效期为1h
                .setExpiration(new Date(System.currentTimeMillis()+expire))
                .compact();
        return jwt;
    }

    /**
     * 解析JWT令牌
     * @param jwt
     * @return
     */
    public static Claims parseJWT(String jwt){
        Claims claims = Jwts.parser()
                .setSigningKey("itheima")
                .parseClaimsJws("eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNjg0NTYxMjQ1LCJ1c2VybmFtZSI6IlRvbSJ9.yQiHVAu8eovlMo6W0sd4h8H1n62NQL60mDlz2X2a23E")
                .getBody();
        return claims;
    }
}
