# 1 软件开发整体介绍

## 1.1 软件开发流程

* 需求分析：产品原型、需求规格说明书
* 设计：产品文档、UI界面设计、概要设计、详细设计、数据库设计
* 编码：项目代码、单元测试
* 测试：测试用例、测试报告
* 上线运维：软件环境安装、配置



## 1.2 角色分工

* 项目经理:对整个项目负责，任务分配、把控进度
* 产品经理:进行需求调研，输出需求调研文档、产品原型等
* UI设计师: 根据产品原型输出界面效果图
* 架构师:项目整体架构设计、技术选型等
* 开发工程师:代码实现
* 测试工程师:编写测试用例，输出测试报告
* 运维工程师:软件环境搭建、项目上线



## 1.3 软件环境

* 开发环境(development): 开发人员在开发阶段使用的环境，一般外部用户无法访问
* 测试环境(testing): 专门给测试人员使用的环境，用于测试项目，一般外部用户无法访问
* 生产环境(production):即线上环境，正式提供对外服务的环境



# 2 瑞吉外卖项目整体介绍

## 2.1 项目介绍

本项目(瑞吉外卖)是专门为餐饮企业(餐厅、饭店)定制的一款软件产品，包括系统管理后台和移动端应用两部分其中系统管理后台主要提供给餐饮企业内部员工使用，可以对餐厅的菜品、套餐、订单等进行管理维护。移动端应用主要提供给消费者使用，可以在线浏览菜品、添加购物车、下单等。



本项目共分为3期进行开发：

* 第一期主要实现基本需求，其中移动端应用通过H5实现，用户可以通过手机浏览器访问

* 第二期主要针对移动端应用进行改进，使用微信小程序实现，用户使用起来更加方便

* 第三期主要针对系统进行优化升级，提高系统的访问性能。



## 2.2 产品原型展示

产品原型，就是一款产品成型之前的一个简单的框架，就是将页面的排版布局展现出来，使产品的初步构思有一个可视化的展示。通过原型展示，可以更加直观的了解项目的需求和提供的功能。



* 注意：产品原型主要用于展示项目的功能，并不是最终的页面效果



## 2.3 技术选型

![image-20230602221307517](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230602221307517.png)



## 2.4 功能架构

![image-20230602221544135](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230602221544135.png)



## 2.5 角色

* 后台系统管理员:登录后台管理系统，拥有后台系统中的所有操作权限
* 后台系统普通员工:登录后台管理系统，对菜品、套餐、订单等进行管理
* C端用户:登录移动端应用，可以浏览菜品、添加购物车、设置地址、在线下单等



# 3 开发环境搭建

## 3.1 数据库环境搭建

注意：

* 导入表结构，既可以使用上面的图形界面，也可以使用MySOL命令：source + sql文件路径
* 通过命令导入表结构时，注意sql文件不要放在中文目录中



## 3.2 maven项目搭建





# 4 后台登录功能开发

## 4.1 需求分析

处理逻辑如下:

1. 将页面提交的密码password进行md5加密处理
2. 根据页面提交的用户名username查询数据库
3. 如果没有查询到则返回登录失败结果
4. 密码比对，如果不一致则返回登录失败结果
5. 查看员工状态，如果为已禁用状态，则返回员工已禁用结果
6. 登录成功，将员工id存入Session并返回登录成功结果

![image-20230603144951642](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230603144951642.png)



## 4.2 代码开发

```java
@PostMapping("/login")
public R<Employee> login(@RequestBody Employee employee,HttpServletRequest request){

    /**
     * 1. 将页面提交的密码password进行md5加密处理
     * 2. 根据页面提交的用户名username查询数据库
     * 3. 如果没有查询到则返回登录失败结果
     * 4. 密码比对，如果不一致则返回登录失败结果
     * 5. 查看员工状态，如果为已禁用状态，则返回员工已禁用结果
     * 6. 登录成功，将员工id存入Session并返回登录成功结果
     */

    //1. 将页面提交的密码password进行md5加密处理
    String password = employee.getPassword();
    password = DigestUtils.md5DigestAsHex(password.getBytes());

    //2. 根据页面提交的用户名username查询数据库
    LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper.eq(Employee::getUsername,employee.getUsername());
    Employee emp = employeeService.getOne(queryWrapper);

    //3. 如果没有查询到则返回登录失败结果
    if (emp == null){
        return R.error("登录失败");
    }

    //4. 密码比对，如果不一致则返回登录失败结果
    if (!password.equals(emp.getPassword())){
        return R.error("登录失败");
    }

    //5. 查看员工状态，如果为已禁用状态，则返回员工已禁用结果
    if (emp.getStatus() == 0){
        return R.error("账号已禁用");
    }

    //6. 登录成功，将员工id存入Session并返回登录成功结果
    request.getSession().setAttribute("employee",emp.getId());
    return R.success(emp);
}
```

## 4.3 功能测试









# 5 后台退出功能开发





## 5.2 代码开发

用户点击页面中退出按钮，发送请求，请求地址为/employee/logout，请求方式为POST。我们只需要在Controller中创建对应的处理方法即可，具体的处理逻辑:

1. 清理Session中的用户id
2. 返回结果



# 6 员工管理业务开发

## 6.1 完善登录功能

### 6.1.1 问题分析

前面我们已经完成了后台系统的员工登录功能开发，但是还存在一个问题:用户如果不登录，直接访问系统首页面，照样可以正常访问。

这种设计并不合理，我们希望看到的效果应该是，只有登录成功后才可以访问系统中的页面，如果没有登录则跳转到登录页面。

那么，具体应该怎么实现呢?
答案就是使用过滤器或者拦截器，在过滤器或者拦截器中判断用户是否已经完成登录，如果没有登录则跳转到登录页面





### 6.1.2 代码实现

实现步骤:
1、创建自定义过滤器LoginCheckFilter

2、在启动类上加入注解@ServletComponentScan

3、完善过滤器的处理逻辑



过滤器具体的处理逻辑如下:
1、获取本次请求的URI

2、判断本次请求是否需要处理

3、如果不需要处理，则直接放行

4、判断登录状态，如果已登录，则直接放行

5、如果未登录则返回未登录结果



## 6.2 新增员工

### 6.2.1 需求分析

后台系统中可以管理员工信息，通过新增员工来添加后台系统用户。点击[添加员工]按钮跳转到新增页面



### 6.2.2 数据模型

新增员工，其实就是将我们新增页面录入的员工数据插入到emplovee表。需要注意，emplovee表中对username字段加入了唯一约束，因为username是员工的登录账号，必须是唯一的



### 6.2.3 代码开发

前面的程序还存在一个问题，就是当我们在新增员工时输入的账号已经存在，由于employee表中对该字段加入了唯一约束，此时程序会抛出异常:
java. sql.SQLIntegrityConstraintViolationException: Duplicate entry 'zhangsan' for key 'idx_username'



此时需要我们的程序进行异常捕获，通常有两种处理方式:

1、在Controller方法中加入try、catch进行异常捕获

2、使用异常处理器进行全局异常捕获



# 7 员工信息分页查询

## 7.1 需求分析

系统中的员工很多的时候，如果在一个页面中全部展示出来会显得比较乱，不便于查看，所以一般的系统中都会以分页的方式来展示列表数据



## 7.2 代码开发

在开发代码之前，需要梳理一下整个程序的执行过程
1、页面发送ajax请求，将分页查询参数(page、pageSize、name)提交到服务端2、服务端Controller接收页面提交的数据并调用Service查询数据
3、Service调用Mapper操作数据库，查询分页数据
4、Controller将查询到的分页数据响应给页面
5、页面接收到分页数据并通过ElementUl的Table组件展示到页面上





# 8 启用/禁用员工账号

## 8.1 需求分析、

在员工管理列表页面，可以对某个员工账号进行启用或者禁用操作。账号禁用的员工不能登录系统，启用后的员工可以正常登录。
需要注意，只有管理员(admin用户)可以对其他普通用户进行启用、禁用操作，所以普通用户登录系统后启用禁用按钮不显示。



## 8.2 代码开发

页面中是怎么做到只有管理员admin能够看到启用、禁用按钮的？

![image-20230617200637064](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230617200637064.png)



在开发代码之前，需要梳理一下整个程序的执行过程：

1、页面发送ajax请求，将参数(id、status)提交到服务端

2、服务端Controller接收页面提交的数据并调用Service更新数据

3、Service调用Mapper操作数据库



启用、禁用员工账号，本质上就是一个更新操作，也就是对status状态字段进行操作在Controller中创建update方法，此方法是一个通用的修改员工信息的方法



## 8.3 功能测试

测试过程中没有报错，但是功能并没有实现，查看数据库中的数据也没有变化。观察控制台输出的SQL:

==>Preparing: UPDATE employee SET status=?, update_time=?, update_user=? WHERE id=?

==>Parameters: 0(Integer)，2021-06-23T14:15:57.031(LocalDateTime)，1(Long)，1391586184024604700(Long)

<==Updates：0



前面我们已经发现了问题的原因，即js对long型数据进行处理时丢失精度导致提交的id和数据库中的id不一致。

如何解决这个问题?
我们可以在服务端给页面响应ison数据时进行处理，将long型数据统一转为String字符串



具体实现步骤:
1)提供对象转换器Jackson0bjectMapper，基于Jackson进行Java对象到json数据的转换 (资料中已经提供，直接复制到
项目中使用)
2)在WebMvcConfig配置类中扩展Spring mvc的消息转换器，在此消息转换器中使用提供的对象转换器进行Java对象到json数据的转换



## 8.4 代码修复





# 9 编辑员工信息

## 9.1 需求分析

在员工管理列表页面点击编辑按钮，跳转到编辑页面，在编辑页面回显员工信息并进行修改，最后点击保存按钮完成编辑操作



## 9.2 代码开发

在开发代码之前需要梳理一下操作过程和对应的程序的执行流程:

1、点击编辑按钮时，页面跳转到add.html，并在ur[中携带参数[员工id]

2、在add.html页面获取url中的参数[员工id]
3、发送ajax请求，请求服务端，同时提交员工id参数
4、服务端接收请求，根据员工id查询员工信息，将员工信息以ison形式响应给页面

5、页面接收服务端响应的ison数据，通过VUE的数据绑定进行员工信息回显

6、点击保存按钮，发送ajax请求，将页面中的员工信息以ison方式提交给服务端

7、服务端接收员工信息，并进行处理，完成后给页面响应
8、页面接收到服务端响应信息后进行相应处理



注意: add.html页面为公共页面，新增员工和编辑员工都是在此页面操作





# 10 分类管理业务开发

## 10.1 公共字段自动填充

### 10.1.1 问题分析

前面我们已经完成了后台系统的员工管理功能开发，在新增员工时需要设置创建时间、创建人、修改时间、修改人等字段，在编辑员工时需要设置修改时间和修改人等字段。这些字段属于公共字段，也就是很多表中都有这些字段，如下:

![image-20230618133350609](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230618133350609.png)

能不能对于这些公共字段在某个地方统一处理，来简化开发呢?答案就是使用Mybatis plus提供的公共字段自动填充功能



### 10.1.2 代码实现

Mybatis Plus公共字段自动填充，也就是在插入或者更新的时候为指定字段赋予指定的值，使用它的好处就是可以统一对这些字段进行处理，避免了重复代码。



实现步骤:
1、在实体类的属性上加入@TableField注解，指定自动填充的策略
2、按照框架要求编写元数据对象处理器，在此类中统一为公共字段赋值，此类需要实现MetaobiectHandler接口



### 10.1.3 功能测试



### 10.1.4 功能完善

前面我们已经完成了公共字段自动填充功能的代码开发，但是还有一个问题没有解决，就是我们在自动填充createuser和updateUser时设置的用户id是固定值，现在我们需要改造成动态获取当前登录用户的id。有的同学可能想到，用户登录成功后我们将用户id存入了HttpSession中，现在我从HttpSession中获取不就行了?注意，我们在MyMetaobjectHandler类中是不能获得HttpSession对象的，所以我们需要通过其他方式来获取登录用户id.

可以使用ThreadLocal来解决此问题，它是DK中提供的一个类



在学习ThreadLocal之前，我们需要先确认一个事情，就是客户端发送的每次http请求，对应的在服务端都会分配一个新的线程来处理，在处理过程中涉及到下面类中的方法都属于相同的一个线程
1、LoginCheckFilter的doFilter方法
2、EmploveeController的update方法
3、MyMetaObjectHandler的updateFill方法
可以在上面的三个方法中分别加入下面代码 (获取当前线程id) :

```
long id = Thread. currentThread().getId() :
log.info("线程id:{}",id);
```

执行编辑员工功能进行验证，通过观察控制台输出可以发现，一次请求对应的线程id是相同的:

```
2021-06-2815:36:00.708 INF011972---[nio-8080-exec-7] c.i.reggie.filter.LoginCheckFilter:线程id:312021-06-28 15:36:00.711INF0 11972 --- [nio-8080-exec-7] c.i.r.controller.EmployeeController:线程id:31: Employee(id=14078983:2021-06-28 15:36:00,711 INFO 11972 --- nio-8080-exec-7] c.i.r.controller.EmployeeController
Creating a new SalSession
SqlSession [org. apache. ibatis. session. defaults.DefaultSqlSession017f9al54] was not registered for synchronization because2021-06-28 15:36:00.715 INF0 11972 --- [nio-8080-exec-7) c.i.reggie.common.Myleta0bjectHandler : 线程id:31
```



什么是ThreadLocal?
ThreadLocal并不是一个Thread，而是Thread的局部变量。当使用ThreadLocal维护变量时，ThreadLocal为每个使用该变量的线程提供独立的变量副本，所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本。ThreadLocal为每个线程提供单独一份存储空间，具有线程隔离的效果，只有在线程内才能获取到对应的值，线程外则不能访问。

ThreadLocal常用方法:

* public void set(T value))设置当前线程的线程局部变量的值
* public T get()    返回当前线程所对应的线程局部变量的值



我们可以在LoginCheckFilter的doFilter方法中获取当前登录用户id，并调用ThreadLocal的set方法来设置当前线程的线程局部变量的值(用户id)，然后在MyMeta0bjectHandler的updateFil方法中调用ThreadLocal的get方法来获得当前线程所对应的线程局部变量的值 (用户id)。



实现步骤
1、编写BaseContext工具类，基于ThreadLocal封装的工具类

2、在LoginCheckFilter的doFilter方法中调用BaseContext来设置当前登录用户的id

3、在MyMetaObjectHandler的方法中调用BaseContext获取登录用户的id



## 10.2 新增分类

### 10.2.1 需求分析

后台系统中可以管理分类信息，分类包括两种类型，分别是菜品分类和套餐分类。当我们在后台系统中添加菜品时需要选择一个菜品分类，当我们在后台系统中添加一个套餐时需要选择一个套餐分类，在移动端也会按照菜品分类和套餐分类来展示对应的菜品和套餐。



可以在后台系统的分类管理页面分别添加菜品分类和套餐分类，如下：



### 10.2.2 数据模型





### 10.2.3 代码开发

在开发业务功能前，先将需要用到的类和接口基本结构创建好

* 实体类Category(直接从课程资料中导入即可)
* Mapper接口CategoryMapper
* 业务层接口CategoryService
* 业务层实现类CategoryServicelmpl
* 控制层CategoryController





在开发代码之前，需要梳理一下整个程序的执行过程:
1、页面(backend/page/category/list.html)发送ajax请求，将新增分类窗口输入的数据以json形式提交到服务端

2、服务端Controller接收页面提交的数据并调用Service将数据进行保存
3、Service调用Mapper操作数据库，保存数据



## 10.3 分类信息分页查询

### 10.3.1 需求分析



### 10.3.2 代码开发

在开发代码之前，需要梳理一下整个程序的执行过程:
1、页面发送ajax请求，将分页查询参数(page、pageSize)提交到服务端

2、服务端Controller接收页面提交的数据并调用Service查询数据

3、Service调用Mapper操作数据库，查询分页数据
4、Controller将查询到的分页数据响应给页面
5、页面接收到分页数据并通过ElementUI的Table组件展示到页面上





## 10.4 删除分类

### 10.4.1 需求分析

在分类管理列表页面，可以对某个分类进行删除操作。需要注意的是当分类关联了菜品或者套餐时，此分类不允许删除





### 10.4.2 代码开发

在开发代码之前，需要梳理一下整个程序的执行过程

1、页面发送ajax请求，将参数(id)提交到服务端

2、服务端Controller接收页面提交的数据并调用Service删除数据

3、Service调用Mapper操作数据库



### 10.4.3 功能完善

前面我们已经实现了根据id删除分类的功能，但是并没有检查删除的分类是否关联了菜品或者套餐，所以我们需要进行功能完善。

要完善分类删除功能，需要先准备基础的类和接口:
1、实体类Dish和Setmeal (从课程资料中复制即可)
2、Mapper接口DishMapper和SetmealMapper
3、Service接口DishService和SetmealService
4、Service实现类DishServicelmpl和SetmealServicelmpl



创建remove方法：

1. 捕获不发删除的异常
2. 抛出业务异常给全局异常处理器，统一对异常进行处理

```
/**
 * 根据id删除分类，删除之前需要进行判断
 * @param id
 */
@Override
public void remove(Long id) {
    LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
    dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
    int count1 = dishService.count(dishLambdaQueryWrapper);

    //查询当前分类是否关联了菜品，如果已经关联，抛出一个业务异常
    if (count1 > 0){
        //已经关联菜品，抛出一个业务异常
        throw new CustomException("当前分类下，关联了菜品，不能删除");
    }

    //查询当前分类是否关联了套餐，如果已经关联，抛出业务异常
    LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
    setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId,id);
    int count2 = setmealService.count();

    if (count2 > 0){
        //已经关联套餐，抛出一个业务异常
        throw new CustomException("当前分类下关联了套餐，不能删除");
    }
    
    //正常删除分类
    super.removeById(id);
}
```



## 10.5 修改分类

### 10.5.1 需求分析

在分类管理列表页面点击修改按钮，弹出修改窗口，在修改窗口回显分类信息并进行修改，最后点击确定按钮完成修改操作



### 10.5.2 代码开发

```
/**
 * 根据id修改分类信息
 * @param category
 * @return
 */
@PutMapping
public R<String> update(@RequestBody Category category){
    log.info("修改分类信息:{}",category);

    categoryService.updateById(category);

    return R.success("修改分类信息成功");
}
```



# 11 菜品管理业务开发

## 11.1 文件上传下载

### 11.1.1 文件上传介绍

文件上传，也称为uload，是指将本地图片、视频、音频等文件上传到服务器上，可以供其他用户浏览或下载的过程。
文件上传在项目中应用非常广泛，我们经常发微博、发微信朋友圈都用到了文件上传功能。



文件上传时，对页面的form表单有如下要求:

* method="post"   		                        采用post方式提交数据
* enctype="multipart/form-data"       采用multipart格式上传文件
* type="file"                                            使用input的file控件上传



举例:
<form method="post" action="/common/upload" enctype="multipart/form-data"><input name="myFile" type="file"/>
<input type="submit" value="提交"/>
</form>



目前一些前端组件库也提供了相应的上传组件，但是底层原理还是基于form表单的文件上传。例如ElementUl中提供的upload上传组件:



服务端要接收客户端页面上传的文件，通常都会使用Apache的两个组件:

* commons-fileupload
* commons-io



Spring框架在spring-web包中对文件上传进行了封装，大大简化了服务端代码，我们只需要在Controller的方法中声明一个MultipartFile类型的参数即可接收上传的文件，例如:

![image-20230705203638419](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230705203638419.png)



### 11.1.2 文件下载介绍

文件下载，也称为download，是指将文件从服务器传输到本地计算机的过程

通过浏览器进行文件下载，通常有两种表现形式:

* 以附件形式下载，弹出保存对话框，将文件保存到指定磁盘目录
* 直接在浏览器中打开



通过浏览器进行文件下载，本质上就是服务端将文件以流的形式写回浏览器的过程。



### 11.1.3 文件上传代码实现

文件上传，页面端可以使用ElementUI提供的上传组件可以直接使用资料中提供的上传页面，位置: 资料/文件上传下载页面/upload.html

![image-20230706135417870](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230706135417870.png)





### 11.1.4 文件下载代码实现

```
/**
 * 文件下载
 * @param name
 * @param response
 */
@GetMapping("/download")
public void download(String name, HttpServletResponse response){
    //输入流，通过输入流读取文件内容
    try {
        FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));

        //输出流，通过输出流将文件写回浏览器
        ServletOutputStream outputStream = response.getOutputStream();

        response.setContentType("image/jpeg");

        int len = 0;
        byte[] bytes = new byte[1024];
        while ((len = fileInputStream.read(bytes)) != -1 ){
            outputStream.write(bytes,0,len);
            outputStream.flush();
        }

        outputStream.close();
        fileInputStream.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```



## 11.2 新增菜品 

### 11.2.1 需求分析

后台系统中可以管理菜品信息，通过新增功能来添加一个新的菜品，在添加菜品时需要选择当前菜品所属的菜品分类并且需要上传菜品图片，在移动端会按照菜品分类来展示对应的菜品信息。



### 11.2.2 数据模型

新增菜品，其实就是将新增页面录入的菜品信息插入到dish表，如果添加了口味做法，还需要向dish_flavor表插入数据。所以在新增菜品时，涉及到两个表:

* dish                         菜品表
* dish flavor              菜品口味表



### 11.2.3 代码开发

准备工作：

在开发业务功能前，先将需要用到的类和接口基本结构创建好:

* 实体类 DishFlavor (直接从课程资料中导入即可，Dish实体前面课程中已经导入过了)
* Mapper接口 DishFlavorMapper
* 业务层接口 DishFlavorService
* 业务层实现类 DishFlavorServicelmpl
* 控制层 DishController



在开发代码之前，需要梳理一下新增菜品时前端页面和服务端的交互过程:
1、页面(backend/page/food/add.html)发送ajax请求，请求服务端获取菜品分类数据并展示到下拉框中

2、页面发送请求进行图片上传，请求服务端将图片保存到服务器
3、页面发送请求进行图片下载，将上传的图片进行回显
4、点击保存按钮，发送ajax请求，将菜品相关数据以json形式提交到服务端

开发新增菜品功能，其实就是在服务端编写代码去处理前端页面发送的这4次请求即可。



## 11.3 菜品信息分页查询

### 11.3.1 需求分析

系统中的菜品数据很多的时候，如果在一个页面中全部展示出来会显得比较乱，不便于查看，所以一般的系统中都会以分页的方式来展示列表数据。





### 11.3.2 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下菜品分页查询时前端页面和服务端的交互过程:

1、页面(backend/page/food/list.html)发送ajax请求，将分页查询参数(page、pageSize、name)提交到服务端，获取分页数据
2、页面发送请求，请求服务端进行图片下载，用于页面图片展示

开发菜品信息分页查询功能，其实就是在服务端编写代码去处理前端页面发送的这2次请求即可。



## 11.4 修改菜品

### 11.4.1 需求分析

在菜品管理列表页面点击修改按钮，跳转到修改菜品页面，在修改页面回显菜品相关信息并进行修改，最后点击确定按钮完成修改操作



### 11.4.2 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下修改菜品时前端页面 (add.html)和服务端的交互过程

1、页面发送ajax请求，请求服务端获取分类数据，用于菜品分类下拉框中数据展示

2、页面发送ajax请求，请求服务端，根据id查询当前菜品信息，用于菜品信息回显

3、页面发送请求，请求服务端进行图片下载，用于页图片回显
4、点击保存按钮，页面发送ajax请求，将修改后的菜品相关数据以ison形式提交到服务端

开发修改菜品功能，其实就是在服务端编写代码去处理前端页面发送的这4次请求即可。



# 12 套餐管理业务功能开发

## 12.1 新增套餐

### 12.1.1 需求分析

套餐就是菜品的集合。
后台系统中可以管理套餐信息，通过新增套餐功能来添加一个新的套餐，在添加套餐时需要选择当前套餐所属的套餐分类和包含的菜品，并且需要上传套餐对应的图片，在移动端会按照套餐分类来展示对应的套餐。



### 12.1.2 数据模型

新增套餐，其实就是将新增页面录入的套餐信息插入到setmeal表，还需要向setmeal dish表插入套餐和菜品关联数据所以在新增套餐时，涉及到两个表:

* setmeal                      套餐表
* setmeal dish             套餐菜品关系表





### 12.1.3 代码开发

* 准备工作

在开发业务功能前，先将需要用到的类和接口基本结构创建好:

* 实体类 SetmealDish (直接从课程资料中导入即可，Setmeal实体前面课程中已经导入过了)
* DTO SetmealDto (直接从课程资料中导入即可)
* Mapper接口 SetmealDishMapper
* 业务层接口 SetmealDishService
* 业务层实现类 SetmealDishServicelmpl
* 控制层 SetmealController



* 梳理流程

在开发代码之前，需要梳理一下新增套餐时前端页面和服务端的交互过程:
1、页面(backend/page/combo/add.html)发送ajax请求，请求服务端获取套餐分类数据并展示到下拉框中

2、页面发送ajax请求，请求服务端获取菜品分类数据并展示到添加菜品窗口中

3、页面发送ajax请求，请求服务端，根据菜品分类查询对应的菜品数据并展示到添加菜品窗口中

4、页面发送请求进行图片上传，请求服务端将图片保存到服务器
5、页面发送请求进行图片下载，将上传的图片进行回显

6、点击保存按钮，发送ajax请求，将套餐相关数据以json形式提交到服务端
开发新增套餐功能，其实就是在服务端编写代码去处理前端页面发送的这6次请求即可。



## 12.2 套餐信息分页查询

### 12.2.1 需求分析

系统中的套餐数据很多的时候，如果在一个页面中全部展示出来会显得比较乱，不便于查看，所以一般的系统中
都会以分页的方式来展示列表数据。



### 12.2.2 梳理交互过程

在开发代码之前，需要梳理一下套餐分页查询时前端页面和服务端的交互过程:

1、页面(backend/page/combo/list.html)发送ajax请求，将分页查询参数(page、pageSizename)提交到服务端，获取分页数据
2、页面发送请求，请求服务端进行图片下载，用于页面图片展示

开发套餐信息分页查询功能，其实就是在服务端编写代码去处理前端页面发送的这2次请求即可，





## 12.3 删除套餐

### 12.3.1 需求分析

在套餐管理列表页面点击删除按钮，可以删除对应的套餐信息。也可以通过复选框选择多个套餐，点击批量删除按钮一次删除多个套餐。注意，对于状态为售卖中的套餐不能删除，需要先停售，然后才能删除。





### 12.3.2 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下删除套餐时前端页面和服务端的交互过程

1、删除单个套餐时，页面发送ajax请求，根据套餐id删除对应套餐

2、删除多个套餐时，页面发送ajax请求，根据提交的多个套餐id删除对应套餐

开发删除套餐功能，其实就是在服务端编写代码去处理前端页面发送的这2次请求即可。观察删除单个套餐和批量删除套餐的请求信息可以发现，两种请求的地址和请求方式都是相同的，不同的则是传递的id个数，所以在服务端可以提供一个方法来统一处理



# 13 手机验证码登录

## 13.1 短信发送

### 13.1.1 短信服务介绍

目前市面上有很多第三方提供的短信服务，这些第三方短信服务会和各个运营商(移动、联通、电信)对接，我们只需要注册成为会员并且按照提供的开发文档进行调用就可以发送短信。需要说明的是，这些短信服务一般都是收费服务。
常用短信服务:

* 阿里云
* 华为云
* 腾讯云
* 京东
* 梦网
* 乐信



### 13.1.2 阿里云短信服务

阿里云短信服务(Short Mesage Service) 是广大企业客户快速触达手机用户所优选使用的通信能力。调用API或用群发助手，即可发送验证码、通知类和营销类短信;国内验证短信秒级触达，到达率最高可达99%，国际/港澳台短信覆盖200多个国家和地区，安全稳定，广受出海企业选用。

应用场景:

* 验证码
* 短信通知
* 推广短信



### 13.1.3 代码开发

导入maven坐标

```xml
<dependency>
    <groupId>com.aliyun</groupId>
    <artifactId>aliyun-java-sdk-core</artifactId>           <version>4.5.16</version>
</dependency>

<dependency>
<groupId>com.aliyun</groupId>
    <artifactId>aliyun-java-sdk-dysmsapi</artifactId>		<version>2.1.0</version>
 </dependency>
```

调用API

```
DefaultProfile profile = DefaultProfile,getProfile("cn-hangzhou", "<accessKeyId>", “<accessKeySecret>");
IAcsClient client = new DefaultAcsClient(profile);
SendSmsRequest request = new SendSmsRequest();
request.setSysRegionId("cn-hangzhou");
request.setPhoneNumbers(phoneNumbers);
request.setSignName(signName);
request.setTemplateCode(templateCode);request.setTemplateParam("{\"code\": ""+param+"\"}");
try {
SendSmsResponse response = client.getAcsResponse(request);
System.out.printIn("短信发送成功");
}catch (ClientException e) {
e.printStackTrace();
}
```





## 13.2 手机验证码登录

### 13.2.1 需求分析

为了方便用户登录，移动端通常都会提供通过手机验证码登录的功能

手机验证码登录的优点：

* 方便快捷，无需注册，直接登录

* 使用短信验证码作为登录凭证，无需记忆密码

* 安全

  登录流程：

  输入手机号>获取验证码>输入验证码>点击登录>登录成功

  注意:通过手机验证码登录，手机号是区分不同用户的标识。





### 13.2.2 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下登录时前端页面和服务端的交互过程:

1、在登录页面(front/page/login.htm)输入手机号，点击[获取验证码] 按钮，页面发送ajax请求，在服务端调用短信服务API给指定手机号发送验证码短信
2、在登录页面输入验证码，点击[登录] 按钮，发送ajax请求，在服务端处理登录请求



开发手机验证码登录功能，其实就是在服务端编写代码去处理前端页面发送的这2次请求即可。



* 准备工作

在开发业务功能前，先将需要用到的类和接口基本结构创建好

* 实体类  User (直接从课程资料中导入即可)
* Mapper接口 UserMapper
* 业务层接口 UserService
* 业务层实现类 UserServicelmpl
* 控制层 UserController
* 工具类SMSUtils、ValidateCodeUtils (直接从课程资料中导入即可)



# 14 菜品展示、购物车、下单

## 14.1 导入用户地址薄相关功能代码

### 14.1.1 需求分析

地址簿，指的是移动端消费者用户的地址信息，用户登录成功后可以维护自己的地址信息。同一个用户可以有多个地址信息，但是只能有一个**默认地址**。



### 14.1.2 数据模型

用户的地址信息会存储在address book表，即地址簿表中。具体表结构如下:

![image-20230714202156604](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230714202156604.png)





## 14.2 菜品展示

### 14.2.1 需求分析

用户登录成功后跳转到系统首页，在首页需要根据分类来展示菜品和套餐。如果菜品设置了口味信息，需要展示选择规格按钮，否则显示+ 按钮。



### 14.2.2 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下前端页面和服务端的交互过程:

1、页面(front/index.html)发送ajax请求，获取分类数据(菜品分类和套餐分类)

2、页面发送aiax请求，获取第一个分类下的菜品或者套餐开发菜品展示功能，其实就是在服务端编写代码去处理前端页面发送的这2次请求即可。



注意:首页加载完成后，还发送了一次ajax请求用于加载购物车数据，此处可以将这次请求的地址暂时修改一下，从静态ison文件获取数据，等后续开发购物车功能时再修改回来，如下:

**![image-20230715110515361](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230715110515361.png)**



## 14.3 购物车

### 14.3.1 需求分析

移动端用户可以将菜品或者套餐添加到购物车。对于菜品来说，如果设置了口味信息，则需要选择规格后才能加入购物车;对于套餐来说，可以直接点击  将当前套餐加入购物车。在购物车中可以修改菜品和套餐的数量也可以清空购物车。



### 14.3.2 数据模型

购物车对应的数据表shopping_cart表，具体表结构如下：

![image-20230715121918318](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230715121918318.png)



### 14.3.3 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下购物车操作时前端页面和服务端的交互过程:
1、点击加入购物车或者 + 按钮，页面发送ajax请求，请求服务端，将菜品或者套餐添加到购物车

2、点击购物车图标，页面发送ajax请求，请求服务端查询购物车中的菜品和套餐

3、点击清空购物车按钮，页面发送aiax请求，请求服务端来执行清空购物车操作



开发购物车功能，其实就是在服务端编写代码去处理前端页面发送的这3次请求即可。



## 14.4 下单

### 14.4.1 需求分析

移动端用户将菜品或者套餐加入购物车后，可以点击购物车中的按钮，页面跳转到订单确认页面，点击去支付则完成下单操作。



### 14.4.2 数据模型

用户下单业务对应的数据表为orders表和order_detail表:

* orders:订单表
* order detail:订单明细表



### 14.4.3 代码开发

* 梳理交互过程

在开发代码之前，需要梳理一下用户下单操作时前端页面和服务端的交互过程
1、在购物车中点击按钮，页面跳转到订单确认页面去结算
2、在订单确认页面发送aiax请求，请求服务端获取当前登录用户的默认地址

3、在订单确认页面，发送aiax请求，请求服务端获取当前登录用户的购物车数据

4、在订单确认页面点击按钮，发送ajax请求，请求服务端完成下单操作去支付



开发用户下单功能，其实就是在服务端编写代码去处理前端页面发送的请求即可。





# 15 项目优化

## 15.1 环境搭建

### 15.1.1 maven坐标

在项目的pom.xml文件中导入spring data redis的maven坐标

```
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```



### 15.1.2 配置文件

在项目的application.yml中加入redis相关配置
spring

​      redis :

​      host  : 172.17.2.94

​      port  :  6379

​      password : root@123456

​      database : 0



### 15.1.3 配置类

在项目中加入配置类RedisConfig

```java
@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {

        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();

        //默认的Key序列化器为：JdkSerializationRedisSerializer
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());

        redisTemplate.setConnectionFactory(connectionFactory);

        return redisTemplate;
    }
}
```



## 15.2 缓存短信验证码

### 15.2.1 实现思路

前面我们已经实现了移动端手机验证码登录，随机生成的验证码我们是保存在HttpSession中的。现在需要改造为将验证码缓存在Redis中，具体的实现思路如下:

1、在服务端UserController中注入RedisTemplate对象，用于操作Redis

2、在服务端UserController的sendMsg方法中，将随机生成的验证码缓存到Redis中，并设置有效期为5分钟

3、在服务端UserController的login方法中，从Redis中获取缓存的验证码，如果登录成功则删除Redis中的验证码



## 15.3 缓存菜品数据

### 15.3.1 实现思路

前面我们已经实现了移动端菜品查看功能，对应的服务端方法为DishController的list方法，此方法会根据前端提交的查询条件进行数据库查询操作。在高并发的情况下，频繁查询数据库会导致系统性能下降，服务端响应时间增长。现在需要对此方法进行缓存优化，提高系统的性能具体的实现思路如下:

1、改造DishController的list方法，先从Redis中获取菜品数据，如果有则直接返回，无需查询数据库;如果没有则查询数据库，并将查询到的菜品数据放入Redis。

2、改造DishController的save和update方法，加入清理缓存的逻辑



注意事项：在使用缓存过程中，要注意保证数据库中的数据和缓存中的数据一致，如果数据库中的数据发生变化，需要及时清理缓存数据





## 15.4 Spring Cache

### 15.4.1 Spring Cache 介绍

Spring Cache是一个框架，实现了基于注解的缓存功能，只需要简单地加一个注解，就能实现缓存功能。

Spring Cache提供了一层抽象，底层可以切换不同的cache实现。具体就是通过CacheManager接口来统一不同的缓存技术。

CacheManager是Spring提供的各种缓存技术抽象接口。

针对不同的缓存技术需要实现不同的CacheManager：

| CacheManager        | 描述                               |
| ------------------- | ---------------------------------- |
| EhCacheCacheManager | 使用EhCache作为缓存技术            |
| GuavaCacheManager   | 使用Google的GuavaCache作为缓存技术 |
| RedisCacheManager   | 使用Redis作为缓存技术              |



### 14.4.2 Spring Cache 常用注解

| 注解           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| @EnableCaching | 开启缓存注解功能                                             |
| @Cacheable     | 在方法执行前spring先查看缓存中是否有数据，如果有数据，则直接返回缓存数据若没有数据，调用方法并将方法返回值放到缓存中 |
| @CachePut      | 将方法的返回值放到缓存中                                     |
| @CacheEvict    | 将一条或多条数据从缓存中删除                                 |

在spring boot项目中，使用缓存技术只需在项目中导入相关缓存技术的依赖包，并在启动类上使用@EnableCaching开启缓存支持即可。
例如，使用Redis作为缓存技术，只需要导入Spring data Redis的maven坐标即可





### 14.4.3 Spring Cache 使用方式

在Spring Boot项目中使用Spring Cache的操作步骤(使用redis缓存技术)：
1、导入maven坐标
spring-boot-starter-data-redis、spring-boot-starter-cache

2、配置application.yml

spring:

​    cache:

​         redis:
​             time-to-live: 1800000.#设置缓存有效期

3、在启动类上加入@EnableCaching注解，开启缓存注解功能

4、在Controller的方法上加入@Cacheable、@CacheEvict等注解，进行缓存操作

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-cache</artifactId>
</dependency>
```



## 15.5 缓存套餐数据

### 15.5.1 实现思路

前面我们已经实现了移动端套餐查看功能，对应的服务端方法为SetmealController的list方法，此方法会根据前端提交的查询条件进行数据库查询操作。在高并发的情况下，频繁查询数据库会导致系统性能下降，服务端响应时间增长现在需要对此方法进行缓存优化，提高系统的性能。具体的实现思路如下：

1、导入Spring Cache和Redis相关maven坐标

2、在application.ym[中配置缓存数据的过期时间

3、在启动类上加入@EnableCaching注解，开启缓存注解功能

4、在SetmealController的list方法上加入@Cacheable注解

5、在SetmealController的save和delete方法上加入CacheEvict注解



# 16 读写分离

读和写所有压力都由一台数据库承担，压力大

数据库服务器磁盘损坏则数据丢失，单点故障

![image-20230723134033450](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230723134033450.png)

## 16.1 Mysql主从复制

### 16.1.1 介绍：

MySQL主从复制是一个异步的复制过程，底层是基于Mysql数据库自带的二进制日志功能。就是一台或多台MySOL数据库(slave，即从库)从另一台MySOL数据库(master，即主库)进行日志的复制然后再解析日志并应用到自身，最终实现从库的数据和主库的数据保持一致。MySOL主从复制是MySOL数据库自带功能，无需借助第三方工具。



MySOL复制过程分成三步:

* master将改变记录到二进制日志 (binary log)
* slave将master的binary log拷贝到它的中继日志 (relay log)
* slave重做中继日志中的事件，将改变应用到自己的数据库中

![image-20230723135106373](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230723135106373.png)





16.1.2 配置

* 前置条件

提前准备好两台服务器，分别安装Mysql并启动服务成功

* 主库Master 192.168.138.100
* 从库slave 192.168.138.101



* 主库Master

1. 修改Mysql数据库的配置文件/etc/my.cnf

   ```
   [mysqld]
   log-bin = mysql-bin   #[必须]启用二进制日志
   server-id=100         #[必须]服务器唯一ID
   ```

2. 重启MySQL服务

```
systemctl restart mysqld;
```

3. 登录MySQL数据库，执行下面SQL

```
GRANT REPLICATION SLAVE ON *.* to 'xiaoming'@'%' identified by 'Root@123456';
```

注：上面SOL的作用是创建一个用户xiaoming，密码为Root@123456，并且给xiaoming用户授予REPLICATION SLAVE权限。常用于建立复制时所需要用到的用户权限，也就是slave必须被master授权具有该权限的用户，才能通过该用户复制。

4. 登录MySQL，执行下面SQL，记录下结果中File和Position的值

```
show master status;
```

![image-20230723162713493](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230723162713493.png)

注：上面SQL的作用是查看Master的状态，执行完此SQL后不要再执行任何操作



* 从库

1. 修改MySQL数据库的配置文件/etc/my.cnf

```
server-id=101     #[必须]服务器唯一ID
```

2. 重启MySQL服务

```
systemctl restart mysqld;
```

3. 登录MySQL数据库，执行下面SQL

```
change master to
master_host='192.168.75.128' ,master_user='xiaoming',master_password='Root@123456',master_log_file='mysql-bin.000001',master_log_pos=441;
```

```
start slave;
```

![image-20230723170809120](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230723170809120.png)

```
show slave status;
```

注意看Slave_IO_Running 是否为 yes；以及Slave_SQL_Running 是否为yes



## 16.2 读写分离案例

### 16.2.1 背景

面对日益增加的系统访问量，数据库的吞吐量面临着巨大瓶颈。对于同一时刻有大量并发读操作和较少写操作类型的应用系统来说，将数据库拆分为主库和从库，主库负责处理事务性的增删改操作，从库负责处理查询操作，能够有效的避免由数据更新导致的行锁，使得整个系统的查询性能得到极大的改善。

![image-20230724144551831](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230724144551831.png)



### 16.2.2 Sharding-JDBC介绍

Sharding-DBC定位为轻量级Java框架，在Java的]DBC层提供的额外服务。它使用客户端直连数据库，以jar包形式提供服务，无需额外部署和依赖，可理解为增强版的]DBC驱动，完全兼容]DBC和各种ORM框架。使用Sharding-JDBC可以在程序中轻松的实现数据库读写分离。

* 适用于任何基于]DBC的ORM框架，如: JPA,Hibernate,Mybatis,SpringJDBCTemplate或直接使用JDBC。
* 支持任何第三方的数据库连接池，如: DBCP,C3PO,BoneCP,Druid,HikariCP等。
* 支持任意实现IDBC规范的数据库。目前支持MySOL，Oracle，SOLServer，PostgresQL以及任何遵循SQL92标准的数据库。

![image-20230724144821781](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230724144821781.png)





### 16.2.3 入门案例

使用Sharding-JDBC 实现读写分离步骤

1. 导入maven坐标
2. 在配置文件中配置读写分离规则
3. 在配置文件中配置允许bean定义覆盖配置项





## 16.3 项目实现读写分离

数据库环境准备（主从复制）

1. 在配置文件中配置读写分离规则
2. 在配置文件中配置允许bean定义覆盖配置项

```
<dependency>
	<groupId>org.apache.shardingsphere</groupId>
	<artifactId>sharding-jdbc-spring-boot-starter</artifactId>
	<version>4.0.0-RC1</version>
</dependency>
```



# 17 Niginx

## 17.1 Niginx概述

### 17.1.1 Nginx介绍

Nginx是一款轻量级的web 服务器/反向代理服务器及电子邮件(IMAP/POP3)代理服务器。其特点是占有内存少，并发能力强，事实上nginx的并发能力在同类型的网页服务器中表现较好，中国大陆使用nginx的网站有:百度、京东新浪、网易、腾讯、淘宝等。



Nginx是由伊戈尔·赛索耶夫为俄罗斯访河量第二的Rambler.ru站点(俄文: Pam6nep)开发的，第一个公开版本0.1.发布于2094年10月4日。



官网: https://nginx.org/



### 17.1.2 Nginx下载和安装

可以到Nginx官方网站下载Nginx的安装包，地址为: https://nginx.org/en/download.html





安装过程:
1、安装依赖包 yum -y install gcc pcre-devel zlib-devel openssl openssl-devel

2、下载Nginx安装包 wget https://nginx.org/download/nginx-1.16.1.tar.gz

3、解压 tar -zxvf nginx-1.16.1.tar.gz

4、cd nginx-1.16.1

5、./configure --prefix=/usr/local/nginx

6、make && make install



### 17.1.3 Nginx目录结构

安装完Nginx后，我们先来熟悉一下Nginx的目录结构
重点目录/文件：

* conf/nginx.conf                  nginx配置文件
* html                                     存放静态文件 (html、cSS、Js等)
* logs                                      日志目录，存放日志文件
* sbin/nginx                           二进制文件，用于启动、停止Nginx服务



## 17.2 Nginx命令

### 17.2.1 查看版本

查看Nginx版本可以使用命令：
./nginx -V

![image-20230726144614883](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726144614883.png)



### 17.2.2 检查配置文件正确性

在启动Nginx服务之前，可以先检查一下conf / nginx.conf 文件配置的是否有错误，命令如下：

./nginx -t

![image-20230726144814179](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726144814179.png)





### 17.2.3 启动和停止

* 启动Nginx服务使用如下命令：
  * ./nginx



* 停止Nginx服务使用如下命令：
  * ./nginx -s stop



* 启动完成后可以查看Nginx进程：
  * ps -ef| grep nginx



### 17.2.4 重新加载配置文件

当修改Nginx配置文件后，需要重新加载才能生效，可以使用下面命令重新加载配置文件：./nginx -s reload



## 17.3 Niginx配置文件结构

整体结构介绍：

Nginx配置文件(conf/ginx.conf)整体分为三部分：

* 全局块                           和Nginx运行相关的全局配置
* events块                       和网络连接相关的配置
* http块                            代理、缓存、日志记录、虚拟主机配置
  * http全局块
  * Server块
    * Server全局块
    * location块



注意: http块中可以配置多个Server块，每个Server块中可以配置多个location块。



### 17.3.1 全局块

```
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

pid        logs/nginx.pid;
```



### 17.3.2 Events块

```
events {
    worker_connections  1024;
}
```



### 17.3.3 Http块

```
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;
        server_name  localhost;

        location / {
            root   html;
            index  index.html index.htm;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
```





## 17.4 Niginx具体应用

### 17.4.1 部署静态资源

Nginx可以作为静态web服务器来部署静态资源。静态资源指在服务端真实存在并且能够直接展示的一些文件，比如常见的htm[页面、css文件、js文件、图片、视频等资源。相对于Tomcat，Nginx处理静态资源的能力更加高效，所以在生产环境下，一般都会将静态资源部署到Nginx中。将静态资源部署到Nginx非常简单，只需要将文件复制到Nginx安装目录下的html目录中即可。



server {
				listen 80:#监听端口
				server_name localhost; 服务器名称#匹配客户端请求url

​				location / {
​				root html;                    #指定静态资源根目录
​				index index.html；     #指定默认首页

}

}



### 17.4.2 反向代理

* 正向代理

是一个位于客户端和原始服务器(origin server)之间的服务器，为了从原始服务器取得内容，客户端向代理发送一个请求并指定目标(原始服务器)，然后代理向原始服务器转交请求并将获得的内容返回给客户端。正向代理的典型用途是为在防火墙内的局域网客户端提供访问Internet的途径。正向代理一般是在客户端设置代理服务器，通过代理服务器转发请求，最终访问到目标服务器



![image-20230726154938850](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726154938850.png)



* 反向代理

反向代理服务器位于用户与目标服务器之间，但是对于用户而言，反向代理服务器就相当于目标服务器，即用户直接访问反向代理服务器就可以获得目标服务器的资源，反向代理服务器负责将请求转发给目标服务器。用户不需要知道目标服务器的地址，也无须在用户端作任何设定

![image-20230726155429244](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726155429244.png)



* 配置反向代理
  	server{
  	listen 82;
  	server name localhost;
  	location / {
  	proxy_pass http://192.168.138.101:8080; #反向代理配置，将请求转发到指定服务

  }

}

![image-20230726155914362](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726155914362.png)



### 17.4.3 负载均衡

早期的网站流量和业务功能都比较简单，单台服务器就可以满足基本需求，但是随着互联网的发展，业务流量越来越大并且业务逻辑也越来越复杂，单台服务器的性能及单点故障问题就凸显出来了，因此需要多台服务器组成应用集群进行性能的水平扩展以及避免单点故障出现。

* 应用集群:将同一应用部署到多台机器上，组成应用集群，接收负载均衡器分发的请求，进行业务处理并返回响应数据
* 负载均衡器:将用户请求根据对应的负载均衡算法分发到应用集群中的一台服务器进行处理

![image-20230726161410610](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726161410610.png)



配置负载均衡
upstream targetserver{          #upstream指令可以定义一组服务器

​			server 192.168.138.101:8080;

​			server 192.168.138.101:8081;

}

server {
		listen
		8080;
		server_name localhost;
		location /{
			proxy_pass http://targetserver;

​	}

}



负载均衡策略:

| 名称       | 说明             |
| ---------- | ---------------- |
| 轮询       | 默认方式         |
| weight     | 权重方式         |
| ip_hash    | 依据ip分配方式   |
| least_conn | 依据最少连接方式 |
| url_hash   | 依据url分配方式  |
| fair       | 依据响应时间方式 |



# 18 前后端分离开发

* 开发人员同时负责前端和后端代码开发，分工不明确
* 开发效率低
* 前后端代码混合在一个工程中，不便于管理
* 对开发人员要求高，人员招聘困难



## 18.1 前后端分离开发

### 18.1.1 介绍

前后端分离开发，就是在项目开发过程中，对于前端代码的开发由专门的前端开发人员负责，后端代码则由后端开发人员负责，这样可以做到分工明确、各司其职，提高开发效率，前后端代码并行开发，可以加快项目开发进度。目前，前后端分离开发方式已经被越来越多的公司所采用，成为当前项目开发的主流开发方式。



前后端分离开发后，从工程结构上也会发生变化，即前后端代码不再混合在同一个maven工程中，而是分为前端工程和后端工程。

![image-20230726200828781](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726200828781.png)





### 18.1.2 开发流程

前后端分离开发后，面临一个问题，就是前端开发人员和后端开发人员如何进行配合来共同开发一个项目，可以按照如下流程进行：

![image-20230726201007680](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230726201007680.png)

接口（API接口）就是一个http的请求地址，主要就是去定义：请求路径、请求方式、请求参数、响应数据等内容



### 18.1.3 前端技术栈

* Visual Studio Code
* hbuider



技术框架

* nodejs
* VUE
* ElementUI
* mock
* webpack



## 18.2 Yapi

### 18.2.1 介绍

YApi 是高效、易用、功能强大的 api 管理平台，旨在为开发、产品、测试人员提供更优雅的接口管理服务。可以帮助开发者轻松创建、发布、维护 APL，YApi 还为用户提供了优秀的交互体验，开发人员只需利用平台提供的接口数据写入工具以及简单的点击操作就可以实现接口的管理。

YApi让接口开发更简单高效，让接口的管理更具可读性、可维护性，让团队协作更合理

源码地址: https://github.com/YMFE/yapi

要使用YApi，需要自己进行部署



### 18.2.2 使用

使用YApi，可以执行下面操作:

* 添加项目
* 添加分类
* 添加接口
* 编辑接口
* 查看接口



## 18.3 Swagger

### 18.3.1 介绍

使用Swagger你只需要按照它的规范去定义接口及接口相关的信息，再通过Swagger衍生出来的一系列项目和工具就可以做到生成各种格式的接口文档，以及在线接口调试页面等等。

官网: https://swagger.io/

knife4j是为Java MVC框架集成Swagger生成Api文档的增强解决方案

```
<dependency>
         <groupId>com.github.xiaoymin</groupId>
         <artifactId>knife4j-spring-boot-starter</artifactId>
        <version>3.0.2</version>
</dependency>
```

 



### 18.3.2 使用方式

操作步骤：
1、导入knife4j的maven坐标

```
<dependency>
         <groupId>com.github.xiaoymin</groupId>
         <artifactId>knife4j-spring-boot-starter</artifactId>
        <version>3.0.2</version>
</dependency>
```

2、导入knife4j相关配置类





3、设置静态资源映射 (WebMvcConfig类中的addResourceHandlers方法)，否则接口文档页面无法访问registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");

registry.addResourceHandler("/webjars/**")addResourceLocations("classpath:/META-INF/resources/webjars/");



4、在LoginCheckFilter中设置不需要处理的请求路径



### 18.3.3 常用注解

| 注解               | 说明                                                     |
| ------------------ | -------------------------------------------------------- |
| @Api               | 用在请求的类上，例如Controller，表示对类的说明           |
| @ApiModel          | 用在类上，通常是实体类，表示一个返回响应数据的信息       |
| @ApiModelProperty  | 用在属性上，描述响应类的属性                             |
| @ApiOperation      | 用在请求的方法上，说明方法的用途、作用                   |
| @ApilmplicitParams | 用在请求的方法上，表示一组参数说明                       |
| @ApilmplicitParam  | 用在@ApilmplicitParams注解中，指定一个请求参数的各个方面 |



## 18.4 项目部署

### 18.4.1 部署架构

![image-20230728130434248](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230728130434248.png)





### 18.4.2 部署环境说明

服务器：

* 192.168.138.100 (服务器A)
          Nginx: 部署前端项目、配置反向代理
          Mysql: 主从复制结构中的主库
* 192.168.138.101 (服务器B)
           jdk: 运行Java项目
           git:版本控制工具
           maven:项目构建工具
           jar: Spring Boot项目打成jar包基于内置Tomcat运行Mysql: 主从复制结构中的从库
* 172.17.2.94(服务器C)
           Redis: 缓存中间件



### 18.4.3 部署前端项目

第一步: 在服务器A中安装Nginx，将课程资料中的dist目录上传到Nginx的html目录下

第二步：修改Nginx配置文件nginx.conf





### 18.4.4 部署后端项目

第一步：在服务器B中安装jdk、git、maven、MySQL，使用git clone命令将git远程仓库的代码克隆下来



第二步：将资料中的reggieStart.sh文件上传到服务器B，通过chomd命令设置执行权限













