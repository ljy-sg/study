# Spring Security笔记

# 1 Spring Security框架简介1.1

## 1.1 概要

Spring 是非常流行和成功的Java 应用开发框架，Spring Security正是Spring 家族中的成员。Spring Security基于 Spring 框架，提供了一套 Web应用安全性的完整解决方案。



正如你可能知道的关于安全方面的两个主要区域是“认证”和“授权( 或者访问控制)，一般来说，Web 应用的安全性包括用户认证( Authentication)和用户授权( Authorization)两个部分，这两点也是 Spring Security 重要核心功能。



(1)用户认证指的是:验证某个用户是否为系统中的合法主体，也就是说用户能否访问该系统。用户认证一般要求用户提供用户名和密码。系统通过校验用户名和密码来完成认证过程。通俗点说就是系统认为用户是否能登录



(2)用户授权指的是验证某个用户是否有权限执行某个操作。在一个系统中，不同用户所具有的权限是不同的。比如对一个文件来说，有的用户只能进行读取，而有的用户可以进行修改。一般来说，系统会为不同的用户分配不同的角色，而每个角色则对应一系列的权限。通俗点讲就是系统判断用户是否有权限去做某些事情





## 1.2 历史

”Spring Security 开始于 2003 年年底““spring 的acegi安全系统”。起因是 Spring开发者邮件列表中的一个问题,有人提问是否考虑提供一个基于 spring 的安全实现。



Spring Security以“The AcegiSecutity System for Spring”的名字始于 2013 年晚些时候。一个问题提交到 Spring 开发者的邮件列表，询问是否已经有考虑一个机遇 Spring的安全性社区实现。那时候 Spring 的社区相对较小(相对现在)。实际上Spring 自己在2013 年只是一个存在于 ScourseForge 的项目，这个问题的回答是一个值得研究的领域，虽然目前时间的缺乏组织了我们对它的探索。



考虑到这一点，一个简单的安全实现建成但是并没有发布。几周后，Spring 社区的其他成员询问了安全性，这次这个代码被发送给他们。其他几个请求也跟随而来。到 2014 年一月大约有 20 万人使用了这个代码。这些创业者的人提出一个 SourceForge 项目加入是为了，这是在 2004 三月正式成立。



在早些时候，这个项目没有任何自己的验证模块，身份验证过程依赖于容器管理的安全性和Acegi.安全性。而不是专注于授权。开始的时候这很适合，但是越来越多的用户请求额外的容器支持。容器特定的认证领域接口的基本限制变得清晰。还有一个相关的问题增加新的容器的路径，这是最终用户的困惑和错误配置的常见问题



Acegi.安全特定的认证服务介绍。大约一年后，Acegi安全正式成为了 Spring 框架的子项目。1.0.0 最终版本是出版于 2006 -在超过两年半的大量生产的软件项目和数以百计的改进和积极利用社区的贡献。



Acegi安全2007 年底正式成为了 Spring 组合项目，更名为"Spring Security'



## 1.3 同款产品对比

### 1.3.1 Spring Security

Spring 技术栈的组成部分



SpringSecurity特点:

* 和 Spring 无缝整合
* 全面的权限控制
* 专门为 Web 开发而设计。
  * 旧版本不能脱离 Web 环境使用。
  * 新版本对整个框架进行了分层抽取，分成了核心模块和 Web 模块。单独引入核心模块就可以脱离 Web 环境。“
* 重量级。





### 1.3.2 Shiro

Apache 旗下的轻量级权限控制框架。



特点：

* 轻量级。Shiro 主张的理念是把复杂的事情变简单。针对对性能有更高要求的互联网应用有更好表现。
* 通用性。
  * 好处：不局限于 Web 环境，可以脱离 Web 环境使用。
  * 缺陷：在Web 环境下一些特定的需求需要手动编写代码定制。



Spring Security是 Spring 家族中的一个安全管理框架，实际上，在Spring Boot 出现之前，Spring Security 就已经发展了多年了，但是使用的并不多，安全管理这个领域，一直是 Shiro 的天下。

相对于Shiro，在SSM 中整合 Spring Security都是比较麻烦的操作，所以，SpringSecurity 虽然功能比 Shiro 强大，但是使用反而没有 Shiro 多( Shiro 虽然功能没有Spring Security多，但是对于大部分项目而言，Shiro 也够用了)。



自从有了 Spring Boot之后，Spring Boot 对于 Spring Security提供了自动化配置方案，可以使用更少的配置来使用 Spring Security。

因此，一般来说，常见的安全管理技术栈的组合是这样的 :“

* SSM + Shiro
* Spring Boot/Spring Cloud + Spring Security

以上只是一个推荐的组合而已，如果单纯从技术上来说，无论怎么组合，都是可以运行的。



# 2 Spring Security 的 hello world

## 2.1 创建一个项目

1、创建springboot工程

2、引入相关依赖

```
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

3、编写controller进行测试

默认用户名：user

密码再启动控制台找到



## 2.2 权限管理中的相关概念









## 2.3 添加一个控制器进行访问









## 2.4 Spring Security基本原理

Spring Security本质是一个过滤器链

从启动是可以获取到过滤器链



代码底层流程：重点看三个过滤器

* FilterSecurityInterceptor: 是一个方法级的权限过滤器,基本位于过滤链的最底部。



super.beforeInvocation(f)表示查看之前的 flter 是否通过

fi.getChain().doFilter(fi.getReguest(),fi.getResponse());表示真正的调用后台的服务。



* ExceptionTranslationFilter: 是个异常过滤器，用来处理在认证授权过程中抛出的异常
* UsernamePasswordAuthenticationFilter :对/login 的 POST请求做拦截，校验表单中用户名，密码。



过滤器如何进行加载的？

1、使用 SpringSecurity 配置过滤器

* DelegatingFilterProxy

![image-20230811120607254](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230811120607254.png)







## 2.5 UserDetailsService （查询数据库用户名和密码过程）讲解

当什么也没有配置的时候，账号和密码是由 Spring Security 定义生成的。而在实际项目中账号和密码都是从数据库中查询出来的。 所以我们要通过自定义逻辑控制认证逻辑。



UserDetailsService ：查询数据库用户名和密码过程

* 创建类继承UsernamePasswordAuthenticationFilter，重写三个方法
* 创建类实现UserDetailsService ，编写查询数据过程，返回User对象，这个User对象是安全框架提供对象



如果需要自定义逻辑时，只需要UserDetailsService接口即可，接口定义如下：

```
public interface UserDetailsService {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
```







* 返回值 UserDetails
* 方法参数 username









## 2.6 PasswordEncoder 接口讲解

PasswordEncoder ：数据加密接口，用于返回User对象里面密码加密



//表示把参数按照特定的解析规则进行解析

String encode(CharSequence rawPassword);



//表示验证从存储中获取的编码密码与编码后提交的原始密码是否匹配。如果密码匹配，则返回 true;如果不匹配，则返 false。第一个参数表示需要被解析的密码.第二个参数表示存储的密码。



booleanmatches(CharSequence encodedPassword，
String rawPassword)；
//表示如果解析的密码能够再次进行解析且达到更安全的结果则返回 true，否则返回 false。默认返回 false。









## 2.7 SpringBoot 对 Security 的自动配置









# 3 Spring Security Web权限方案

## 3.1 设置登录系统的账号、密码

方式一：在application.properties（通过配置文件）

```
spring.security.user.name=atguigu
spring.security.user.password=atguigu
```

方式二：通过配置类

```java
package com.atguigu.securitydemo1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author 林继源
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 注入PasswordEncoder 类到spring容器中
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = passwordEncoder.encode("123");
        auth.inMemoryAuthentication().withUser("lucy").password(password).roles("admin");
    }
}
```



方式三：自定义编写实现类

步骤：

* 创建配置类，设置使用哪个userDetailsService实现类
* 编写实现类，返回User对象，User对象有用户名密码和操作权限



## 3.2 实现数据库认证来完成用户登录

完成自定义登录

* 整合MyBatisPlus完成数据库操作
  * 引入相关依赖
* 创建数据库以及数据库表
* 创建user表对应实体类
* 整合map，创建接口，继承map的接口
* 在MyUserDetailsService调用mapper里面的方法查询数据库进行用户认证

```java
/**
 * @author 林继源
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //调用userMapper方法，根据用户名查询数据库
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        //where username=?
        wrapper.eq("username",username);
        Users users = userMapper.selectOne(wrapper);

        //判断
        if (users == null){
            //数据库没有用户名，认证失败
            throw new UsernameNotFoundException("用户名不存在！");
        }
        
        List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("role");

        return new User("mary",new BCryptPasswordEncoder().encode("123"),auths);
    }
}
```

* 在启动类添加注解MapperScan
* 配置数据库信息

```yml
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/itheima?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&useSSL=false&allowPublicKeyRetrieval=true
      username: root
      password: ljy040226
```





## 3.3 自定义设置登录页面

1、在配置类中实现相关的配置

```java
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http.formLogin()    //自定义自己编写的登录页面
                .loginPage("/login.html")   //登录页面设置    
                .loginProcessingUrl("/user/login")   //登录访问路径    
                .defaultSuccessUrl("/test/index").permitAll()    //登录成功之后，跳转路径    
                .and().authorizeHttpRequests()
                .antMatchers("/","/test/hello","/user/login").permitAll()    //设置哪些路径可以直接访问，不需要认证，//不需要认证可以访问
                .anyRequest().authenticated()
                .and().csrf().disable();   //关闭crsf防护
    }
```

2、创建登录页面，对应的controller





## 3.4 基于角色或权限进行访问控制

### 3.4.1 hasAuthority 方法

如果当前的主体具有指定的权限，则返回true，否则返回false

* 修改配置类

  * 在配置类设置当前访问地址有哪些权限

  ```java
  //当前登录用户。只有具有admins权限才可以访问这个路径
                  .antMatchers("/test/index").hasAuthority("admins")
  ```

  * 在UserDetailsService，把返回User对象设置权限

  ```java
  List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins");
  ```

* ```
  没有访问权限 403
  (type=Forbidden, status=403)
  ```

  

### 3.4.2 hasAnyAuthority方法

 如果当前的主体有任何提供的角色（给定的作为一个逗号分隔的字符串列表）的话，返回true

访问 http://localhost:8080/find

```java
             .antMatchers("/test/index").hasAnyAuthority("admins,manager")

                 
List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins");
```





### 3.4.3 hasRole 方法

如果用户具备给定角色就允许访问，否则出现403

如果当前主体具有指定的角色，则返回true

底层源码：

```java
    public static <T> AuthorityAuthorizationManager<T> hasRole(String role) {
        Assert.notNull(role, "role cannot be null");
        Assert.isTrue(!role.startsWith("ROLE_"), () -> {
            return role + " should not start with " + "ROLE_" + " since " + "ROLE_" + " is automatically prepended when using hasRole. Consider using hasAuthority instead.";
        });
        return hasAuthority("ROLE_" + role);
    }
```

修改配置文件：

注意配置文件中不需要添加"ROLE_"，因为上述的底层代码会自动添加与之进行匹配

```java
.antMatchers("/test/index").hasRole("sale")


List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins,ROLE_sale");
```



### 3.4.4 hasAnyRole 方法

表示用户具备任何一个条件都可以访问

给用户添加角色：

```java
.antMatchers("/test/index").hasAnyRole("sale,admins,manager")
```

修改配置文件：

```java
.antMatchers("/test/index").hasAnyRole("sale,admins,manager")

List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins,ROLE_sale");
```



## 3.5 自定义403没有权限访问的页面

* 在配置类中进行配置

```java
//配置没有权限访问跳转自定义页面      http.exceptionHandling().accessDeniedPage("/unauth.html");
```





## 3.6 注解使用

### 3.6.1 @Secured

判断是否具有角色，另外需要注意的是这里匹配的字符串需要添加前缀"ROLE_"

使用注解先要开启注解功能

1、启动类（配置类）开启注解

```
@EnableGlobalMethodSecurity(securedEnabled = true)
```

2、在controller的方法上使用注解，设置角色

```
    @GetMapping("update")
    @Secured({"ROLE_sale","ROLE_manager"})
    public String helloUser(){
        return "hello user";
    }
```

3、userDetailsService设置用户角色

```java
List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins,ROLE_sale");
```



### 3.6.2 @PreAuthorize

先开启注解功能

在方法之前进行校验

1、启动类开启注解

```
@EnableGlobalMethodSecurity(prePostEnabled = true)
```

@PreAuthorize：注解适合进入方法前的权限验证，@PreAuthorize可以将登录用户的 roles/permissions 参数传到方法中



2、在controller的方法上添加注解

```
    @GetMapping("update")
//    @Secured({"ROLE_sale","ROLE_manager"})
    @PreAuthorize("hasAnyAuthority('admins')")
    public String helloUser(){
        return "hello update";
    }
```





### 3.6.3 @PostAuthorize

先开启注解功能

```
@EnableGlobalMethodSecurity(prePostEnabled = true)
```

@PostAuthorize 注解使用并不多，在方法执行后再进行权限验证，适合验证带有返回值的权限

```java
@PostAuthorize("hasAnyAuthority('admins')")
```



### 3.6.4 @PostFilter

@PostFilter：权限验证之后对数据进行过滤 留下用户名是admin1的数据

表达式中的 filterObject 引用的是方法返回值List中的某一个元素

```
    @GetMapping("/getAll")
    @PostAuthorize("hasAnyAuthority('admins')")
    @PostFilter("filterObject.username == 'admin1'")
    public List<Users> getAllUser(){

        List<Users> list = new ArrayList<>();
        list.add(new Users(11,"admin1","666"));
        list.add(new Users(21,"admin2","888"));

        return list;
    }
```



### 3.6.5 @PreFilter

@PreFilter：进入控制器之前对数据进行过滤



## 3.7 用户注销

### 3.7.1 在登录页面添加一个退出连接

1、在配置类中添加退出的配置

```java
 //退出     http.logout().logoutUrl("/logout").logoutSuccessUrl("/test/hello").permitAll();
```



2、测试

* 修改配置类，登录成功之后跳转到成功页面
* 在成功页面添加超链接，写设置退出路径
* 登录成功之后，在成功页面点击退出，再去访问其他controller不能进行访问的（退出之后，是无法访问需要登录时才能访问的控制器）



success.html：

```
<body>
登录成功
<br>
<a href="/logout">退出</a>
</body>
</html>
```





3.8 基于数据库的记住我（自动登录）

1、cookie技术

2、 安全框架机制实现自动登录

* 实现原理：
  * ![image-20230812144654419](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230812144654419.png)
  * 再次访问：获取cookie信息，拿着cookie信息到数据库进行比对，如果查询到对应信息，认证成功，可以登录

![image-20230812144828197](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230812144828197.png)



* 具体实现

1、创建数据库表

```sql
create table persistent_logins(
    username varchar(64) not null,
    series varchar(64) primary key,
    token varchar(64) not null,
    last_used timestamp not null
);
```

2、配置类，注入数据源，配置操作数据库对象

```
    //注入数据源
    @Autowired
    private DataSource dataSource;

    //配置对象
    @Bean
    public PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);

        return jdbcTokenRepository;
    }
```

3、配置类中配置自动登录

```java
                .and().rememberMe().tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(60)   //设置有效时长，单位秒
                .userDetailsService(myUserDetailsService)
```

4、在登录页面中添加复选框

```html
<body>
    <form action="/user/login" method="post">
        用户名:<input type="text" name="username">
        <br>
        密码:<input type="text" name="password">
        <br>
        <input type="checkbox" name="remember-me">自动登录
        <input type="submit" value="login">
    </form>
</body>
```



## 3.8 CSRF

### 3.8.1 CSRF理解

跨站请求伪造(英语: Cross-site requestforgery) ，也被称为 one-clickattack 或者 session riding，通常缩写为 CSRF 或者 XSRF，是一种挟制用户在当前已登录的 Web 应用程序上执行非本意的操作的攻击方法。跟跨网站脚本(XSS) 相比，XSS利用的是用户对指定网站的信任，CSRF 利用的是网站对用户网页浏览器的信任。



跨站请求攻击，简单地说，是攻击者通过一些技术手段欺骗用户的浏览器去访问一个自己曾经认证过的网站并运行一些操作(如发邮件，发消息，甚至财产操作如转账和购买商品)。由于浏览器曾经认证过，所以被访问的网站会认为是真正的用户操作而去运行这利用了 web 中用户身份验证的一个漏洞: 简单的身份验证只能保证请求发自某个用户的浏览器，却不能保证请求本身是用户自愿发出的。



从 Spring Security 4.0 开始，默认情况下会启用 CSRF 保护，以防止 CSRF 攻击应用程序，Spring Security CSRF 会针对 PATCH，POST，PUT和 DELETE方法进行防护。



### 3.8.2 案例

* 在登陆页面添加一个隐藏域：

```html
<form action="update_token" method="post">
    <input type="hidden" th:name="${_crsf.paramterName}" th:value="${_csrf.token}">
    用户名:<input type="text" name="username">
    <br>
    密码:<input type="text" name="password">
    <br>
    <input type="checkbox" name="remember-me">自动登录
    <input type="submit" value="login">
</form>
```

* 关闭安全配置类中的csrf

```
http.scrf().disable();
```



# 4 SpringSecurity 微服务权限方案

## 4.1 什么是微服务

1、微服务由来

微服务最早由 Martin Fowler 与 James Lewis 于 2014,年共同提出，微服务架构风格是一种使用一套小服务来开发单个应用的方式途径，每个服务运行在自己的进程中，并使用轻量级机制通信，通常是 HTTP API，这些服务基于业务能力构建，并能够通过自动化部署机制来独立部署，这些服务使用不同的编程语言实现，以及不同数据存储技术，并保持最低限度的集中式管理。“



2、微服务优势

(1) 微服务每个模块就相当于一个单独的项目，代码量明显减少，遇到问题也相对来说比较好解决。

(2) 微服务每个模块都可以使用不同的存储方式(比如有的用 redis，有的用 mysql等)，数据库也是单个模块对应自己的数据库。

(3) 微服务每个模块都可以使用不同的开发技术，开发模式更灵活



3、微服务本质

(1) 微服务，关键其实不仅仅是微服务本身，而是系统要提供一套基础的架构，这种架构使得微服务可以独立的部署、运行、升级，不仅如此，这个系统架构还让微服务与微服务之间在结构上“松耦合”，而在功能上则表现为一个统一的整体。这种所谓的“统一的整体”表现出来的是统一风格的界面，统一的权限管理，统一的安全策略，统一的上线过程，统一的日志和审计方法，统一的调度方式，统一的访问入口等等。

(2) 微服务的目的是有效的拆分应用，实现敏捷开发和部署。





## 4.2 微服务认证和授权实现过程

1、认证授权过程分析

(1) 如果是基于 Session，那么 Spring-security 会对 cookie 里的 sessionid 进行解析，找到服务器存储的 session 信息，然后判断当前用户是否符合请求的要求。

(2)如果是 token，则是解析出 token，然后将当前请求加入到 Spring-security 管理的权限信息中去

![image-20230813154859027](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230813154859027.png)





## 4.3 完成基于SpringSecurity认证授权案例

### 4.3.1 需求说明

微服务权限管理案例主要功能：

1、登录（认证）

2、添加角色

3、为角色分配菜单

4、添加用户

5、为用户分配角色



### 4.3.2 权限管理数据模型



![image-20230813161649322](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230813161649322.png)



* 添加角色
* 为角色分配菜单



* 添加用户
* 为用户分配角色



### 4.3.3 案例涉及技术说明

1、Maven

创建父工程：管理项目依赖版本

创建子模块：使用具体依赖



2、SpringBoot 

本质就是Spring



3、MyBtisPlus

操作数据库框架



4、SpringCloud

（1）GateWay网关

（2）注册中心  --- Nacos



5、其他技术

* Redis

* JWT
* Swagger
* 前端技术



### 4.3.4 搭建项目工程

1、创建父工程 acl_parent ： 管理依赖版本

2、在父工程创建子模块

（1）common

* service_base：工具类（编写使用工具类，比如MD5加密等等）
* Spring_security：权限配置（SpringSecurity相关配置）

（2）infrastructure

* api_gateway：网关（配置gateway网关）

（3）service

* service_acl：权限管理服务模块（实现权限管理功能代码）





### 4.3.5 引入依赖



### 4.3.6 启动redis和Nacos

1、redis启动

2、启动Nacos

* 注册中心
* 网关服务
* 权限管理服务（ip + port）

![image-20230814152513431](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230814152513431.png)

* 访问地址：[Nacos](http://localhost:8848/nacos)
  * 默认用户名密码：nacos



### 4.3.7 编写common中的工具类

* 编写SpringSecurity认证授权工具类和处理器

![image-20230814154714769](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230814154714769.png)

* 密码处理

```java
    //进行md5加密
    @Override
    public String encode(CharSequence rawPassword) {
        return MD5.encrypt(rawPassword.toString());
    }

    //进行密码比对
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(MD5.encrypt(rawPassword.toString()));
    }
```



* token操作工具类
  * 使用jwt生成token

```java
    //1、根据用户名生成token
    public String createToken(String username){
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + tokenEcpiration))
                .signWith(SignatureAlgorithm.ES512, tokenSignKey).compressWith(CompressionCodecs.GZIP).compact();
        return token;
    }

    //2 根据token字符串得到用户信息
    public String getUserInfoFromToken(String token){
        String userinfo = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token).getBody().getSubject();

        return userinfo;
    }
```

* 退出处理器

```java
    private TokenManager tokenManager;
    private RedisTemplate redisTemplate;

    public TokenLogoutHandler(TokenManager tokenManager,RedisTemplate redisTemplate){
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        //1 从header里面获取token
        //2 token不为空，移除token，从redis删除token
        String token = request.getHeader("token");

        if (token != null){
            //移除
            tokenManager.removeToken(token);
            //从token获取用户名
            String username = tokenManager.getUserInfoFromToken(token);
            redisTemplate.delete(username);
        }
        ResponseUtil.out(response, R.ok());
    }
```

* 未授权统一处理类

```java
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ResponseUtil.out(response,R.error());
    }
```



### 4.3.8 编写自定义认证和授权的过滤器

1、认证的过滤器（继承UsernamePasswordAuthenticationFilter）

```java
    private TokenManager tokenManager;
    private RedisTemplate redisTemplate;
    private AuthenticationManager authenticationManager;


    public TokenLoginFilter(AuthenticationManager authenticationManager,TokenManager tokenManager,RedisTemplate redisTemplate){
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
        this.setPostOnly(false);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/admin/acl/login","POST"));
    }


    //获取表单提交用户名和密码
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            User user = new ObjectMapper().readValue(request.getInputStream(), User.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword(), Collections.<GrantedAuthority>unmodifiableList(new ArrayList<>())));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    //认证成功调用的方法
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        //认证成功之后，得到认证成功之后用户信息
        SecurityUser user = (SecurityUser) authResult.getPrincipal();
        //根据用户名生成token
        String token = tokenManager.createToken(user.getCurrentUserInfo().getUsername(), user.getPermissionValueList());
        //把用户名称和用户权限列表放到redis
        redisTemplate.opsForValue().set(user.getCurrentUserInfo().getUsername(),user.getPermissionValueList());

        //返回
        ResponseUtil.out(response, R.ok().data("token",token));
    }


    //认证失败调用的方法
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        ResponseUtil.out(response,R.error());
    }
```

2、授权的过滤器

```java
    private TokenManager tokenManager;
    private RedisTemplate redisTemplate;

    public TokenAuthFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        //获取当前认证澈哥哥用户权限信息
        UsernamePasswordAuthenticationToken authRequest =  getAuthentication(request);

        //判断如果有权限信息，放到权限上下文中
        if (authRequest != null) {
            SecurityContextHolder.getContext().setAuthentication(authRequest);
        }
        chain.doFilter(request,response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request){
        //从header获取token
        String token = request.getHeader("token");

        if (token != null){
            //从token中获取用户名
            String username = tokenManager.getUserInfoFromToken(token);

            //从redis获取对应权限列表
            List<String> permissionValueList = (List<String>) redisTemplate.opsForValue().get(username);

            Collection<GrantedAuthority> authority = new ArrayList<>();

            for (String permissionValue : permissionValueList){
                SimpleGrantedAuthority auth = new SimpleGrantedAuthority(permissionValue);
                authority.add(auth);
            }
            return new UsernamePasswordAuthenticationToken(username,token,authority);
        }
        return null;
    }
```



### 4.3.9 编写核心配置类

```java
    private TokenManager tokenManager;
    private RedisTemplate redisTemplate;
    private DefaultPasswordEncoder defaultPasswordEncoder;
    private UserDetailsService userDetailsService;

    public TokenWebSecurityConfig(UserDetailsService userDetailsService,DefaultPasswordEncoder defaultPasswordEncoder,
                                  TokenManager tokenManager,RedisTemplate redisTemplate){

    }

    /**
     * 配置设置
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling()
                .authenticationEntryPoint(new UnauthEntryPoint())   //没有权限访问
                .and().csrf().disable()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and().logout().logoutUrl("/admin/acl/index/logout")   //退出路径
                .addLogoutHandler(new TokenLogoutHandler(tokenManager,redisTemplate)).and()
                .addFilter(new TokenLoginFilter(authenticationManager(),tokenManager,redisTemplate))
                .addFilter(new TokenAuthFilter(authenticationManager(),tokenManager,redisTemplate)).httpBasic();
    }

    //调用userDetailsService和密码处理
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(defaultPasswordEncoder);
    }

    
    //不进行认证的路径，可以直接访问
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/**");
    }
```



### 4.3.10 编写UserDetailsService

```java
    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名查询数据
        User user = userService.selectByUserName(username);

        //判断
        if (user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        com.example.security.entity.User curUser = new com.example.security.entity.User();
        BeanUtils.copyProperties(user,curUser);

        //根据用户查询用户权限列表
        List<String> permissionValueList = permissionService.selectPermissionValueByUserId(user.getId());
        SecurityUser securityUser = new SecurityUser();
        securityUser.setPermissionValueList(permissionValueList);

        return securityUser;
    }
```



