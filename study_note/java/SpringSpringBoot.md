# 						Spring/SpringBoot

## Spring工程需要导入的依赖：

```
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.4.0</version>
  <relativePath/>
</parent>
```

```
<dependency>
  <groupId>org.junit.platform</groupId>
  <artifactId>junit-platform-launcher</artifactId>
  <scope>test</scope>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
  <scope>test</scope>
</dependency>

<dependency>
  <groupId>org.mybatis.spring.boot</groupId>
  <artifactId>mybatis-spring-boot-starter</artifactId>
  <version>2.2.2</version>
</dependency>

<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
</dependency>

<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
</dependency>
```



配置文件内容：

```
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/alias?useUnicode=true&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&characterEncoding=utf8
spring.datasource.username=root
spring.datasource.password=ljy040226

mybatis.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl

mybatis.configuration.map-underscore-to-camel-case=true
```





## 请求响应：

@ResponseBody

* 类型：方法注解、类注解
* 位置：Controller方法上/类上
* 作用：将方法返回值直接响应，如果返回值类型是 实体对象/集合，将会转换为JSON格式响应
* 说明：@RestController = @Controller + @ResponseBody;



统一响应结果：

```
Result (code,msg,data);

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Integer code;//响应码，1 代表成功; 0 代表失败
    private String msg;  //响应信息 描述字符串
    private Object data; //返回的数据

    //增删改 成功响应
    public static Result success(){
        return new Result(1,"success",null);
    }
    //查询 成功响应
    public static Result success(Object data){
        return new Result(1,"success",data);
    }
    //失败响应
    public static Result error(String msg){
        return new Result(0,msg,null);
    }
}
```



请求响应案例：

* 在pom.xml文件中引入dom4j的依赖，用于解析XML文件
* 引入资料中提供的解析XML的工具类XMLParserUtils、对应的实体类Emp、XML文件emp.xml
* 引入资料中提供的静态页面文件，放在resources下的static目录下
* 编写Controller程序，处理请求，响应数据



## 分层解耦：

控制反转：lnversion 0f (ontrol，简称10C。对象的创建控制权由程序自身转移到外部(容器)，这种思想称为控制反转。

依赖注入：Dependency lnjection，简称DI。容器为应用程序提供运行时，所依赖的资源，称之为依赖注入。

Bean对象：IOC容器中创建、管理的对象，称之为bean。



* IOC & DI入门

1. 将Service层几Dao层的实现类交给IOC容器管理(加上注解@Component)
2. 为Controller 及 Service注入运行时，依赖的对象(加上注解@Autowired)



要把某个对象交给IOC容器管理，需要在对应的类上加上如下注解之一：

| 注解         | 说明                 | 位置                                          |
| ------------ | -------------------- | --------------------------------------------- |
| @Component   | 声明bean的基础注解   | 不属于以下三类时，用此注解                    |
| @Controller  | @Conponent的衍生注解 | 标注在控制器类上                              |
| @Service     | @Conponent的衍生注解 | 标注在业务类上                                |
| @Respository | @Conponent的衍生注解 | 标注在数据访问类上(由于与mybatis整合，用的少) |

注意事项：

* 声明bean的时候，可以通过value属性指定bean的名字，如果没有指定，默认为类名首字母小写。
* 使用以上四个注解都可以声明bean，但是在springboot集成web开发中，声明控制器bean只能用@Controller。



Bean组件扫描：

* 前面声明bean的四大注解，要想生效，还需要被组件扫描注解@ComponentScan扫描
* @ComponentScan注解虽然没有显示配置，但是实际上已经包含在了启动类声明注解@SpringBootApplication中，默认扫描的范围是启动类所在包及其子包



Bean注入：

* @Autowired注解，默认是按照类型进行，如果存在多个相同类型的bean，将会报错
* 通过以下几个方案来解决：
  * @Primary
  * @Qualifier("bean的名称") + @Autowired
  * @Resource("name="bean的名称")



@Resource与@Autowired区别

* @Autowired 是spring框架提供的注解，而@Resource是JDK提供的注解
* @Autowired 默认是按照类型注入，而@Resource默认是按照名称注入。





# 3 MyBatis

## 3.1 简介

什么是MyBstis？

* MyBatis是一款优秀的持久层框架，用于简化JDBC开发
* MyBatis本是Apache的一个开源项目iBatis，2010年这个项目由apache software foundation 迁移到了google code,并且改名为MyBatis.2013年11月迁移到Github
* 官网：http://mybatis.org/mybatis-3/zh/index.html



持久层

* 负责将数据保存到数据库的那一层代码
* JavaEE三层架构：表现层、业务层、持久层

表现层：做页面展示

业务层：做逻辑处理

持久层：对数据进行持久化，保存到数据库



框架：

* 框架就是一个半成品软件，是一套可重用的、通用的、软件基础代码模型
* 在框架的基础之上构建软件编写更加高效、规范、通用、可拓展



JDBC缺点：

1. 硬编码
   * 注册驱动，获取连接
   * SQL语句
2. 操作繁琐
   * 手动设置参数
   * 手动封装结果集



## 3.2 快速入门

UserMapper.xml文件：

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
    namespace:名称空间
-->
<mapper namespace="test">
    <select id="selectAll" resultType="pojo01.User">
        select * from tb_user;
    </select>
</mapper>
```

mybatis-config.xml文件：

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <!--数据库连接信息-->
                <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/itheima?useSSL=false&amp;serverTimezone=UTC"/>
                <property name="username" value="root"/>
                <property name="password" value="ljy040226"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <!--加载SQL映射文件-->
        <mapper resource="UserMapper.xml"/>
    </mappers>
</configuration>
```

class文件：

```
//1.加载mybatis的核心配置文件，获取sqlSessionFactory
String resource = "mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

//2.获取sqlSession对象，用他来执行sql
SqlSession sqlSession = sqlSessionFactory.openSession();

//3.执行sql
List<Object> users = sqlSession.selectList("test.selectAll");

System.out.println(users);

//释放资源
sqlSession.close();
```



解决SQL映射文件的警告提示

* 产生原因：Idea和数据库没有建立连接，不识别表信息
* 解决方式：在Idea中配置MySQL数据库连接

![image-20230209140347598](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230209140347598.png)



mybatis入门：（查询所有用户数据）

1. 准备工作（创建springboot工程、数据库表user、实体类User）
2. 引入Mybatis的相关依赖，配置Mybatis（数据库连接信息）
3. 编写SQL语句（注解/XML）





## 3.3 Mapper 代理开发

* 目的
  * 解决原生方式中的硬编码
  * 简化后期执行SQL

![image-20230209141936351](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230209141936351.png)

* Mapper代理方式：
  * 定义与SQL映射文件同名的Mapper接口，并且将Mapper接口与SQL映射文件放置在同一目录下
  * 设置SQL映射文件的namespace属性为Mapper接口全限定名
  * 在Mapper接口中定义方法，方法名就是SQL映射文件中sql语句的id，并保持参数类型和返回值类型一致
  * 编码
    1. 通过SqlSession的getMapper方法获取Mapper接口的代理对象
    2. 调用对应方法完成sql的执行

细节：如果Mapper接口名称和SQL映射文件名称相同，并在同一目录下，则可以使用包扫描的方式简化SQL映射文件的加载

![image-20230209144313677](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230209144313677.png)

代码演示：

class文件：

```
        //1.加载mybatis的核心配置文件，获取sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2.获取sqlSession对象，用他来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3.执行sql
//        List<Object> users = sqlSession.selectList("test.selectAll");
        //3.1获取UserMapper接口的代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.selectAll();

        System.out.println(users);

        //释放资源
        sqlSession.close();
```

接口：

```
public interface UserMapper {
    List<User> selectAll();
}
```

UserMapper.xml：

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
    namespace:名称空间
-->
<mapper namespace="mapper.UserMapper">
    <select id="selectAll" resultType="pojo01.User">
        select * from tb_user;
    </select>
</mapper>
```



mybatis-config.xml：

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <!--数据库连接信息-->
                <property name="driver" value="com.mysql.cj.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://localhost:3306/itheima?useSSL=false&amp;serverTimezone=UTC"/>
                <property name="username" value="root"/>
                <property name="password" value="ljy040226"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <!--加载SQL映射文件-->
       <!-- <mapper resource="mapper/UserMapper.xml"/> -->

        <!--Mapper代理方式-->
        <package name="mapper"/>

    </mappers>
</configuration>
```



## 3.4 MyBatis核心配置文件

![image-20230209150534759](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230209150534759.png)





## 3.5 配置文件完成增删改查

* 查询所有数据：

  1. 编写接口方法：Mapper接口			List<Brand> selectAll();

  * 参数：无
  * 结果：List<Brand>                                        <select id = "selectAll" resultType="brand">		

  2. 编写SQL语句：SQL映射文件                                    select * from tb_brand
  3. 执行方法：测试                                                  </select>



## 3.6 lombok

* Lombok是一个实用的Java类库，能通过注解的形式自动生成构造器、getter/setter、equals、hashcode、toString等方法，并可以自动化生成日志变量，简化java开发、提高效率。

| 注解                |                                                              |
| ------------------- | ------------------------------------------------------------ |
| @Getter/@Setter     | 为所有的属性提供get/set方法                                  |
| @ToString           | 会给类自动生成易阅读的tostring 方法                          |
| @EqualsAndHashCode  | 根据类所拥有的非静态字段自动重写 equals 方法和 hashCode 方法 |
| @Data               | 提供了更综合的生成代码功能 (@Getter + @Setter + @ToString + @EqualsAndHashCode) |
| @NoArgsConstructor  | 为实体类生成无参的构造器方法                                 |
| @AllArgsConstructor | 为实体类生成除了static修饰的字段之外带有各参数的构造器方法   |

使用时引入依赖：

```
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
    </dependency>
```



## 3.7 Mybatis基础操作

准备工作：

* 准备数据库表emp
* 创建一个新的springboot工程，选择引入对应的起步依赖(mybatis、mysql驱动、lombok)
* application.properties中引入数据库连接信息
* 创建对应的实体类Emp(实体类属性采用驼峰命名)
* 准备Mapper接口EmpMapper



* 删除操作：

  ```
      /**
       * 根据ID删除数据
       * @param id
       */
      @Delete("delete from emp where id=#{id}")
      public void delete(Integer id);
  ```



* 日志输出：

可以在application.properties中，打开mybatis的日志，并指定输出到控制台

```
#指定mybatis输出日志的位置,输出控制台mybatis.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl
```

* 预编译SQL：
  * 性能更高
  * 更安全（防止SQL注入）
    * SQL注入：是通过操作输入的数据来修改事先定义好的SQL语句，以达到执行代码对服务器进行攻击的方法

| #{...}                                                       | ${...}                                                |
| ------------------------------------------------------------ | ----------------------------------------------------- |
| 执行SQL时，会将#{...}替换为?，生成预编译SQL，会自动设置参数值。 | 拼接SQL。直接将参数拼接在SQL语句中，存在SQL注入问题。 |
| 使用时机:参数传递，都使用#{...}                              | 使用时机:如果对表名、列表进行动态设置时使用           |

* 新增操作

```
    /**
     * 增加用户
     * @param emp
     */
    @Options(keyProperty = "id",useGeneratedKeys = true)//会自动将生成的主键值，赋值给emp对象的id属性
    @Insert("insert into emp (username, name, gender, image, job, entrydate, dept_id, create_time, update_time) VALUES (#{userName},#{name},#{gender},#{image},#{job},#{entryDate},#{deptId},#{createTime},#{updateTime})")
    public void insert(Emp emp);
```



* 更新信息

```
    /**
     * 更新用户信息
     * @param emp
     */
    @Update("update emp set username=#{userName}, name=#{name}, gender=#{gender}, image=#{image}, job=#{job}, entrydate=#{entryDate}, dept_id=#{deptId}, create_time=#{createTime}, update_time=#{updateTime} where id = #{id}")
    public void update(Emp emp);
```



* 数据封装
  * 实体类属性名和数据库查询返回的字段名一致，mybatis会自动封装
  * 如果实体类属性名和数据库表查询返回的字段名不一致，不能自动封装
* 起别名：在SQL语句中，对不一样的列名起别名，别名和实体类属性名一样。

```
//    /**
//     * 根据id查询,起别名
//     * @param id
//     * @return
//     */
//    @Select("select id,username, name, gender, image, job, entrydate,dept_id as deptId, create_time as createTime, update_time as updateTime from emp where id = #{id}")
//    public Emp getById(Integer id);
```

* 手动结果映射:通过 @Results及@Result 进行手动结果映射。

```
    /**
     * 根据id查询
     * 通过@Results，@Result注解手动映射封装
     * @param id
     * @return
     */
    @Results({
            @org.apache.ibatis.annotations.Result(column = "dept_id",property = "deptId"),
            @org.apache.ibatis.annotations.Result(column = "create_time",property = "createTime"),
            @org.apache.ibatis.annotations.Result(column = "update_time",property = "updateTime")
    })
    @Select("select * from emp where id = #{id}")
    public Emp getById(Integer id);
```

* 开启驼峰命名:如果字段名与属性名符合驼峰命名规则，mybatis会自动通过驼峰命名规则映射。

  在application配置文件中，加入：

```
mybatis.configuration.map-underscore-to-camel-case=true
```



* 条件查询

```
    /**
     * 模糊匹配
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @return
     */
    @Select("select * from emp where (name like '%${name}%' and gender = #{gender} and entryDate between #{begin} and #{end}) order by update_time desc")
    public List<Emp> list(String name, Short gender, LocalDate begin,LocalDate end);
```

使用concat函数进行字符串拼接：

```
    /**
     * 模糊匹配
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @return
     */
    @Select("select * from emp where (name like concat('%',#{name},'%') and gender = #{gender} and entryDate between #{begin} and #{end}) order by update_time desc")
    public List<Emp> list(String name, Short gender, LocalDate begin,LocalDate end);
```



## 3.8 XML映射文件

* XML映射文件的名称与Mapper接口名称一致，并且将XML映射文件和Mapper接口放置在相同包下(同包同名)
* XML映射文件的namespace属性为Mapper接口全限定名一致
* XML映射文件中sql语句的id与Mapper 接口中的方法名一致，并保持返回类型一致

![image-20230516125419397](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230516125419397.png)



XML文件配置信息：

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
```



使用Mybatis的注解，主要是完成一些简单的增删改查功能。如果需要实现复杂的SQL功能，建议使用XML来配置映射语句

官方说明：https://mybatis.net.cn/getting-started.html



## 3.9 动态SQL

随着用户的输入或外部条件的变化而变化的SQL语句，我们称为动态SQL



* <if>
  * <if>：用于判断条件是否成立。使用test属性进行条件判断，如果条件为true，则拼接SQL
  * <where>：where元素只会在子元素有内容的情况下才会插入where子句。而且会自动去除子句的开头的AND或OR
  * <set> ：动态地在行首插入SET关键字，并会删掉额外的逗号（用在update语句中）



* <foreach>
  * 属性：
    * collection：遍历的集合
    * item：遍历出来的元素
    * separator：分隔符
    * open：遍历开始前拼接的SQL片段
    * close：遍历结束后拼接的SQL片段



* <sql> <include>
* <sql>：定义可重用的SQL片段
* <include>：通过属性refid，指定包含的sql片段

案例：

```XML
<sql id="query">select * from emp</sql>
<!--    单条记录所封装的类型-->
<select id="list" resultType="com.itheima.PoJo.Emp">
    <include refid="query"></include>
            <where>
               <if test="name != null">
                   name like concat('%',#{name},'%')
               </if>
            <if test="gender != null">
                and gender = #{gender}
            </if>
            <if test="begin != null and end != null">
                and entryDate between #{begin} and #{end}
            </if>
            </where>
            order by update_time desc
</select>
```



# 4 案例演示：

开发流程：查看页面原型明确需求 -> 阅读接口文档 -> 思路分析 -> 接口开发 -> 接口测试 -> 前后端联调

* 日志小技巧

```
@Slf4j
@RestController
public class DeptController{}
```



开发规范-Restful

* RES下(REpresentational State Transfer)，表述性状态转换，它是一种软件架构风格



传统风格：

```
http://localhost:8080/user/getById?id=1   GET:查询id为1的用户

http://1ocalhost:8080/user/saveUser   POST:新增用户

http://localhost:8080/user/updateuser   POST:修改用户

http://localhost:8080/user/deleteuser?id=1   GET:删除id为1的用户
```

REST风格：

```
http://localhost:8080/users/1    GET:查询id为1的用户
http://localhost:8080/users		 POST: 新增用户
http://localhost:8080/users		 PUT:修改用户
http://localhost:8080/users/1	 DELETE:删除id为1的用户
```

注意事项：

* REST是风格，是约定方式，约定不是规定，可以打破
* 描述模块的功能通常使用复数，也就是加s的格式来描述，表示此类资源，而非单个资源。如: users、emps、books...



## 4.1 部门管理：

* 查询部门（@GetMapping）（）







* 前后端联调
  * 将资料中提供的“前端工程”文件夹中的压缩包，拷贝到一个没有中文不带空格的目录下，解压。
  * 启动nginx，访问测试: http://localhost:90



* 删除部门（@DeleteMapping）

注意事项：

一个完整的请求路径，应该是类上的 @RequestMapping 的value属性 + 方法上的 @RequestMapping的value属性.





* 新增部门（@PostMapping）





* 修改部门-根据ID查询、修改部门



## 4.2 员工管理

* 分页查询员工

@RequestParam 的属性defaultValue可以来设置参数的默认值



可以使用：pagehelper简化开发：

需要导入pagehelper依赖

```
<dependency>
  <groupId>com.github.pagehelper</groupId>
  <artifactId>pagehelper-spring-boot-starter</artifactId>
  <version>1.4.2</version>
</dependency>
```

实例：（service层）

```
@Override
public PageBean page(Integer page, Integer pageSize) {
    //设置分页参数
    PageHelper.startPage(page,pageSize);

    //执行查询
    List<Emp> empList = empMapper.page();
    Page<Emp> p = (Page<Emp>) empList;

    return new PageBean(p.getTotal(),p.getResult());
}
```



* 批量删除员工：主要是使用<foreach>动态sql



* 新增员工：（阿里云OSS-集成）

步骤：

* 引入阿里云OSS上传文件工具类（由官方的示例代码改造而来）
* 上传图片接口开发



* 修改员工
  * 查询回显
  * 修改员工



# 5 文件上传

## 5.1 简介：

* 文件上传，是指将本地图片、视频、音频等文件上传到服务器，供其他用户浏览或下载的过程
* 文件上传在项目中应用非常广泛，我们经常发微博，发微信朋友圈都用到了文件上传功能

![image-20230517225227580](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230517225227580.png)



<form action="/upload" method="post"enctype="multipart/form-data">
    姓名: <input type="text" name="username"><br>
    年龄: <input type="text" name="age"><br>
    头像: <input type="file" name="image"><br>
    <input type="submit" value="提交">
</form>



```
@RestController
public class UploadController {
@PostMapping("/upload")
public Result upload(String username , Integer age,@RequestParam("image") MultipartFile image){
return Result.success();
	}
}
```



* 前端页面三要素
  * 表单项 type="file"
  * 表单提交方式 post
  * 表单的enctype属性 multipart/form-data
* 服务端接收文件
  * MutipartFile



## 5.2 本地存储

在服务端，接收到上传上来的文件之后，将文件存储在本地服务器磁盘中



在SpringBoot中，文件上传，默认单个文件允许最大大小为1M，如果需要上传大文件，可以进行如下配置：

在properties中：(文件上传配置)

```
#配置单个文件最大上传大小
spring.servlet.multipart.max-file-size=10MB
#配置单个请求最大上传(一次请求可以上传多个文件)
spring.servlet.multipart.max-request-size=100MB
```



* 劣势：

前端页面无法直接访问文件

磁盘空间有限，容易丢失



```
log.info("文件上传:image:{},{},{}",username,age,image);

//获取原始文件名
String originalFilename = image.getOriginalFilename();

//构建唯一的文件名（不能重复）
int index = originalFilename.lastIndexOf(".");
String extname = originalFilename.substring(index);
String newFileName = UUID.randomUUID().toString() +extname;
log.info("新文件名:{}",newFileName);


//将文件存储在服务器的磁盘目录中E:\MyByte
image.transferTo(new File("E:\\MyByte\\"+originalFilename));
```

multipart主要方法：

* String getOriginalFilename(); //获取原始文件名
* void transferTo(File dest);//将接收的文件转存到磁盘文件中
* long getSize(); //获取文件的大小，单位: 字节
* byte[] getBytes(); //获取文件内容的字节数组
* InputStream getlnputStream(); //获取接收到的文件内容的输入流



## 5.3 阿里云OSS

阿里云是阿里巴巴集团旗下全球领先的云计算公司，也是国内最大的云服务提供商



阿里云对象存储OSS（Object Storage Service），是一款海量、安全、低成本、高可靠的云存储服务。使用OSS，您可以通过网络随时存储和调用包括文本、图片、音频和视频等在内的各种文件



* 第三方服务-通用思路

准备工作 --> 参照官方SDK编写入门程序 --> 继承使用



注册阿里云(实名认证) -> 充值 -> 开通对象存储服务(OSS) -> 创建bucket -> 获取AccessKey(秘钥)  --> 参照官方SDK编写入门程序 --> 继承使用



* Bucket：存储空间是用户用于存储对象（Object,就是文件）的容器，所有的对象都必须隶属于某个存储空间

* SDK：Software Development Kit的缩写，软件开发工具包，包括辅助软件开发的依赖（jar包）、代码示例等，都可以叫SDK



# 6 配置文件

## 6.1 参数配置化

```
#自定义的阿里云OSS配置信息
aliyun.oss.endpoint=https://oss-cn-hangzhou.aliyuncs.com
aliyun.oss.accessKeyId=LTAI4GCHlVX6DKqJWxd6nEuW
aliyun.oss.accessKeySecret=yBshYweHOpqDuhCArrVHwIiBKpyqSL
aliyun.oss.bucketName=web-tlias
```

* @Value注解通常用于外部配置的属性注入，具体用法为：@Value("${配置文件中的key}")



## 6.2 yml配置文件

* SpringBoot提供了多种属性配置方式

  * application/properties

    ```
    server.port=8080
    server.address=127.0.0.1
    ```

  * application.yml

    ```
    server:
    	port: 8080
    	address: 127.0.0.1
    ```

  * application.yaml

    ```
    server:
    	port: 8080
    	address: 127.0.0.1
    ```



* 常见配置文件格式对比

  * XML（臃肿）

  ```
  <server>
  	<port>8080</port>
  	<address>127.0.0.1</address>
  </server>	
  ```

  * properties（层级结构不清晰）

  ```
  server.port=8080
  server.address=127.0.0.1
  ```

  * yml / yaml（简洁、数据为中心）（推荐）

  ```
  server:
  	port: 8080
  	address: 127.0.0.1
  ```



* yml基本语法：
  * 大小写敏感
  * 数值前边必须有空格，作为分隔符
  * 使用缩进表示层级关系，缩进时，不允许使用Tab键，只能用空格（idea中会自动将Tab转换为空格）
  * 缩进的空格数且不重要，只要相同层级的元素左侧对齐即可
  * #表示注释，从这个字符一直到行尾，都会被解析器忽略

```
#配置服务器相关信息
server:
	port: 8080
	address: 127.0.0.1
```



yml数据格式：

* 对象/Map集合：

```
user:
   name:zhangsan
   age:18
   password: 123456
```

* 数组/List/Set集合：

```
hobby:
 -java
 -game
 -sport
```



* 在application.yml中配置案例相关的配置项

```
#数据库连接信息
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/alias?useUnicode=true&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&characterEncoding=utf8
    username: root
    password: ljy040226

  #文件上传的配置
  servlet:
    multipart:
      max-file-size: 10MB
      max-request-size: 100MB

#Mybatis配置
mybatis:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
    map-underscore-to-camel-case: true


aliyun:
  oss:
    endpoint: https://oss-cn-hangzhou.aliyuncs.com
    accessKeyId: LTAI5tN6zeya5yCeSM6W9arP
    accessKeySecret: WgdG0Qn2u6q8iakHWFSOfXalMYTZfG
    bucketName: web-alias-ljy
```



## 6.3 @ConfigurationProperties

引入依赖：

```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-configuration-processor</artifactId>
</dependency>
```



用法：@ConfigurationProperties(prefix = "aliyun.oss")



@ConfigurationProperties与@Value

* 相同点：

  都是用来注入配置的属性的

* 不同点：

  * @Value注解只能一个一个的进行外部属性的注入
  * @ConfigurationProperties可以批量的将外部的属性配置注入到bean对象的属性中



# 7 事务管理&AOP

## 7.1 事务管理

### 7.1.1 事务回顾

概念：事务是一组操作的集合，它是一个不可分割的工作单位，这些操作 要么同时成功，要么同时失败



操作：

* 开启事务（一组操作开始前，开启事务）：start transaction / begin
* 提交事务（这组操作全部成功后，提交事务）：commit
* 回滚事务（中间任何一个操作出现异常，回滚事务）：rollback





### 7.1.2 Spring事务管理

### 7.1.2 Spring事务管理

注解：

* 注解：@Transactional
* 位置：业务（service）层的方法上、类上、接口上
* 作用：将当前方法交给spring进行事务管理，方法执行前，开启事务；成功执行完毕，提交事务；出现异常，回滚事务



日志开关配置文件：

```
#spring事务管理日志
logging:
	level:
		org.springframework.jdbc.support.JdbcTransactionManager: debug
```



例子：

```
@Transactional
@Override
public void delete(Integer id) {
    deptMapper.deleteById(id);

    empMapper.deleteById(id);
}
```



### 7.1.3 事务进阶

* rollbackFor：
  * 默认情况下，只有出现 RuntimeException 才回滚异常。rollbackFor属性用于控制出现何种异常类型，回滚事务

* propagatioon：
  * 事务传播行为：指的就是当一个事务方法被另一个事务方法调用时，这个事务方法应该如何进行事务控制

| 属性值        | 含义                                                         |
| ------------- | ------------------------------------------------------------ |
| REQUIRED      | [默认值]需要事务，有则加入，无则创建新事务                   |
| REQUIRES NEW  | 需要新事务，无论有无，总是创建新事务                         |
| SUPPORTS      | 支持事务，有则加入，无则在无事务状态中运行                   |
| NOT SUPPORTED | 不支持事务，在无事务状态下运行,如果当前存在已有事务,则挂起当前事务 |
| MANDATORY     | 必须有事务，否则抛异常                                       |
| NEVER         | 必须没事务，否则抛异常                                       |



场景：

* REQUIRED：大部分情况下都是用该传播行为即可
* REQUIRED_NEW：当我们不希望事务之间相互影响时，可以使用该传播行为。比如：下订单前需要记录日志，不论订单保存成功与否，都需要保证日志记录能够记录成功



## 7.2 AOP基础

### 7.2.1 AOP概述

* 概述
  * AOP：Aspect Oriented Programming（面向切面编程、面向方面编程），其实就是面向特定方法编程

* 场景：
  * 案例部分功能运行较慢，定位执行耗时较长的业务方法，此时需要统计每一个业务方法的执行耗时
* 实现：
  * 动态代理是面向切面编程最主流的实现。而SpringAOP是Spring框架的高级技术，旨在管理bean对象的过程中，主要通过底层的动态代理机制，对特定的方法进行编程



### 7.2.2 AOP快速入门

* 统计各个业务层方法执行耗时

  * 导入依赖：

  ```
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-aop</artifactId>
      </dependency>
  ```

  * 编写AOP程序：针对特定方法根据业务需要进行编程

* 场景：
  * 记录操作日志
  * 权限控制
  * 事务管理
* 优势：
  * 代码无侵入
  * 减少重复代码
  * 提高开发效率
  * 维护方便



### 7.2.3 AOP核心概念

* 连接点：JoinPoint，可以被AOP控制的方法（暗含方法执行时的相关信息）
* 通知：Advice，指哪些重复的逻辑，也就是共性功能（最终体现为一个方法）
* 切入点：PointCut，匹配连接点的条件，通知仅会在切入点方法执行时被应用
* 切面：Aspect，描述通知与切入点的对应关系（通知+切入点）
* 目标对象：Target，通知所应用的对象

![image-20230522223147340](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230522223147340.png)

![image-20230522223202595](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230522223202595.png)



## 7.3 AOP进阶

### 7.3.1 通知类型

* @Around：环绕通知，此注解标注的通知方法在目标方法前、后都被执行
* @Before：前置通知，此注解标注的通知方法在目标方法前被执行
* @After：后置通知，此注解标注的通知方法在目标方法后被执行，无论是否有异常都会执行
* @AfterReturning ：返回后通知，此注解标注的通知方法在目标方法后被执行，有异常不会执行
* @AfterThrowing：异常后通知，此注解标注的通知方法发生异常后执行



注意事项：

* @Around环绕通知需要自己调用 ProceedingJoinPoint.proceed()来让原始方法执行，其他通知不需要考虑目标方法执行
* @Around环绕通知方法的返回值，必须指定为Object，来接收原始方法的返回值





@PointCut

* 该注解的作用是将公共的切点表达式抽取出来，需要用到时引用该切点表达式即可

```
@Pointcut("execution(* com.itheima.service.Impl.DeptServiceImpl.*(..))")
private void pt(){}

@Before("pt()")
public void before(){
    log.info("before...");
}
```



### 7.3.2 通知顺序

当有多个切面的切入点都匹配到了目标方法，目标方法运行时，多个通知方法都会被执行

执行顺序：

* 不同切面类中，默认按照切面类的类名字母排序
  * 目标方法前的通知方法：字母排名靠前的先执行
  * 目标方法后的通知方法：字母排名靠前的后执行

* 用@Order(数字)加在切面类上来控制顺序
  * 目标方法前的通知方法：数字小的先执行
  * 目标方法后的通知方法：数字小的后执行



### 7.3.3 切入点表达式

* 切入点表达式：描述切入点方法的一种表达式
* 作用：主要用来决定项目中的哪些方法需要加入通知
* 常见形式：
  * execution(.....)：根据方法的签名来匹配
  * @annotation(......)：根据注解匹配



execution主要根据方法的返回值、包名、类名、方法名、方法参数等信息来匹配，语法为：

```
execution(访问修饰符? 返回值 包含.类名.?方法名(方法参数) throws 异常)
```

* 其中带？的表示可以省略的部分
  * 访问修饰符：可省略（比如：public、protected）
  * 包名.类名：可省略
  * throws 异常：可省略（注意是方法上声明抛出的异常，不是实际抛出的异常）

```
@Pointcut ("execution(public void com,itheima,service,impl,DeptServicelmpl.delete(java,lang.Integer))")

private void pt(){}

@Before ("pt ()")
public void before(){
log.info("MyAspect6 ... before ...") ;
```



* 可以使用通配符描述切入点
  * *：单个独立的任意符号，可以通配任意返回值、包名、类名、方法名、任意类型的一个参数，也可以通配包、类、方法名的一部分

```
execution(* com.*.service.*.update*(*))
```

* ..：多个连续的任意符号，可以通配任意层级的包，或任意类型、任意个数的参数

```
execution(* com.itheima..DeptService.*(..))
```



注意事项：

* 根据业务需要，可以使用且（&&）、或（！）来组合比较复杂的切入点表达式



* 书写建议：
  * 所有业务方法名在命名时尽量规范，方便切入点表达式快速匹配。如：查询方法都是find开头，更新类方法都是update开头
  * 描述切入点方法通常基于接口描述，而不是直接描述实现类，增强拓展性
  * 在满足业务需要的前提下，尽量缩小切入点的匹配范围。如：包名匹配尽量不使用..，使用*匹配单个包



* @annotation 切入点表达式，用于匹配标识有特定注解的方法

```
@annotation(com.itheima.anno.Log)
```

```
@Before("@annotation(com.itheima.anno.Log)")
	public void before() {
		log.info("before ....") ;
}
```



### 7.3.4 连接点

* 在Spring中用JointPoint抽象了连接点，用它可以获得方法执行时的相关信息，如目标类名、方法名、方法参数等
  * 对于 @Around 通知，获取连接点信息只能使用 ProceedingJoinPoint
  * 对于其他四种通知，获取连接点信息只能使用JoinPoint，它是ProceedingJoinPoint 的父类型

```
@Around("execution(* com.itheima.service.DeptService.*(..))")
public void before(JoinPoint joinPoint){
	String className = joinPoint.getTarget().getclass().getName(); //获取目标类名
	Signature signature = joinPoint.getSignature(); //获取目标方法签名
	String methodName = joinPoint.getSignature().getName(); //获取目标方法名
	Object[] args = joinPoint.getArgs(); //获取目标方法运行参数
}
```



## 7.4 AOP案例



将案例中增、删、改相关接口的操作日志记录到数据库表中

* 操作日志：

  日志信息包含：操作人、操做时间、执行方法的全类名、执行方法名、方法运行时参数、返回值、方法执行



* 需要对所有业务类中的增、删、改方法添加统一功能，使用AOP技术最为方便	@Around 环绕通知

* 由于、增、删、改方法名没有规律，可以自定义@Log注解完成目标方法匹配



* 获取当前登录用户
  * 获取request对象，从请求头中获取jwt令牌，解析令牌获取出当前用户的id



# 8 Web后端开发（原理篇）

## 8.1 配置优先级

* SpringBoot 中支持三种格式的配置文件：

  properties > yml > yaml

* 注意事项：虽然springboot支持多种格式配置文件，但是在项目开发时，推荐同意使用一种格式的配置（yml时主流）

* SpringBoot除了支持配置文件属性配置，还支持Java系统属性和命令行参数的方式进行属性配置

```
VM options:-Dserver.port=9000
Program arguments:--server.port=10010
```



1. 执行maven 打包指令package

tlias-web-management-0.0.1-SNAPSHOT.jar

2. 执行Java命令，运行jar包

```
java -Dserver.port=9000 -jar tlias-web-management-0.0.1-SNAPSHOT.jar --server.port=10010
```

* 注意：Springboot项目进行打包时，需要引入插件 spring-boot-maven-plugin（基于官网骨架创建项目，会自动添加该插件）

* **优先级：(高 -> 低)**
  * 命令行参数
  * Java系统属性
  * properties
  * yml
  * yaml



## 8.2 Bean管理

### 8.2.1 获取bean

* 默认情况下，Spring项目启动时，会把bean都创建好放在IOC容器中，如果想要主动获取这些bean，可以通过如下方式：

  * 根据name获取bean：

    ```
    Object getBean(String name)
    ```

  * 根据类型获取bean：

    ```
    <T> T getBean(Class<T> requiredType)
    ```

  * 根据name获取bean（带类型转换）：

    ```
    <T> T getBean(String name,Class<T> requiredType)
    ```

* 注意：上述所说的【Spring项目启动时，会把其中的bean都创建好】还会受到作用域及延迟初始化影响，这里主要针对于默认的单例非延迟加载的bean而言



### 8.2.2. bean作用域

* Spring支持五种作用域，后三种在web环境才生效

| 作用域      | 说明                                           |
| ----------- | ---------------------------------------------- |
| singleton   | 容器内同 名称的 bean 只有一个实例(单例) (默认) |
| prototype   | 每次使用该 bean 时会创建新的实例 (非单例)      |
| request     | 每个请求范围内会创建新的实例 (web环境中，了解) |
| session     | 每个会话范围内会创建新的实例 (web环境中，了解) |
| application | 每个应用范围内会创建新的实例 (web环境中，了解) |

* 可以通过@Scope注解来进行配置作用域：

```java
@Slf4j
@RestController
@Scope("prototype")
@RequestMapping("/depts")
public class DeptController {
```



注意事项：

* 默认singleton的bean，在容器启动时被创建，可以使用@Lazy注解来延迟初始化(延迟到第一次使用时)
* prototype的bean，每一次使用该bean的时候都会创建一个新的实例。
* 实际开发当中，绝大部分的Bean是单例的，也就是说绝大部分Bean不需要配置scope属性。



### 8.2.3 第三方bean

* @Bean：

  * 如果要管理的bean对象来自于第三方（不是自定义的），是无法使用@Component 及衍生注解声明bean的，就需要用到@bean注解

  * 若要管理第三方bean对象，建议对这些bean进行集中分类配置，可以通过@Configuration 注解声明一个配置类

注意：

* 通过@Bean注解的name或value属性可以声明bean的名称，如果不指定，默认bean的名称就是方法名
* 如果第三方bean需要依赖其它bean对象，直接在bean定义方法中设置形参即可，容器会根据类型自动装配



* @Component及衍生注解与@Bean注解使用场景
  * 项目中自定义的，使用@Component及其衍生注解
  * 项目中引入第三方的，使用@Bean注解



## 8.3 SpringBoot原理

### 8.3.1 起步依赖原理

及A 依赖了 B ，B 依赖了C ，C 依赖了 D，则引入A的依赖，就将ABCD全部依赖引用了



### 8.3.2 自动配置

* SpringBoot的自动配置就是当spring容器启动后，一些配置类、bean对象就自动存入到了IOC容器中，不需要我们手动去声明，从而简化了开发，省去了繁琐的配置操作



#### 8.3.2.1 自动配置原理

* 方案一：@ComponentScan 组件扫描(使用繁琐，性能低)

  ```
  @Component({"com.example","com.itheima"})
  @SpringBootApplication
  public class SpringbootWebConfig2Application{
  }
  ```

* 方案二：@Import 导入。使用@Import导入的类会被Spring加载到IOC容器中，导入形式主要有以下几种：

  * 导入 普通类
  * 导入配置类
  * 导入ImportSelector 接口实现类
  * @EnableXxxx注解，封装@Import注解（方便）

```
@Import({TokenParser.class， HeaderConfig.class})
@SpringBootApplicationpublic class SpringbootWebConfig2Application {
}
```



* 源码跟踪

![image-20230525171259750](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230525171259750.png)

![image-20230525171642952](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230525171642952.png)



@SpringBootApplication：

* 该注解标识在SpringBoot工程引导类上，是SpringBoot中最最最重要的注解。该注解由三个部分组成：
  * @SpringBootConfiguration: 该注解与 @Configuration 注解作用相同，用来声明当前也是一个配置类
  * @ComponentScan: 组件扫描，默认扫描当前引导类所在包及其子包。
  * @EnableAutoConfiguration: SpringBoot实现自动化配置的核心注解。



![image-20230525171939512](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230525171939512.png)

@Conditional：

* 作用:按照一定的条件进行判断，在满足给定条件后才会注册对应的bean对象到SpringIOC容器中
* 位置：方法、类
* @Conditional本身是一个父注解，派生出大量的子注解
  * @ConditionalOnClass：判断环境中是否有对应字节码文件，才注册bean到IOC容器。
  * @ConditionalOnMissingBean：判断环境中没有对应的bean (类型或 名称)，才注册bean到IOC容器
  * @ConditionalOnProperty： 判断配置文件中有对应属性和值，才注册bean到IOC容器。

```
@Bean
@Conditional0nClass(name ="io,jsonwebtoken.Jwts") //当前环境存在指定的这个类时，才声明该bean
public HeaderParser headerParser()...}
```

```
@Bean
@ConditionalOnMissingBean //当不存在当前类型的bean时，才声明该bean
public HeaderParser headerParser(){...}
```

```
@Bean@ConditionalonProperty(name =name",havingvalue ="itheima") //配置文件中存在对应的属性和值，才注册bean到IOC容器
public HeaderParser headerParser(){...}
```





#### 8.3.2.2 案例（自定义starter）

场景：

* 在实际开发中，经常会定义一些公共组件，提供给各个项目团队使用。而在SpringBoot的项目中，一般会将这些公共组件封装为SpringBoot的starter

需求：

* 需求：自定义aliyun-oss-spring-boot-starter，完成阿里云0SS操作工具类Aliyun0sSUtils 的自动配置
* 目标：引入起步依赖引入之后，要想使用阿里云OSS，注入AliyunssUtils直接使用即可。

步骤：

* 创建aliyun-ossospring-boot-starter模块
* 创建aliyun-oss-spring-boot-autoconfigure 模块，在starter中引入该模块
* 在 alivun-oss-soring-boot-autoconfiaure 模快中的定义自动配置功能，并定义自动配置文件 META-NF/sprina/xxxx.imports

```
alivun-oss-sprina-boot-starter
alivun-oss-spring-boot-autoconfiqure
```



# 9 总结

![image-20230529214246787](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230529214246787.png)



# 10 Maven高级

## 10.1 分模块设计与开发

* 为什么？将项目按照功能拆分成若干个子模块，方便项目的管理维护、拓展，也方便，模块间的相互调用，资源共享

![image-20230529214837317](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230529214837317.png)



* 实践
  * 创建maven模块 alias-pojo，存放实体类
  * 创建maven模块 alias-utils，存放相关工具类
* 注意事项：分模块开发需要先针对模块功能进行设计，再进行编码。不会先将工程开发完毕，然后进行拆分





## 10.2 继承与聚合

### 10.2.1 继承

#### 10.2.1.1 继承关系

* 概念：继承描述的是两个工程间的关系。，与Java中的继承相似，子工程可以继承父工程中的配置信息，常见于依赖关系的继承

* 作用：简化依赖配置、统一管理依赖
* 实现：<parent> ... </parent>



打包方式：

* jar: 普通模块打包，springboot项目基本都是iar包 (内嵌tomcat运行)
* war: 普通web程序打包，需要部署在外部的tomcat服务器中运行
* pom: 父工程或聚合工程，该模块不写代码，仅进行依赖管理



* 继承关系实现
  * 创建maven模块 tlias-parent ，该工程为父工程，设置打包方式pom(默认jar)。
  * 在子工程的pom.xml文件中，配置继承关系。
  * 在工程中配置各个工程共有的依赖(子程会自动继承父工程的依赖)

```xml
父工程：
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.7.5</version>
  <relativePath/>
</parent>

<groupId>com.itheima</groupId>
<artifactId>alias-parent</artifactId>
<version>1.0-SNAPSHOT</version>
<packaging>pom</packaging>

子工程：
  <parent>
    <groupId>com.itheima</groupId>
    <version>1.0-SNAPSHOT</version>
    <artifactId>alias-parent</artifactId>
    <relativePath>../alias-parent/pom.xml</relativePath>
  </parent>
```



* 注意：若父子工程都配置了同一个依赖的不同版本，以子工程的为准
* 工程结构：（第二种更清晰）

![image-20230530165006532](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230530165006532.png)



#### 10.2.1.2 版本锁定

* 在maven中，可以在父工程的pom文件中通过<dependencyManagement>来统一管理依赖版本

* 注意：子工程引入依赖时，无需指定<version>版本号，父工程统一管理。变更依赖版本，只需在父工程中统一变更

* 自定义属性/引用属性

  ```
  <properties>
  <lombok.version>1.18.24</lombok.version><jjwt.version>0.9.0</jjwt.version>
  </properties>
  ```



<dependencyManagement> 与 <dependencies>的区别是什么?

* <dependencies>是直接依赖,在父工程配置了依赖,子工程会直接继承下来
* <dependencyManagement> 是统一管理依赖版本,不会直接依赖，还需要在子工程中引入所需依赖(无需指定版本)



### 10.2.2 聚合

* 聚合：将多个模块组织成一个整体，同时进行项目的构建

* 聚合工程：

  一个不具有业务功能的“空”工程（有且仅有一个pom文件）

* 作用：快速构建项目（无需根据依赖关系手动构建，直接在聚合工程上构建即可）



实现：

maven中可以通过<modules>设置当前聚合工程所包含的子模块名称

```
<modules>
    <module>../tlias-pojo</module>
    <module>../tlias-utils</module>
    <module>../tlias-web-management</module>
</modules>
```



* 注意：

  聚合工程中所包含的模块，在构建时，会自动根据模块间的依赖关系设置构建顺序，与聚合工程中模块的配置书写位置无关



* 作用
  * 聚合用于快速构建项目
  * 继承用于简化依赖配置、统一管理依赖
* 相同点:
  * 聚合与继承的pom.xm1文件打包方式均为pom，可以将两种关系制作到同一个pom文件中
  * 聚合与继承均属于设计型模块，并无实际的模块内容
* 不同点:
  * 聚合是在聚合工程中配置关系，聚合可以感知到参与聚合的模块有哪些
  * 继承是在子模块中配置关系，父模块无法感知哪些子模块继承了自己



## 10.3 私服

### 10.3.1 介绍

* 它是架设在局域网内的仓库服务，用来代理位于外部的中央仓库，用于解决团队内部的私服是一种特殊的远程仓库，资源共享与资源同步问题



* 依赖查找顺序：
  * 本地仓库
  * 私服
  * 中央仓库
* 注意：私服在企业项目开发中，一个项目/公司，只需要一台即可(无需我们自己搭建，会使用即可)。



### 10.3.2 资源上传与下载

![image-20230530194000006](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230530194000006.png)

项目版本：

* RELEASE(发行版本): 功能趋于稳定、当前更新停止，可以用于发行的版本，存储在私服中的RELEASE仓库中。
* SNAPSHOT(快照版本):功能不稳定、尚处于开发中的版本，即快照版本，存储在私服的SNAPSHOT仓库中。



1. 设置私服的访问用户名/密码 (settings.xm1中的servers中配置)

```xml
例：
<server>
    <id>maven-releases</id>
    <username>admin</usernamex
    <password>admin</password>
</server>
<server>
    <id>maven-snapshots</id>
    <username>admin</username>
    <password>admin</password>
</server>
```



2. IDEA的maven工程的pom文件中配置上传(发布)地址

```
<distributionManagement>
<repository>
<id>maven-releases</id>
<url>http://192.168.150.101:8081/repository/maven-releases/</url>
</repository>


<snapshotRepository>
<id>maven-snapshots</id><url>http://192.168.15.101:8081/repository/maven-snapshots/</url>
</snapshotRepository>
</distributionManagement>
```



3. 设置私服依赖下载的仓库组地址 (settings.xml中的mirrors、profiles中配置)

```
<mirror>
<id>maven-public</id>
<mirrorOf>*</mirrorOf>
<url>http://192.168.150.101:8081/repository/maven-public/</url>
</mirror>
```

```xml
<profile>
    <id>allow-snapshots</id>
    <activation>
    	<activeByDefault>true</activeByDefault>
    </activation>
    
<repositories>
	<repository>
    <id>maven-public</id>
    <url>http://192.168.150.101:8081/repository/maven-public/</url>
   	 <releases>
			<enabled>true</enabled>
  	  </releases>
       	 <snapshots>
           	 <enabled>true</enabled>
        	</snapshots>
		</repository>
	</repositories>
</profile>
```

![image-20230530201310835](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230530201310835.png)







