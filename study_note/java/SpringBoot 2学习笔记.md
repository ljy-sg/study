# 					SpringBoot 2学习笔记

# 1 基础入门

## 1.1 Spring与SpringBoot

SpringMVC执行流程：

![image-20230804212828098](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230804212828098.png)



Spring框架：

Spring框架解决了企业级的开发的复杂性，它是一个容器框架，用于装java对象（Bean），使程序间的依赖关系交由容器统一管理，松耦合，提高了可测试性和维护效率，Spring主要为我们做了两件事，一省去了我们创建对象的操作，二声明了属性赋值。

对于我来说，Spring框架就是提供了IOC容器、控制反转、依赖注入以及一些模块，简化了大量的代码，便捷了程序的开发，节省了开发时间，提高了效率。Spring框架为我们提供了全面的基础框架，但是Spring框架的配置是一项问题，使用一项第三方jar包的时候都需要配置相关的XML文件，有时候配置起来十分麻烦，降低了编程效率。编写基于ssm框架的项目需要配置Tomcat，把封装好的war包放到Tomcat容器运行。Spring可以接管Web层、业务层、dao层、持久层的组件，可以配置各种bean。

总体而言，Spring框架就是为我们解决开发中的基础性问题，使我们开发者可以更加专注于应用程序的开发，并且Spring框架是轻量级框架，扩展性强，非侵入式框架，消耗的资源少，占用的空间小，运行和部署简单。





Spring Boot框架：

Spring Boot是一个微服务框架，延续了Spring框架的核心思想IOC和AOP，简化了应用的开发和部署。

在我看来Spring Boot框架是对Spring框架的补充，它消除了Spring框架配置XML的麻烦事，完善了Spring框架的开发环境，使我们可以更加高效的完成编程，并且为我们提供了 spring-boot-starter-web 依赖，这个依赖包含了Tomcat和springmvc等一系列的web依赖（无需部署war文件）。

以前我们SpringMVC要配置properties文件的时候需要写大量的配置，现在用Spring Boot只需要导入相关依赖，然后写两句话就可以配置完Web程序，并且还提供了@Configuration来替代XML配置。

Spring 和 Spring Boot的最大的区别在于Spring Boot的自动装配原理：
比如：
我们使用Spring创建Web程序时需要导入几个Maven依赖，而Spring Boot只需要一个Maven依赖来创建Web程序，并且Spring Boot还把我们最常用的依赖都放到了一起，现在的我们只需要spring-boot-starter-web这一个依赖就可以完成一个简单的Web应用。



以前用Spring的时候需要XML文件配置开启一些功能，现在Spring Boot不用XML配置了，只需要写一个配置类（@Configuration和继承对应的接口）就可以继续配置。



Spring Boot会通过启动器开启自动装配功能以@EnableAutoConfiguration扫描在spring.factories中的配置，然后通过@XxxxautoConfiguration进行扫描和配置所需要的Bean，自动的扫描SpringBoot项目引入的Maven依赖，只有用到的才会被创建成Bean，然后放到IOC容器内



## 1.2 Spring的生态

https://spring.io/projects/spring-boot

覆盖了：

* web开发
* 数据访问
* 安全控制
* 分布式
* 消息服务
* 移动开发
* 批处理.........





## 1.3 Spring5重大升级

### 1.3.1 响应式编程

[(4条消息) Java：理解java响应式编程_netyeaxi的博客-CSDN博客](https://blog.csdn.net/netyeaxi/article/details/112621600)





### 1.3.2 内部源码设计

基于Java8的一些新特性，如接口默认实现。重新设计源码架构



### 1.3.3 为什么使用SpringBoot

能快速生产级别的Spring应用





## 1.4 SpringBoot优点

* Create stand-alone Spring applications
  * 创建独立Spring应用
* Embed Tomcat, Jetty or Undertow directly (no need to deploy WAR files)。
  * 内嵌web服务器
* Provide opinionated 'starter' dependencies to simplify your build configuration
  * 自动starter依赖，简化构建配置
* Automatically configure Spring and 3rd party libraries whenever possible。
  * 自动配置Spring以及第三方功能
* Provide production-ready features such as metrics, health checks, and externalized configuration。
  * 提供生产级别的监控、健康检查及外部化配置
* Absolutely no code generation and no requirement for XML configuration。
  * 无代码生成、无需编写XML



SpringBoot是整合Spring技术栈的一站式框架

SpringBoot是简化Spring技术栈的快速开发脚手架



## 1.5 SpringBoot缺点

* 人称版本帝，迭代快，需要时刻关注变化
* 封装太深，内部原理复杂，不容易精通



## 1.6 时代背景

### 1.6.1 微服务

James Lewis and Martin Fowler (2014) 提出微服务完整概念。https://martinfowler.com/microservices/



* 微服务是一种架构风格
* 一个应用拆分为一组小型服务
* 每个服务运行在自己的进程内，也就是可独立部署和升级
* 服务之间使用轻量级HTTP交互
* 服务围绕业务功能拆分
* 可以由全自动部署机制独立部署
* 去中心化，服务自治。服务可以使用不同的语言、不同的存储技术



### 1.6.2 分布式

分布式的困难

* 远程调用
* 服务发现
* 负载均衡
* 服务容错
* 配置管理
* 服务监控
* 链路追踪
* 日志管理
* 任务调度



分布式的解决

* SpringBoot + SpringCloud



### 1.6.3 云原生

原生应用如何上云。Cloud Native



**上云的困难**

* 服务自愈
* 弹性伸缩
* 服务隔离
* 自动化部署
* 灰度发布
* 流量治理
* ....



上云的解决：





## 1.7 如何学习SpringBoot

### 1.7.1 官网文档架构

[Spring Boot](https://spring.io/projects/spring-boot)



## 1.2 SpringBoot2入门

### 1.2.1 系统要求

* Java 8 & 兼容Java14
* Maven 3.3+
* idea 2019.1.2 



maven设置：

```
<mirrors>
	<mirror>
		<id>nexus-aliyun</id>
		<mirrorOf>central</mirrorOf><name>Nexus aliyun</name>
		<url>http://maven.aliyun.com/nexus/content/groups/public</url>
	</mirror>
</mirrors>

<profiles>
	<profile>
		<id>jdk-1.8</id>
		<activation>
		<activeByDefault>true< /activeByDefault><jdk>1.8</jdk>
		</activation>

	<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>			<maven.compiler,compilerVersion>1.8</maven.compiler.compilerVersion>		</properties>
	</profile>
</profiles>
```





### 1.2.2 Hello world

需求：浏览器发送/hello请求，响应Hello，Spring Boot 2

1.2.2.1 创建maven工程

1.2.2.2 引入依赖

```
<parent>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-parent</artifactId>
     <version>2.7.14</version>
     <relativePath/> <!-- lookup parent from repository -->
</parent>
```

```
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

1.2.2.3 创建主程序

1.2.2.4 编写业务

1.2.2.5 测试

1.2.2.6 简化配置

1.2.2.7 简化部署

```
<plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
      <configuration>
</plugin>      
```



## 1.3 了解自动配置原理

### 1.3.1 依赖管理

* 父项目做依赖管理

```
依赖管理：
<parent>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-parent</artifactId>
      <version>2.7.14</version>
      <relativePath/> <!-- lookup parent from repository -->
</parent>

几乎声明了所有开发中常用的jar的版本号，自动版本仲裁机制
```



* 开发导入starter场景启动器

```
1、见到很多spring-boot-starter-*，*就某种场景
2、只要引入starter，这个场景的所有常规需要的依赖我们都自动引入
3、SpringBoot所有支持的场景
https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-starter

4、见到的 *-spring-boot-starter，第三方为我们提供的简化开发的场景启动器
```

* 无需关注版本号，自动版本仲裁

```
1、引入依赖默认都可以不写版本
2、引入非版本仲裁的jar，要写版本号
```

* 可以修改版本号

```
1、查看Spring-boot-dependencies里面规定当前依赖的版本 用的key
2、再当前项目里面重写配置
	<properties>
		<mysql.version>5.1.43</mysql.version>
	</properties>
```



### 1.3.2 自动配置

* 自动配置好Tomcat
  * 引入Tomcat依赖
  * 配置Tomcat

* 自动配好SpringMVC
  * 引入springMVC全套组件
  * 自动配好SpringMVC常用组件（功能）
* 自动配好Web常见功能，如：字符编码问题
  * SpringBoot帮我们配置好了所有web开发的常见场景

* 默认的包结构
  * 主程序所在包及其下面的所有子包里面的组件都会被默认扫描进来
  * 无需以前的包扫描配置
  * 想要改变扫描路径，@SpringBootApplication(scanBasePackages="com.atguigu")
    * 或者@ComponentScan指定扫描路径

```
@SpringBootApplication
等同于
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan("com.atguigu.boot")
```

* 各种配置拥有默认值

  * 默认的值最终都是映射到MutipartProperties
  * 配置文件的值最终会绑定每个类上，这个类会在容器中创建对象

* 按需加载所有自动配置项

  * 非常多的starter
  * 引入了哪些场景这个场景的自动配置才会开启
  * SpringBoot所有的自动配置功能都在

  ```
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-autoconfigure</artifactId>
  都在spring-boot-autoconfigure包里面
  ```

* .......



### 1.3.3 容器功能

#### 1.3.3.1 组件添加

@Configuration

* 基本使用
* Full模式与Lite模式
  * 示例
  * 最佳实战
    * 配置类组件之间无依赖关系用Lite模式加速容器启动过程，减少判断
    * 配置类组件之间有依赖关系，方法被调用得到之前单实例组件，用Full模式

```
@Configuration //告诉SpringBoot这是一个配置类 == 配置文件
1、配置类里面使用@Bean标注在方法上给容器注册组件，默认也是单实例的
2、配置类本身也是组件
3、proxyBeanMethods：代理bean的方法

@Bean  //给容器中添加组件，以方法名作为组件的id。返回类型就是组件类型。返回的值，就是组件在容器中的实例


4、@Import({User.class,DBHelper.class})
   给容器中自动创建出这两个类型的组件，默认组件的名字就是全类名
```

@Bean、@Component、@Controller、@Service、@Repository

@ComponentScan、@Import

@lmport 高级用法: htps://www.bilibili.com/video/BV1gW411W7wy?p=8



@Conditional

条件装配，满足Conditional指定的条件，则进行组件注入



#### 1.3.3.2 原生配置文件引入

@ImportResource

@ImportResource("classpath:beans.xml")导入Spring的配置文件





#### 1.3.3.3 配置绑定

如何使用Java读取到properties文件中的内容，并且把他封装到JavaBean中。，以供随时使用



只有在容器中的组件，才会拥有SpringBoot提供的强大功能

@ConfigurationProperties（只有这个是报错的，只实现属性绑定但是没有将组件加入到容器中）



@EnableConfigurationProperties + @ConfigurationProperties（写在配置类中）

1、开启属性配置绑定功能

2、把组件自动注册到容器中



@Component + @ConfigurationProperties（写在与配置类属性相关的类中）





## 1.4 自动配置原理入门

### 1.4.1 引导加载自动配置类

```
@EnableAutoConfiguration
```

```
1、@SpringBootConfiguration
@Configuration 代表当前是一个配置类

2、@ComponentScan
指定扫描哪些，Spring注解

3、@EnableAutoConfiguration = @AutoConfigurationPackage + @Import(AutoConfigurationImportSelector.class)
```

* @AutoConfigurationPackage 自动配置包（指定默认包规则）

```
Import(AutoConfigurationPackages.Registrar.class) //给容器中导入一个组件
public @interface AutoConfigurationPackage {}

//利用Registrar给容器导入一系列组件
//将指定的一个包下的所有组件导入进来？MainApplication所在包下
```

* @Import(AutoConfigurationImportSelector.class)

```
1、利用getAutoConfigurationEntry(annotationMetadate);给容器中批量导入一些组件
2、调用List<String> configurations = getCandidateConfigurations(annotationMetadate,attributes)获取到所有需要导入到容器中的组件
3、利用工厂加载 Map<String,List<String>> loadSpringFactories(@Nullable Classlader classloader)：得到所有的组件
4、从MEATA-INF/spring.factories位置来加载一个文件
   默认扫描我们当前系统里面所有MEATA-INF/spring.factories位置的文件
   spring-boot-autoconfigure-2.3.4.RELEASE.jar里面也有MEATA-INF/spring.factories
```

文件里面写死了spring-boot一启动就要给容器中加载所有的配置类



### 1.4.2 按需开启自动配置项

```
虽然我们127个场景的所有自动配置启动的时候默认全部加载.xxxAutoConfiguration
按照条件装配原则(@Conditional)，最终会按需配置
```





### 1.4.3 定制化修改自动配置

```java
@Bean
@ConditionalOnBean(MultipartResolver.class) //容器中有这个类型组件
@ConditionalOnMissingBean(name = DispatcherServlet.MULTIPART_RESOLVER
_BEAN_NAME) //容器中没有这个名字mutipartResolver
public MultipartResolver multipartResolver(MultipartResolver resolver){
 //给@Bean标注的方法传入了对象参数，这个参数的值就会从容器中找
    //SpringMVC mutipartResolver。防止有写用户配置的文件上传解析器不符合规范
    //Detect if the user has created a MultipartResolver but named it incorrectly
 return resolver;
 }
给容器中加入了文件上传解析器;
```



SpringBoot默认会在底层配好所有的组件，但是如果用户自己配置了以用户的优先

```java
@Bean
@ConditionalOnMissingBean
public CharacterEncodingFilter characterEncodingFilter() {
    
}
```



总结：

* SpringBoot先加载所有的自动配置类。xxxAutoConfiguration
* 每个自动配置类按照条件进行生效，默认都会绑定配置文件指定的值。xxxProperties里面拿，xxxProperties和配置文件进行了绑定
* 生效的配置类就会给容器中装配很多组件
* 只要容器中有这些组件，相当于这些功能就有了
* 定制化配置
  * 用户直接自己@Bean替换底层的组件
  * 用户去看这个组件是获取的配置文件扫描值就去修改

xxxxxAutoConfiguration --->组件 --->xxxxProperties里面拿值 ----> application.properties





### 1.4.4 最佳实践

* 引入场景依赖
  * [Spring Boot](https://spring.io/projects/spring-boot)
* 查看自动配置了哪些(选做)
  * 自己分析，引入场景对于的自动配置一般都生效了
  * 配置文件中debug=true开启自动配置报告。Negative(不生效)\Positive（生效）
* 是否需要修改
  * 参照文档修改配置项
    * [Developing with Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/using.html#using.build-systems.starters)
    * 自己分析。xxxProperties绑定了配置文件的哪些
  * 自定义加入或者替换组件
    *  @Bean、@Component......
  * 自定义器xxxxxCustomizer
  * ........



## 1.5 开发小技巧

### 1.5.1 Lombok

简化JavaBean开发

```xml
<dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <optional>true</optional>
</dependency>

并搜索安装lombok插件
```



### 1.5.2 dev-tools

```xml
<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-devtools</artifactId>
</dependency>
```



Ctrl + F9；



### 1.5.3 Spring Initailizer（项目初始化向导）



# 2 SpringBoot2核心技术-核心功能

## 2.1 配置文件

### 2.1.1 文件类型

* properties

*  yaml



### 2.1.1 yaml简介

YAML 是“YAML Ain't Markup Language”(YAML 不是一种标记语言)的递归缩写。在开发的这种语言时，YAML的意思其实是:“Yet Another Markup Language”(仍是一种标记语言)。



非常适合用来做以数据为中心的配置文件





### 2.1.2 基本语法

* key: value; kv之间有空格
* 大小写敏感
* 使用缩进表示层级关系
* 缩进不允许使用tab，只允许空格
* 缩进的空格数不重要，只要相同层级的元素左对齐即可
* '#'表示注释
* "与”""表示字符串内容 会被 转义/不转义
  * 单引号会将\n作为字符串输出    双引号会将\n作为换行输出





### 2.1.3 数据类型

* 字面量：单个的、不可再分的值。date、boolean、string、number、null

```
k: v
```

* 对象：键值对的集合。map、hash、set、object

```
行内写法:    k: {k1:v1,k2:v2,k3:v3}
#或
k:
  k1: v1
  k2: v2
  k3: v3
```

* 数组：一组按次序排序的值。array、list、queue

```
行内写法， k: [v1,v2,v3]
#或者
k: 
 - v1
 - v2
 - v3
```



## 2.2 Web开发

### 2.2.1 SpringMVC自动配置概览

Spring Boot provides auto-configuration for Spring MVC that works well with most applications.(大多场我们都无需自定义配置)

The auto-configuration adds the following features on top of Spring’ s defaults:

* Inclusion of ContentNegotiatingViewResolver and BeanNameViewResolver beans。
  * 内容协商视图解析器和BeanName视图解析器
* Support for serving static resources, including support for WebJars (covered later in this document))。
  * 静态资源 (包括webjars)
* Automatic registration of Converter, GenericConverter, and Formatter beans.
  * 自动注册 Converter， GenericConverter，Formatter
* Support for HttpMessageConverters (covered later in this document)。
  * 支持 HttpMessageConverters (后来我们配合内容协商理解原理)
* Automatic registration of MessageCodesResolver (covered later in this document)。 
  * 自动注册 MessageCodesResolver (国际化用)
* Static index.html support
  * 静态indexhtml 页支持
* Custom Favicon support (covered later in this document).
  * 自定义 Favicon
* Automatic use of a ConfigurablewebBindingInitializer bean (covered later in this document.。
  * 自动使用 configurablewebBindingInitializer ， (DataBinder负责将请求数据绑定到JavaBean上)



### 2.2.2 简单功能分析

#### 2.2.2.1 静态资源访问

静态资源目录

只要静态资源放在类路径下：called /static(or /public or /resources or /META-INF/resources)

访问：当前项目根路径/ + 静态资源名



原理：静态映射/**

请求进来，先去找controller看能不能处理，不能处理的所有请求又都交给静态资源处理器，静态资源也找到404



静态资源访问前缀

默认无前缀

```
spring:
  mvc:
   static-path-pattern: /res/**
```

当前项目 + static-path-pattern+静态资源名 = 静态资源文件夹下找



webjar

自动映射

[WebJars - Web Libraries in Jars](https://www.webjars.org/)

```
<dependency>
     <groupId>org.webjars</groupId>
     <artifactId>jquery</artifactId>
     <version>3.5.1 </version>
</dependency>
```

访问地址：http://localhost:8080/webjars/jquery/3.5.1/jquery.js  后面地址要按照依赖里面的包路径



#### 2.2.2.2 欢迎页支持

* 静态资源路径下 index.html
  * 可以配置静态资源路径
  * 但是不可以配置静态资源的访问前缀，否则导致 index.html不能被默认访问

```
spring:
 # mVc:
 #  static-path-pattern: /res/**     这个会导致welcome page功能失效
   resources:
      static-locations: [classpath:/haha/]
```

* controller能处理/index



#### 2.2.2.3 自定义Favicon

```
spring:
 # mVc:
 #  static-path-pattern: /res/**     这个会导致Favicon功能失效
```





#### 2.2.2.4 静态资源配置原理

* SpringBoot启动默认加载 xxxAutoConfiguration 类(自动配置类)
* SpringMVC功能的自动配置类 WebMvcAutoConfiguration，生效
* 给容器中配了什么
* 配置文件的相关属性和xxx进行了绑定，WebMvcProperties==spring.mvc、ResourceProperties==spring.resources



配置类只有一个有参构造器

* 有参构造器所有参数的值都会从容器中确定

```
//ResourceProperties resourceProperties;   获取和spring.resources绑定的所有的值的对象
//WebMvcProperties mvcProperties 获取和spring.mvc绑定的所有的值的对象 //ListableBeanFactory beanFactory Spring的beanFactory
//HttpMessageConverters 我到所有的HttpMessageConverters
//ResourceHandlerRegistrationCustomizer 找到 资源处理器的自定义器==== =====
//DispatcherServletPath
//ServletRegistrationBean   给应用注册Servlet、Filter....
```

```
spring:
 # mVc:
 #  static-path-pattern: /res/**     这个会导致Favicon功能失效
 
  resources:
    static-location:[classpath:/haha/]
    add-mappings:false  禁用所有静态资源规则
```

```
@ConfigurationProperties(prefix = "spring.resources", ignoreUnknownFields = false)
public class ResourceProperties{
	private static final String[] CLASSPATH RESOURCE LOCATIONS =  "classpath:/META-INF/resources/""classpath:/resources/"，"classpath:/static/"，"classpath:/public/" };


/**

*Locations of static resources. Defaults to classpath:[/META-INF/resources/,* /resources/，/static/，/public/].

**/
private String[] staticLocations = CLASSPATH_RESOURCE_LOCATIONS
```



HandlerMapping：处理器映射。保存了每一个Handler能处理哪些请求。



favicon

浏览器会发送/fvicon.ico 请求获取到图标，整个session期间不再获取



### 2.2.3 请求参数处理

#### 2.2.3.1 请求映射

* @xxxMapping：
* Rest风格支持（使用HTTP请求方式动词来表示对资源的操作）
  * 以前：/getUser  获取用户   /deleteUser 删除用户    /editUser 修改用户   /saveUser 保存用户
  * 现在：/user  GET-获取用户     Delete-删除用户    PUT-修改用户    POST-保存用户
  * 核心Filter：HiddenHttpMethodFilter
    * 用法：表单method=post，隐藏域_method=put
    * SpringBoot中手动开启

```
mvc:
	hiddenmethod:
	   filter:
		enabled: true
```



Rest原理（表单提交要使用Rest的时候）

* 表单提交会带上_method=PUT
* 请求过来被HiddenHttpMethodFilter拦截
  * 请求是否正常，并且是POST
    * 获取到_method的值
    * 兼容以下请求：PUT.DELETE.name()，HttpMethod.PATCH.name()
    * 原生request（post），包装模式requestWrapper重写了getMethod方法，返回的是传入的值
    * 过滤器链放行的时候用wrapper。以后的方法调用getMethod是调用requestWrapper的



Rest使用客户端工具

* 如PostMan直接发送Put、delete等方式请求，无需Filter

```
spring:
  mvc:
	hiddenmethod:
	   filter:
		enabled: true   #开启页面表单的Rest功能
```



请求映射原理：

![image-20230802203755141](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230802203755141.png)

RequestMappingHandlerMapping：保存了所有@RequestMapping和handler的映射规则



所有的请求映射都在HandlerMapping中

* Spring自动配置欢迎页的HandlerMapping，访问/能访问到index.html
* SpringBoot自动配置了默认的RequestMappingHandlerMapping
* 请求进来，挨个尝试所有的HandlerMapping看是否有请求信息
  * 如果有就找到这个请求对于的handler
  * 如果没有就下一个HandlerMapping
* 我们需要一些自定义的映射处理，我们也可以自己给容器中放HandlerMapping





#### 2.2.3.2 普通参数与基本注解

* 注解：

  @PathVariable、@RequestHeader、@ModelAttribute、@RequestParam、@MatrixVariable、@Cookievalue、 @ReqguestBody



* Servlet API：

  WebRequest，ServletRequestMultipartRequest HttpSession,javax.servlet.http.PushBuilder，Principal,inputStream.Reader、HttpMethod、Locale、TimeZone、Zoneld

```
ServletRequstMethodArgumentResolver 以上的部分参数
-参数解析器的
```



* 复杂参数:
  Map、Model（map、model里面的数据会被放在request的请求域 request.setAttribute）、Errors/BincinaResult Mode、RedirectAttributes(重定向携带数据)、ServletResponse（response）、SessionStatus、UriComponentsBuilder、ServletUriComponentsBuilder

```
Map<String,Object> map,Model model，HttpServletRequest request 都是可以给request域中放数据

request.getAttribute();
```

Map类型的参数，会返回mavContainer.getModel();       -------->   BindingAwareModelMap 是Model 也是Map mavContainer.getModel();获取到值的

![image-20230804155724474](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230804155724474.png)

* 自定义对象参数:

  可以自动类型转换与格式化，可以级联封装



页面开发，cookie禁用了，session里面的内容怎么使用;

session.set(a,b)---> jsessionid ---> cookie ----> 每次发请求携带url重写;

 /abc;jsesssionid=xxxx 把cookie的值使用矩阵变量的方式进行传递



1、语法: /cars/sell;Low=34;brand=byd,audi,yd

2、SpringBoot默认是禁用了矩阵变量的功能

​     手动开启:原理。对于路径的处理。UrLPathHeLper进行解析。       removeSemicolonContent (移除分号内容)支持知阵变量的

3、矩阵变量必须有urL路径变量才能被解析

```java
@Bean
public WebMvcConfigurer webMvcConfigurer(){
return new WebMvcConfigurer(){
@override
public void configurePathMatch(PathMatchConfigurer configurer) {
UrlPathHelper urlPathHelper = new UrlPathHelper();
// 不移除:后面的内容。矩阵变量功能就可以生效urlPathHelper.setRemoveSemicolonContent(false);
configurer.setUrlPathHelper(urlPathHelper);
}
};
}
```





#### 2.2.3.3 参数处理原理

* HandlerMapping中找到能处理请求的Handler（Controller.method）
* 为当前Handler找一个适配器 HandlerAdapter；RequestMappingHandlerAdapter



HandlerAdapter

1-支持方法上标注@RequestMapping

2-支持函数式编程的

  

4 执行目标方法

```
//Actually invoke the handler .
//DispatcherServlet -- doDispatch
mv =ha.handle(processedRequest,response,mappedHandler.getHandler());
```

```
mav = invokeHandlerMethod(request,response,handlerMethod); //执行目标方法


//ServletInvocableHandlerMethod
Object returnValue = invokeFhrRequest(webRequest, mavContainer, providedArgs);

//获取方法的参数值
Object[] args = getMethodArgumentValues(request, mavContainer，providedArgs);
```



5.1 参数解析器-HandlerMethodArgumentResolver

参数解析器：确定要执行的目标方法的每一个参数的值是什么

SpringMVC目标方法能写多少种参数类型，取决于参数解析器

* 判断当前解析器是否支持解析这种参数
* 支持就调用resolveArgument



如何确定目标方法每一个参数的值：InvocableHandlerMethod



5.2 挨个判断所有参数解析器哪个支持解析这个参数

```java
@Nullable
private HandlerMethodArgumentResolver getArgumentResolver(MethodParameter parameter){
HandlerMethodArgumentResolver result = this.argumentResolverCacheget(parameter);
if (result == nul1) {
   for (HandlerMethodArgumentResolver resolver : this.argumentResolvers){
      if (resolver.supportsParameter(parameter)){
         result = resolver;
         this.argumentResolverCache.put(parameter，result);
         break;
         }
       }
    }   
return result;
}
```



5.2 解析这个参数的值：

```
调用各自 HandlerMethodArgumentResolver 的 resolveArgument 方法即可
```



5.3 自定义类型参数封装 POJO

ServletModelAttributeMethodProcessor 这个参数处理器支持

是否为简单类型



WebDataBinder binder = binderFactory.createBinder(webRequest, attribute, name)；

WebDataBinder：web数据绑定器，将请求参数的值绑定到指定的JavaBean里面

WebDatabinder利用它里面的Converters 将请求数据转成指定的数据类型，再次封装到JavaBean中



GenericConversionService: 在设置每一个值的时候，找它里面的所有converter那个可以将这个数据类型（request带来参数的字符串）转换到指定的类型（JavaBean -- Integer）

byte -----> file

```
未来我们可以给WebDataBinder里面放自己的Converter;
private static final class StringToNumber<T extends Number> implements Converter<String,T>
```

@Functionallnterfacepublic interface Converter<S, T>



6 目标方法执行完成

将所有的数据都放在ModelAndViewContainer；包含要去的页面地址View。还包含Model数据



7 处理派发结果

processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);



renderMergedOutputModel(mergedModel, getRequestToExpose(request), response);

```
InternalResourceView
```

```
暴露模型作为请求域属性
// Expose the model object as request attributes
   exposeModelAsRequestAttributes(model，request);
```



#### 2.2.3.4 自定义对象参数：

可以自动类型转换与格式化，可以级联封装

自定义converter：

```java
@Override
public void addFormatters(FormatterRegistry registry) (registry.addConverter(new Converter<String， Pet>() {
	@Override
	public Pet convert(String source) {
		// 啊猫,3
		if(!stringUtils.isEmpty(source)){
		Pet pet = new Pet();
		String[] split = source.split( regex:pet .setName(split[0]);
		pet.setAge(Integer.parseInt(split[1]));
		return pet;
		}
		return null;
}
});
}
```





### 2.2.4 响应数据与内容协商

![image-20230804174816421](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230804174816421.png)

#### 2.2.4.1 响应JSON

1、jackson.jar + @ResponseBody





给前端自动返回json数据；



* 返回值解析器

```
try {
	this.returnValueHandlers.handleReturnValue(
		returnValue,getReturnValueType(returnValue),mavContainer, webRequest);
}
```



```java
	@Override
	public void handleReturnValue(@Nullable Object returnValue, MethodParameter returnType,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {

        //selectHandler()实现在下面
		HandlerMethodReturnValueHandler handler = selectHandler(returnValue, returnType);
		if (handler == null) {
			throw new IllegalArgumentException("Unknown return value type: " + returnType.getParameterType().getName());
		}
        //开始处理
		handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
	}
```

HTTPMessageConverter原理：

* HttpMessageConverter看是否支持将此Class类型的对象，转为MediaType类型的数据

例子：Person对象转为JSON，或者JSON转为Person

* 默认的MessageConverter

0 - 只支持Byte类型的

1 - String

2 - String

3 - Resource

4 - ResourceRegion

5 - DOMSource.class \ SAXSource.class \ StAXSource.class \ StreamSource.class \ Source.class

6 - MultiValueMap

7 - true

8 - true

9 - 支持注解方式xml处理的



最终MappingJackson2HttpMessageConverter  把对象转化为JSON（利用底层的jackson的objectMapper），返回给response



原理：

一、返回值处理器先判断是否支持这种类型的返回值   supportReturnType

二、返回值处理器调用  handleReturnValue 进行处理

三、RequestResponseBodyMethodProcessor 可以处理返回值标了@ResponseBody注解的

* 利用MessageConverters 进行处理 将数据写为json
  * 内容协商（浏览器默认会以请求头的方式告诉服务器他能接受什么样的内容类型）
  * 服务器最终根据自己自身的能力，决定服务器能生产出什么样内容类型的数据
  * SpringMVC会挨个遍历所有容器底层的 HttpMessageConverter，看谁能处理
    * 得到MappingJackson2HttpMessageConverter 可以将对象写为JSON
    * 利用MappingJackson2HttpMessageConverter 将对象转为JSON再写出去





2.2.4.2 内容协商

根据客户端接受能力不同，返回不同媒体类型的数据

一、引入xml依赖

```
<dependency>
    <groupId>com.fasterxml.jackson.dataformat</groupId>     <artifactId>jackson-dataformat-xml</artifactId></dependency>
```



二、postman分别测试返回json和xml

只需要改变请求头种Accept字段。Http协议中规定的，告诉服务器本客户端可以接受的数据类型



三、开启浏览器参数方式内容协商功能

为了方便内容协商，开启基于请求参数的内容协商功能

```
spring:
   contentnegotiation:
        favor-parameter: true  #开启请求参数内容协商
```

发请求： http://localhost:8080/test/person?format=json

http://localhost:8080/test/person?format=json

确定客户端接受什么样的内容类型：

1、Parameter策略优先确定是要返回json数据（获取请求头中的format的值）





四、内容协商原理

* 判断当前响应头中是否已经又确定的媒体类型 MediaType
* 获取客户端（Postman、浏览器）支持接受的内容类型。（获取客户端Accept请求头字段）【application/xml】
  * contentNegotiationManager内容协商管理器  默认使用基于请求头的策略
  * HeaderContentNegotiationStrategy 确定客户端可以接收的内容类型
* 遍历循环所有当前系统的MessageConverter，看谁支持操作这个对象（Person）
* 找到支持操作Person的Converter，把converter支持的媒体类型统计出来
* 客户端需要【application/xml】。服务端能力【10种】
* 进行内容协商的最佳匹配媒体类型
* 用 支持 将对象转化为 最佳匹配媒体类型的converter .调用它进行转化



SpringMVC到底支持哪些返回值

```
ModelAndView
Model
View
ResponseEntity
ResponseBodyEmitter
StreamingResponseBody
HttpEntity
HttpHeaders
Callable
WebAsyncTask
@ModelAtribute
@ResponseBody注解  -----> RequestResponseBodyModelProcessor
```



2、HTTPMessageConverter原理



五、自定义MessageConverter

实现多协议数据兼容。json、xml、x-guigu

0、@ResponseBody 响应数据出去 调用RequestResponseBodyMethodProcessor 处理

1、Processor 处理方法返回值。通过MessageConverter处理

2、所有MessageConverter合起来可以支持各种媒体类型数据的操作（读、写）

3、内容协商找到最终的messageConverter;

SpringMVC的什么功能，一个入口给容器种添加一个 WebMvcConfiguration

```java
@Bean
 public WebMvcConfigurer webMvcConfigurer(){
 return new WebMvcConfigurer() {
 
@Override
public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

   }
  }
}
```



有可能我们添加的自定义的功能会覆盖默认很多功能，导致一些默认的功能失效



### 2.2.5 视图解析与模板引擎

视图解析：SpringBoot默认不支持JSP，需要引入第三方模板引擎技术实现页面渲染



#### 2.2.5.1 视图解析

![image-20230805190928320](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230805190928320.png)



视图解析原理流程：

1、目标方法处理的过程中，所有数据都会被放在ModelAndViewContainer里，包括数据和视图地址

2、方法的参数是一个自定义类型对象（从请求参数中确定的），把他重新放在ModelAndViewContainer

3、任何目标方法执行完成以后都会返回ModelAndView

4、processDispatchResult 处理派发结果（页面该如何响应）

* 1、render(mv,request,response) 进行页面渲染逻辑
  * 1、根据方法的String返回值得到 View 对象【定义了页面的渲染逻辑】
    * 1、所有的视图解析器尝试是否能根据当前返回值得到 View 对象
    * 2、得到了 Redirect/main.html ---> Thymeleaf new RedirecrtView
    * 3、ContentNegotiationViewResolver 里面包含了下面所有的视图解析器，内部还是利用下面所有视图解析器得到视图对象
    * 4、view.render(mv.getModellnternal()),request,response); 视图对象调用自定义的render进行页面渲染工作
      * RedirectView 如何渲染【重定向到一个页面】
      * 1、获取目标url地址
      * 2、response.sendRedirect(encodedURL)



视图解析：

* 返回值以 forward: 开始：new InternalResourceView(forwardUrl);  ---> request.getRequestDispatcher(path)
* 返回值以 redirect：开始：new RedirectView()    ----> render 就是重定向



自定义视图解析器 + 自定义视图；



#### 2.2.5.2 模板引擎-Thymeleaf

* 简介：

Thymeleaf is a modern server-side Java template engine for both web and standalone environments, capable of processingHTML，XML.JavaScript,CSS and even plain text.
现代化、服务端Java模板引擎



##### 2.2.5.2.1 基本语法

表达式：

| 表达式名字 | 语法   | 用途                           |
| ---------- | ------ | ------------------------------ |
| 变量取值   | ${...} | 获取请求域、session域、对象等  |
| 选择变量   | *{...} | 获取上下文对象值               |
| 消息       | #{...} | 获取国际化等值                 |
| 链接       | @{...} | 生成链接                       |
| 片段表达式 | ~{...} | jsp:include 作用，引入公共页面 |



字面量

文本值：’one text‘，’Another onel‘，...

数字：0，34，3.0，12.3，...

布尔值：true，false

空值：null

变量：one，two，...变量不能有空格



文本操作：

字符串拼接： *

变量替换：|The name is ${name}|



数字运算：

运算符：+，-，*，/，%



布尔运算：

运算符：and、or

一元运算：！，not



比较运算：

比较：>，<，>=，<=（gt，it，ge，le）

等式：==，！=（eq，ne）



条件运算：

if-then：(if) ? (then)

if-then-else：(if)?(then):(else)

Default:（value）?: (defaultvalue)



特殊操作：

无操作：_



#### 2.2.5.3 设置属性值-th:attr

设置单个值

```
<form action="subscribe.html" th:attr="action=@{/subscribe}">
<fieldset>
	<input type="text" name="email" />
	<input type="submit" value="Subscribe!" th:attr="value=#{subscribe.submit}"/>
	</fieldset>
</form>
```



设置多个值



以上两个的代替写法 th:xxx

```
<input type="submit" value="Subscribe!" th:value="#{subscribe,submit}"/>
<form action="subscribe.html" th :action="@{/subscribe}">
```



所有h5兼容的标签写法：

https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#setting-value-to-specific-attributes





#### 2.2.5.4 迭代

```
<tr th:each="prod : ${prods}">
	<td th:txt="s{prod .name]">Onions</td>
	<td th:text="${prod.price}">2.41</td>
	<td th:text="${prod.inStock} ? #{true} : #{false}">yes</td>
</tr>
```

```
<tr th:each="prod,iterStat : $(prods)" th:class="${iterStat.odd} ? 'odd'">
	<td th:text="${prod .name]">0nions</td>
	<td th:text="${prod.price}">2.41</td>
	<td th:text="$prod.inStock}? #{true] : #{false}">yes</td>
</tr>
```



#### 2.2.5.5 条件运算

```html
<a href="cd ments .html"
th:href="@{/product/comments(prodId=${prod.id})}"
th:if="${not #lists.isEmpty(prod.comments)}">view</a>
```

```html
<div th:switch="$fuser.role}">
	<p th:case="'admin'">User is an administrator</p>
	<p th:case="#{roles.manager}">User is a manager</p>
	<p th:case="*">User is some other thing</p>
</div>
```





### thymeleaf使用：

1、引入starter

```xml
<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

2、自动配置好了thymeleaf

```java
@AutoConfiguration(after = { WebMvcAutoConfiguration.class, WebFluxAutoConfiguration.class })
@EnableConfigurationProperties(ThymeleafProperties.class)
@ConditionalOnClass({ TemplateMode.class, SpringTemplateEngine.class })
@Import({ TemplateEngineConfigurations.ReactiveTemplateEngineConfiguration.class,
		TemplateEngineConfigurations.DefaultTemplateEngineConfiguration.class })
public class ThymeleafAutoConfiguration {
```



自动配置好的策略

* 1、所有thymeleaf的配置值都在ThymeleafProperties
* 2、配置好了SpringTemplateEngine
* 3、配置好了ThymeleafViewResolver
* 4、我们只需要直接开发页面

```java
public static final String DEFAULT_PREFIX = "classpath:/templates/";

public static final String DEFAULT_SUFFIX = ".html"; //xxx.html
```





### 构建后台管理系统

1、项目构建

2、静态资源处理

3、 路径构建

4、模板抽取

5、页面跳转

6、数据渲染











### 2.2.6 拦截器

```
1、编写一个拦截器实现HandlerInterceptor接口
2、拦截器注册到容器中（实现WebMvcConfiguration的addInterceptor）
3、指定拦截规则【如果是拦截所有，静态资源也会被拦截】
```

#### 2.2.6.1 HandlerIntercepter 接口







#### 2.2.6.2 配置拦截器

```java
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                //所有请求都被拦截，包括静态资源
                .addPathPatterns("/**")
                //放行的请求
                .excludePathPatterns("/","login","/css/**","/fonts/**","/images/**","/js/**");
    }
```





#### 2.2.6.3 拦截器原理

1、根据当前请求，找到Handler可以处理请求的handler以及handler的所有拦截器

2、先来顺序执行 所有拦截器的preHandle方法

* 1、如果当前拦截器preHandler返回为true。则执行下一个拦截器的preHandle
* 2、如果当前拦截器返回为false。直接倒叙执行所有已经执行了的拦截器的 afterCompletion

3、如果任何一个拦截器返回false。直接跳出不执行目标方法

4、所有拦截器都返回True。执行目标方法

5、倒叙执行所有拦截器的postHandle方法

6、前面的步骤有任何异常都会直接倒叙触发 afterCompletion

7、页面成功渲染完成以后，也会倒叙触发 afterCompletion

![image-20230806214524893](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230806214524893.png)





### 2.2.7 文件上传

#### 2.2.7.1 页面代码：

```
<form method="post" action="/upload"enctype="multipart/form-data">
<input type="file" name="file"><br>
<input type="submit” value="提交">
</form>
```





#### 2.2.7.2 文件上传代码：

```java
    /**
     * MultipartFile 自动封装上传过来的文件
     * @param email
     * @param username
     * @param headerImg
     * @param photos
     * @return
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("email") String email,
                         @RequestParam("username") String username,
                         @RequestPart("headerImg") MultipartFile headerImg,
                         @RequestPart("photos") MultipartFile[] photos) throws IOException {

        log.info("上传的信息:email={},username={},headerImg={},photos={}",email,username,headerImg.getSize(),photos.length);

        if (!headerImg.isEmpty()){
            //保存到文件服务器，OSS服务器
            String originalFilename = headerImg.getOriginalFilename();
            headerImg.transferTo(new File("D:\\cache\\" + originalFilename));
        }

        if (photos.length > 0){
            for (MultipartFile photo: photos) {
                String originalFilename = photo.getOriginalFilename();
                photo.transferTo(new File("D:\\cache\\" + originalFilename));
            }

        }
        return "main";
    }
```





#### 2.2.7.3 自动配置原理：

文件上传-MultipartAutoConfiguration-MultipartProperties

* 自动配置好了StandardServiceMultipartResolver【文件上传解析器】
* 原理步骤：
  * 1、请求进来使用文件上传解析器判断【isMultipart】并封装（返回）文件上传请求
  * 2、参数解析器来解析请求中的文件内容封装成MultipartFile
  * 3、将request中文件信息封装为一个Map; MultiValueMap<String,MultipartFile>

FileCopyUtils ：实现文件流的拷贝

```java
    /**
     * MultipartFile 自动封装上传过来的文件
     * @param email
     * @param username
     * @param headerImg
     * @param photos
     * @return
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("email") String email,
                         @RequestParam("username") String username,
                         @RequestPart("headerImg") MultipartFile headerImg,
                         @RequestPart("photos") MultipartFile[] photos)
```



### 2.2.8 异常处理

#### 2.2.8.1 错误处理

* 默认情况下，Spring Boot提供 /error 处理所有错误的映射
* 对于机器客户端，它将生成JSON响应，其中包含错误，HTTP状态和异常消息的详细信息。对于浏览器客户端响应一个“ whitelabel”错误视图，以HTML格式呈现相同的数据
* 要对其进行自定义，添加 view解析为 error
* 要完全替换默认行为，可以实现 ErrorController 并注册该类型的Bean定义，或添加ErrorAttributes类型的组件 以使用现有机制但替换其内容。



#### 2.2.8.2 定制错误处理

* 自定义错误页
  * error/404.html error/5xx.html
* @ControllerAdvice+@ExceptionHandler处理全局异常
* @ResponseStatus + 自定义异常；底层是 ResponseStatusExceptionResolver，把responseStatus注解的信息底层调用 response.sendError(statusCode,resolvedReason)；Tomcat发送的/error
* Spring底层的异常，如 参数类型转换异常;DefaultHandlerExceptionResolver 处理框架底层的异常
  * response.sendError(HttpServletResponse.SC_BAD_REQUEST,ex.getMessage());
* 自定义实现 HandlerExceptionResolver 处理异常；库作为默认的全局异常处理规则
* ErrorViewResolver 实现自定义处理异常
  * response.sendError。error请求就会转给controller
  * 你的异常没有人能处理。tomcat底层 response.sendError。error请求就会转给controller
  * basicErrorController 要去的页面地址是 ErrorViewResolver





#### 2.2.8.3 异常处理自动配置原理

* ErrorMvcAutoConfiguration 自动配置异常处理规则
  * 容器中的组件：类型 ：DefaultErrorAttributes -> id: errorAttributes
    * public class DefaultErrorAttributes implements ErrorAttributes, HandlerExceptionResolver
  * 容器中的组件：类型：BasicErrorController ---> id：basicErrorController
    * 处理默认 /error 路径的请求；页面响应 new ModelAndView("error",model);
    * 容器有组件View --> id 是error; （响应默认错误页）
    * 容器中放组件BeanNameViewResolver
      (视图解析器)； 按照返回的视图名作为组件的id去容器找View对象
  * 容器中的组件：类型：DefaultErrorViewResolver --> id：conventionErrorViewResolver
    * 如果发生错误，会以HTTP的状态码作为视图页地址（ViewName），找到真正的页面
    * error/404、5xx.html

如果想要返回页面；就会找error视图【StaticView】。默认是一个白页





#### 2.2.8.4 异常处理步骤流程

1、执行目标方法，目标方法运行期间有任何异常都会被catch;并且有dispatchException

2、进入视图解析流程（页面渲染？）

processDispatchResult(processedRequest,response,mappedHandler,mv,dispatchException)

3、mv = processHandlerException;处理handler发生的异常，处理完成返回ModelAndView;

* 1、遍历所有的 handlerExceptionResolvers,看谁能处理当前异常【HandlerExceptionResolver处理器异常解析器】
* 2、系统默认的异常解析器；
  * 1、DefaultErrorAttributes先来处理异常。把异常信息保存到request域，并且返回null
  * 2、默认没有任何人能处理异常，所以异常会被抛出
    * 1、如果没有任何人能处理最终就会发送 /error 请求。会被底层的BasicErrorController处理
    * 2、解析错误视图：遍历所有的 ErrorViewResolver 看谁能解析
    * 3、默认的DefaultErrorViewResolver，作用是把响应状态码作为错误页的地址，error/500.html
    * 4、模板引擎最终响应这个页面 error/500.html





### 2.2.9 Web原生组件注入（Servlet、Filter、Listen）

1、使用Servlet API

```java
@ServletComponentScan(Packages= "com.atguigu.admin") : 指定原生servlet组件都放在哪里

@WebServlet(urlPatterns = "/my") : 效果：直接响应，没有Spring的拦截器？
    
@WebFilter(urlPatterns = {"/css/*","/images/*"})
    
@WebListener
```



推荐可以使用这种方式：

扩展：DispatchServlet如何注册进来

* 容器中自动配置了DispatchServlet 属性绑定到WebMvcProperties;对应的配置文件是 spring.mvc
* 通过ServletRegistrationBean<DispatcherServlet> 把DispatcherServlet 配置进来
* 默认映射的是 / 路径



Tomcat-Servlet;

多个Servlet都能处理到同一层路径，精确优选原则

A :   /my/

B:   /my/2



2、使用RegistrationBean

ServletRegistrationBean, FilterRegistrationBean, and ServletListenerRegistrationBean



### 2.2.10 嵌入式Servlet容器

1、切换嵌入式Servlet容器

* 默认支持的WebServer
  * Tomcat,Jetty, or Undertow
  * ServletwebServerApplicationContext 容器启动寻找ServletWebServerFactory 并引导服务器
* 切换服务器
  * Jetty
  * Netty
  * Tomcat
  * Undertow





* 原理
  * SpringBoot应用启动发现当前是Web应用。web场景包-导入tomcat
  * web应用会创建一个web版的ioc容器 ServletWebServerApplicationContext
  * ServletWebServerApplicationContext 启动的时候寻找 ServletWebServerFactory(Servlet的web服务器工厂 ----> Servlet的web服务器)
  * SpringBoot底层默认有很多的WebServer工厂；TomcatServletWebServerFactory,JettyServletWebServerFactory,or UndertowServletWebServerFactory
  * 底层直接会有一个自动配置类。ServletWebServerFactoryAutoConfiguration
  * ServletWebServerFactoryAutoConfiguration导入了ServletWebServerFactoryConfipuration (配置类)
  * ServletWebServerFactoryConfiguration 配置类根据动态判断系统中到底导入了那个web服务器的包。(默认是
    web-starter导入了tomcat包) ，容容器中就有 TomcatServletWebServerFactory
  * TomcatServletWebServerFactory 创建出Tomcat服务器并启动：TomcatWebServer的构造器拥有初始化方法initialize---this.tomcat.start();
  * 内嵌服务器，就是手动把启动服务器的代码调用（tomcat核心jar包存在）
* 









2、定制Servlet容器

* 实现 WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>
  * 把配置文件的值和ServletWebServerFactory   进行绑定
* 修改配置文件server.xxx
* 直接自定义 ConfigurableServletWebServerFactory

xxxxCustomizer：定制化器，可以改变xxxx的默认规则





2.2.11 定制化原理

1、定制化常见方式

* 修改配置文件
* xxxxxCusomizer
* 编写自定义的配置类  xxxConfiguration; + @Bean替换、增加容器中默认组件；视图解析器
* **web应用 编写一个配置类实现 WebMvcConfigurer 即可定制化web功能 + @Bean给容器中扩展一些组件**

```java
/**
 * @author 林继源
 * 1、编写一个拦截器实现HandlerInterceptor接口
 * 2、拦截器注册到容器中（实现WebMvcConfiguration的addInterceptor）
 * 3、指定拦截规则【如果是拦截所有，静态资源也会被拦截】
 */
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                //所有请求都被拦截，包括静态资源
                .addPathPatterns("/**")
                //放行的请求
                .excludePathPatterns("/","login","/css/**","/fonts/**","/images/**","/js/**");
    }
}
```



* @EnableWebMvc + WebMvcConfigurer ——@Bean 可以全面接管SpringMVC，所有规则全部自己重新配置；实现定制和扩展功能
  * 原理
  * 1、WebMvcAutoConfiguration 默认的SpringMVC的自动配置功能类。静态资源、欢迎页......
  * 2、一旦使用@EnableWebMvc、。会@Import(DelegatingWebMvcConfiguration.class)
  * 3、DelegatingWebMvcConfiguration的作用
    * 把所有系统中的WebMvcConfiguration拿过来。所有功能的定制都是这些 WebMvcConfigurer 合起来一起生效
    * 自动配置了一些非常底层的组件.RequestMappingHandlerMapping、这些组件依赖的组件都是从容器中获取
    * public class DelegatingWebMvcConfiguratioin extends WebMvcConfigurationSupport
  * 4、WebMvcAutoConfiguration 里面的配置要能生效 必须@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
* ... ...



2、原理分析套路

场景starter - xxxAutoConfiguration - 导入xxx组件 - 绑定xxxProperties





## 2.3 数据访问

### 2.3.1 SQL

#### 2.3.1.1 数据源的自动配置-HikariDataSource

1、导入jdbc场景

```
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

![image-20230808182413829](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230808182413829.png)



数据库驱动？

为什么导入JDBC场景，官方不导入驱动？官方不知道我们接下来要操作说明数据库。

数据库版本与驱动版本一致

```
默认版本：<mysql.version>8.0.22</mysql.version>

<dependency>
    <groupId>com.mysql</groupId>
    <artifactId>mysql-connector-j</artifactId>
    <scope>runtime</scope>
</dependency

想要修改版本
1、直接依赖引入具体版本（maven的就近依赖原则）
2、重新声明版本(maven的属性的就近原则)

<properties>
	<java.version>1.8</java.version>
	<mysql.version>5.1.49</mysql.version>
</properties>
```



2、分析自动配置

1. 自动配置的类

* DataSourceAutoConfiguration：数据源的自动配置

  * 修改数据源相关的配置：spring.datasource
  * 数据库连接池的配置，是自己容器中没有DataSource才自动配置的、
  * 底层配置好的连接池是：HikariDataSource

  

* DataSourceTransactionManagerAutoConfiguration： 事务管理器的自动配置

* JdbcTemplateAutoConfiguration： JbcTemplate的自动配置，可以来对数据库进行crud

  * 可以修改这个配置项@ConfigurationProperties(prefix = "spring.jdbc")来修改JdbcTemplate

* JndiDataSourceAutoConfiguration：jndi的自动配置

* XADataSourceAutoConfiguration：分布式事务相关的





3、修改配置项

```yml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/examination
    username: root
    password: 123456
    driver-class-name: com.mysql.jdbc.Driver
```





#### 2.3.1.2 使用Druid数据源

1、druid官方github地址

https://github.com/alibaba/druid



整合第三方技术的两种方式

* 自定义
* 找strater





2、自定义方式

1. 创建数据源

```
<dependency>
     <groupId>com.alibaba</groupId>
     <artifactId>druid</artifactId>
     <version>1.2.3</version>
</dependency>
```



使用官方starter方式：

1、引入druid-starter

```
<dependency>
     <groupId>com.alibaba</groupId>
     <artifactId>druid-spring-boot-starter</artifactId>
     <version>1.2.6</version>
</dependency>
```



2、分析自动配置

* 扩展配置项 spring.datasource.druid

* DruidSpringAopConfiguration.class，     监控SpringBean的；配置项：spring.datasource.druid.aop-patterns

* DruidStatViewServletConfiguration.class,监控页的配置：spring.datasource.druid.stat-view-servlet；默认开启

*  DruidWebStatFilterConfiguration.class, Web监控配置：spring.datasource.druid.web-stat-filter；默认开启

* DruidFilterConfiguration.class 所有Druid自己filter的配置

  

```
    druid:
      filter:
        stat:  #对上面filters里面的stat的详细配置
          slow-sql-millis: 1000
          log-slow-sql: true
        wall:
          enabled: true
          config:
            drop-table-allow: false

      stat-view-servlet:    #配置监控页功能
        enabled: true
        login-username: admin
        login-password: admin
        reset-enable: false


      web-stat-filter:   #监控web
        enabled: true
        url-pattern: /*
        exclusions: '*.js,*.gif,*.jpg,*.css,*.ico,/druid/*'
      aop-patterns: com.atguigu.admin.*      #监控SpringBean
      filters: stat,wall,slf4j    #底层开启功能，stat(sql监控)，wall(防火墙)
```





#### 2.3.1.3 整合MyBatis 操作

https://github.com/mybatis

starter
SpringBoot官方的Starter: spring-boot-starter-*

第三方的:*-spring-boot-starter

```
<dependency>
     <groupId>org.mybatis.spring.boot</groupId>
     <artifactId>mybatis-spring-boot-starter</artifactId>
     <version>2.2.2</version>
</dependency>
```

![image-20230809140826216](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230809140826216.png)





1、配置模式

* 全局配置文件
* SqlSessionFactory：自动配置好了
* SqlSession：自动配置了SqlSessionTemplate 组合了SqlSession
* @lmport(AutoConfiguredMapperScannerRegistrar.class);
* Mapper：只要我们写的操作MyBatis的接口标准了 @Mapper 就会被自动扫描进来

```
@EnableConfigurationProperties(MybatisProperties.class) : MyBatis配置项绑定类

@AutoConfigureAfter({DataSourceAutoConfiguration.class, MybatisLanguageDriverAutoConfiguration.class})

public class MybatisAutoConfiguration{}


@ConfigurationProperties(prefix ="mybatis")
public class MybatisProperties
```

可以修改配置文件中mybatis开始的所有；

```
#配置mybatis规则
mybatis:
  config-location: classpath:mybatis/mybatis-config.xml   #全局配置文件位置
  mapper-locations: classpath:mybatis/mapper/*.xml   #sql映射文件位置
  
  
 Mapper接口 ---> 绑定xml
 <?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper 
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.example.bootdemoexercise.mapper.AccountMapper">
    <!--public Account getAcct(Long id); -->

    <select id="getAcct" resultType="com.example.bootdemoexercise.bean.Account">
        select * from account_tbl where  id=#{id}
</select>
</mapper>
```



配置 private Configuration configuration; mybatis.configuration下面的所有，就是相当于改全局配置文件中的值

```
#配置mybatis规则
mybatis:
#  config-location: classpath:mybatis/mybatis-config.xml
  mapper-locations: classpath:mybatis/mapper/*.xml
  configuration:
    map-underscore-to-camel-case: true
    
可以不写全局：配置文件，所有全局配置文件都放在configuration配置项中即可
```





* 导入mybatis官方starter
* 编写mapper接口
* 编写sql映射文件并绑定mapper接口
* 在application.yaml中指定Mappe配置文件的位置，以及指定全局配置文件的信息（建议；在mybatis.configuration）



2、注解模式

3、混合模式



最佳实战：

* 引入mybatis-starter
* 配置application.yaml中，指定mapper-location位置即可
* 编写Mapper接口并标注@Mapper注解
* 简单方法直接注解方式
* 复杂方法编写mapper.xml进行绑定映射
* @MapperScan(comatguiguadmin.mapper)简化，其他的接口就可以不用标注@Mapper注解



#### 2.3.1.4 整合MyBatis-Plus完成CRUD

1、什么是MyBatis-Plus

MyBatis-Plus(简称 MP)是一个 MyBatis 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

[简介 | MyBatis-Plus (baomidou.com)](https://baomidou.com/pages/24112f/#特性)

建议安装MybatisX插件



2、整合MyBatis-Plus

```
<dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>mybatis-plus-boot-starter</artifactId>
      <version>3.4.1</version>
</dependency>
```

自动配置：

* MybatisPlusAutoConfiguration 配置类，MybatislusProperties 配置项绑定。mybatis-plus: xxx 就是对mybatis-plus的定
  制

* SqlSessionFactory 自动配置好。底层是容器中默认的数据源
* mapperLocations 自动配置好的。有默认值。classpath*:/mapper/**/*.xml；任意包的类路径下的所有mapper文件夹下任意路径下的所有xml都是sql映射文件
* 容器中也自动配置好了 SqlSessionTemplate
* @Mapper 标注的接口也会被自动扫描；建议直接 @MapperScan(“comatguiguadmin.mapper")批量扫描就行



3、CRUD功能





### 2.3.2 NoSQL

Redis 是一个开源(BSD许可)的，内存中的数据结构存储系统，它可以用作数据库、缓存和消息中间件。它支持多种类型的数据结构，如 字符串 (strings) ，散列 (hashes) ，列表 (lists) ， 集合 (sets) ，有序集合 (sorted sets)与范围查询，bitmaps， hyperloglogs 和 地理空间 (geospatial) 索引半径查询。 Redis 内置了 复制(replication)，LUA脚本 (Luascripting) ，LRU驱动事件(LRU eviction) ，事务(transactions) 和不同级别的 磁盘持久化(persistence) ，并通过 Redis哨兵 (Sentinel) 和自动分区 (Cluster) 提供高可用性 (high availability)。



1、Redis自动配置

```
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```



自动配置:

* RedisAutoConfiquration 自动配置类。RedisProperties 属性类--> spring.redis.xxx是对redis的配置
* 连接工厂是准备好的。LettuceConnectionConfiguration、JedisConnectionConfiguration
* 自动注入了RedisTemplate<Object,Object> : xxxTemplate;
* 自动注入了StringRedisTemplate; k:v都是String
* key: value
* 底层只要我们使用 StringRedisTemplate、RedisTemplate就可以操作redis





redis环境搭建
1、阿里云按量付费redis。经典网络

2、申请redis的公网连接地址

3、修改白名单允许0.0.0.0/0 访问





2、RedisTemplate与Lettuce



3、切换至jedis

```
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>

        <!--导入jedis-->
<dependency>
     <groupId>redis.clients</groupId>
      <artifactId>jedis</artifactId>
</dependency>
```

```yml
  redis:
    host: localhost
    port: 6379
    database: 0 #操作的是0号数据库
    jedis:
      #Redis连接池配置
      pool:
        max-active: 8 #最大连接数
        max-wait: 1ms #连接池最大阻塞等待时间
        max-idle: 4 #连接池中的最大空闲连接
        min-idle: 0 #连接池中的最小空闲连接
```

```
Filter、Interceptor几乎拥有相同的功能？
1、Filter是servlet定义的原生组件。好处，脱离Spring应用也能使用
2、Interceptor是spring定义的接口。可以使用Spring的自动装配等功能
```





## 2.4 单元测试

### 2.4.1 JUnit5的变化

Spring Boot 2.2.0 版本开始引入 JUnit 5 作为单元测试默认库

作为最新版本的Jnit框架，JUnit5与之前版本的Jnt框架有很大的不同。由三个不同子项目的几个不同模块组成。

JUnit 5 = JUnit Platform + JUnit Jupiter + JUnit Vintage

JUnit Platform：Junit Platform是在JVM上启动测试框架的基础，不仅支持Jnit自制的测试引擎，其他测试引擎也都可以接入。

JUnitJupiter：JUnitJupiter提供了JUnit5的新的编程模型，是JUnit5新特性的核心，内部包含了一个测试引擎，用于在Junit Platform上运行。

JUnit Vintage： 由于JUint已经发展多年，为了照顾老的项目，JUnit Vintage提供了兼容JUnit4.x，Junit3.x的测试引擎

![image-20230809172435696](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230809172435696.png)

注意：

SpringBoot 2.4 以上版本移除了默认对 Vintage 的依赖。如果需要兼容junit4需要自行引入(不能使用junit4的功能)

JUnit 5’ s Vintage Engine Removed from spring-boot-starter-test,如果需要继续兼junit4需要自行引入vintage

```
<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-test</artifactId>
     <scope>test</scope>
</dependency>
```

```xml
<!--兼容junit4-->
<dependency>
    <groupId>org.junit.vintage</groupId>
    <artifactId>junit-vintage-engine</artifactId>
    <scope>test</scope>
   <exclusions>
      <exclusion>
          <groupId>org.hamcrest</groupId>
          <artifactId>hamcrest-core</artifactId>
      </exclusion>
   </exclusions>
</dependency>
```



SpringBoot整合Junit以后。

* 编写测试方法：@Test标注（需要注意使用Junit5版本的注解）
* Junit类具有Spring的功能，@Autowired、比如@Transactional 标注测试方法，测试完成后自动回滚



### 2.4.2 JUnit5常用注解

JUnit5的注解与JUnit4的注解有所变化

[JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/#writing-tests-annotations)

* @Test:表示方法是测试方法。但是与JUnit4的@Test不同，他的职责非常单一不能声明任何属性，拓展的测试将会由Jupiter提供额外测试
* @ParameterizedTest:表示方法是参数化测试，下方会有详细介绍
* @RepeatedTest:表示方法可重复执行，下方会有详细介绍.
* @DisplayName:为测试类或者测试方法设置展示名称0
* @BeforeEach :表示在每个单元测试之前执行
* @AfterEach:表示在每个单元测试之后执行.
* @BeforeAll:表示在所有单元测试之前执行
* @AfterAIl:表示在所有单元测试之后执行
* @Tag:表示单元测试类别，类似于JUnit4中的@Categories
* @Disabled:表示测试类或测试方法不执行，类似于JUnit4中的@lgnore
* @Timeout:表示测试方法运行如果超过了指定时间将会返回错误
* @Extendwith:为测试类或测试方法提供扩展类引用



### 2.4.3 断言（assertions）

断言(assertions)是测试方法中的核心部分，用来对测试需要满足的条件进行验证。这些断言方法都是org.junitjupiterapi.Assertions 的静态方法。JUnit5内置的断言可以分成如下几个类别:检查业务逻辑返回的数据是否合理。

所有的测试运行结束以后，会有一个详细的测试报告；



1.简单断言：

用来对单个值进行简单的验证。如：

| 方法            | 说明                                 |
| --------------- | ------------------------------------ |
| assertEquals    | 判断两个对象或两个原始类型是否相等   |
| assertNotEquals | 判断两个对象或两个原始类型是否不相等 |
| assertSame      | 判断两个对象引用是否指向同一个对象   |
| assertNotSame   | 判断两个对象引用是否指向不同的对象   |
| assertTrue      | 判断给定的布尔值是否为 true          |
| assertFalse     | 判断给定的布尔值是否为 false         |
| assertNull      | 判断给定的对象引用是否为 null        |
| assertNotNull   | 判断给定的对象引用是否不为 null      |



2.数组断言

通过assertArrayEquals 方法来判断两个对象或原始类型的数组是否相等

```
    @Test
    @DisplayName("array assertion")
    public void array(){
        assertArrayEquals(new int[]{1,2},new int[]{1,2},"两数组不一样");
}
```



3.组合断言

```
    @Test
    @DisplayName("组合断言")
    void all() {
        assertAll("test", 
            () -> assertTrue(true && true),
            ()-> assertEquals(1, 1));
}
```





4.异常断言

```
    @Test
    @DisplayName("异常断言")
    void testException() {
        assertThrows(ArithmeticException.class, () -> {
            int i = 10 / 0;
        }, "业务逻辑居然正常运行?");
    }
```





### 2.3.4 前置条件（assumptions）

JUnit 5中的前置条件(assumptions[假设])类似于断言，不同之处在于不满足的断言会使得测试方法失败，而不满足的前置条件只会使得测试方法的执行终止。前置条件可以看成是测试方法执行的前提，当该前提不满足时，就没有继续执行的必要。

```java
 Stack<Object> stack;

    @Test
    @DisplayName("is instantiated with new Stack()")
    void isInstantiatedWithNew() {
        //嵌套测试情况下，外层的Test不能驱动内层的Before（After）Each/All之类的方法提前/之后运行
        new Stack<>();
    }

    @Nested
    @DisplayName("when new")
    class WhenNew {

        @BeforeEach
        void createNewStack() {
            stack = new Stack<>();
        }

        @Test
        @DisplayName("is empty")
        void isEmpty() {
            assertTrue(stack.isEmpty());
        }

        @Test
        @DisplayName("throws EmptyStackException when popped")
        void throwsExceptionWhenPopped() {
            assertThrows(EmptyStackException.class, stack::pop);
        }

        @Test
        @DisplayName("throws EmptyStackException when peeked")
        void throwsExceptionWhenPeeked() {
            assertThrows(EmptyStackException.class, stack::peek);
        }

        @Nested
        @DisplayName("after pushing an element")
        class AfterPushing {

            String anElement = "an element";

            @BeforeEach
            void pushAnElement() {
                stack.push(anElement);
            }


            /**
             * 内层的Test可以驱动外层的Before(After)Each/All之类的方法提前/之后运行
             */
            @Test
            @DisplayName("it is no longer empty")
            void isNotEmpty() {
                assertFalse(stack.isEmpty());
            }

            @Test
            @DisplayName("returns the element when popped and is empty")
            void returnElementWhenPopped() {
                assertEquals(anElement, stack.pop());
                assertTrue(stack.isEmpty());
            }

            @Test
            @DisplayName("returns the element when peeked but remains not empty")
            void returnElementWhenPeeked() {
                assertEquals(anElement, stack.peek());
                assertFalse(stack.isEmpty());
            }
        }
    }
```





### 2.3.5 参数化测试

参数化测试是Unit9很重要的一个新特性，它使得用不同的参数多次运行测试成为了可能，也为我们的单元测试带来许多便利。



利用@ValueSource等注解，指定入参，我们将可以使用不同的参数进行多次单元测试，而不需要每新增一个参数就新增一个单元测试，省去了很多冗余代码。



@ValueSource:为参数化测试指定入参来源，支持八大基础类以及String类型,Class类型

@NullSource:表示为参数化测试提供一个null的入参

@EnumSource:表示为参数化测试提供一个枚举入参

@CsvFileSource:表示读取指定CSV文件内容作为参数化测试入参

@MethodSource: 表示读取指定方法的返回值作为参数化测试入参(注意方法返回需要是一个流)



当然如果参数化测试仅仅只能做到指定普通的入参还达不到让我觉得惊艳的地步。让我真正感到他的强大之处的地方在于他可以支持外部的各类入参。如:CSV,YMLJSON 文件甚至方法的返回值也可以作为入参。只需要去实现ArgumentsProvider接口，任何外部文件都可以作为它的入参。



### 2.3.6 迁移指南

在进行迁移的时候需要注意如下的变化:

* 注解在orgjunitjupiter.api包中，断言在 orgjunit;jupiter.api.Assertions 类中，前置条件在org.junitjupiter.api.Assumptions 类中。
* 把@Before 和@After 换成@BeforeEach 和@AfterEach
* 把@BeforeClass 和@AfterClass 替换成@BeforeAll和@AfterAll。
* 把@lgnore 替换成@Disabled。
* 把@Category 替换成@Tag。
* 把@RunWith、@Rule 和@ClassRule 替换成@ExtendWith。





## 2.5 指标监控

### 2.5.1 SpringBoot Actuator

1、简介：

未来每一个微服务在云上部署以后，我们都需要对其进行监控、追踪、审计、控制等。SpringBoot就抽取了Actuator场景，使得我们每个微服务快速引用即可获得生产级别的应用监控、审计等功能。

```
<dependency>
<groupId>org.springframework.boot</groupId><artifactId>spring-boot-starter-actuator</artifactId></dependency>
```





2、 1.x与2.x的不同

![image-20230810110500959](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230810110500959.png)



3、如何使用

* 引入场景
* 访问http://localhost:8080/actuator/**
* 暴露所有监控信息为HTTP

```
management:
	endpoints:
		enabled-by-default: true   #暴露所有端点信息
		web :
		  exposure :
			include: '*'     #以web方式暴露
```



4、可视化

https://github.com/codecentric/spring-boot-admin

```
<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
      <groupId>de.codecentric</groupId>
      <artifactId>spring-boot-admin-starter-server</artifactId>
      <version>2.3.1</version>
</dependency>
```







### 2.5.2 Actuator Endpoint

1、最常使用的端点

| ID               | 描述                                                         |
| ---------------- | ------------------------------------------------------------ |
| auditevents      | 暴露当前应用程序的审核事件信息。需要一个AuditEventRepository组件。 |
| beans            | 显示应用程序中所有Spring Bean的完整列表                      |
| caches           | 暴露可用的缓存。                                             |
| conditions       | 显示自动配置的所有条件信息，包括匹配或不匹配的原因           |
| configprops      | 显示所有@ConfigurationProperties。                           |
| env              | 暴露Spring的属性ConfigurableEnvironment                      |
| flyway           | 显示已应用的所有Flyway数据库迁移。需要一个或多个Flyway组件   |
| health           | 显示应用程序运行状况信息。                                   |
| httptrace        | 显示HTTP跟踪信息(默认情况下，最近100个HTTP请求-响应)。需要一个HttpTraceRepository组件。 |
| info             | 显示应用程序信息                                             |
| integrationgraph | 显示Springintegrationgraph。需要依赖springintegration-core。 |
| loggers          | 显示和修改应用程序中日志的配置                               |
| liquibase        | 显示已应用的所有Liquibase数据库迁移。需要一个或多个Liquibase组件 |
| metrics          | 显示当前应用程序的“指标”信息。                               |
| mappings         | 显示所有@RequestMapping路径列表                              |
| scheduledtasks   | 显示应用程序中的计划任务                                     |
| sessions         | 允许从Spring Session支持的会话存储中检索和删除用户会话。需要使用Spring Session的基于Servlet的Web应用程序。 |
| shutdown         | 使应用程序正常关闭。默认禁用。                               |
| startup          | 显示由Applicationstartup收集的启动步骤数据。需要使用SpringApplication进行配置BufferingApplicationStartup |
| threaddump       | 执行线程转储。                                               |



如果您的应用程序是web应用程序（Spring MVC，Spring WebFlux或Jersey），则可以使用以下附加端点：

| ID         | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| heapdump   | 返回hprof堆转储文件。                                        |
| jolokia    | 通过HTTP暴露JMX bean (需要引入Jolokia，不适用于WebFlux)。需要引入依赖jolokiacore。 |
| logfile    | 返回日志文件的内容(如果已设置logging.file.name或logging.file.path属性)。支持使用HTTPRange标头来检索部分日志文件的内容。 |
| prometheus | 以Prometheus服务器可以抓取的格式公开指标。需要依赖micrometer-registry-prometheus. |



最常用的EndPoint

* Health：监控状况
* Metrics：运行时指标
* Loggers：日志记录



2、Health Endpoint

健康检查端点，我们一般用于在云平台，平台会定时的检查应用的健康状况，我们就需要Health Endpoint可以为平台返回当前应用的一系列组件健康状况的集合。重要的几点:

* health endpoint返回的结果，应该是一系列健康检查后的一个汇总报告
* 很多的健康检查默认已经自动配置好了，比如: 数据库、redis等
* 可以很容易的添加自定义的健康检查机制





3、Metrics Endpoint

提供详细的、层级的、空间指标信息，这些信息可以被pull(主动推送)或者push (被动获取)方式得到

* 通过Metrics对接多种监控系统
* 简化核心Metrics开发
* 添加自定义Metrics或者扩展已有Metrics





4、管理Endpoints

1、开启与禁用Endpoints

* 默认所有的Endpoint除过shutdown都是开启的
* 需要开启或者禁用某个Endpoint。配置模式为 management.endpoint.<endpointName>.enabled = true

```
management:
	endpoint:
		beans:
			enabled: true
```

* 或者禁用所有的Endpoint然后手动开启指定的Endpoint



5、自定义Endpoint

```java
package com.example.bootdemoexercise.health;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 林继源
 */
@Component
public class MyComHealthIndicator extends AbstractHealthIndicator {

    /**
     * 真实的检查方法
     * @param builder
     * @throws Exception
     */
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        //mongodb。  获取连接进行测试
        Map<String,Object> map = new HashMap<>();
        if (1 == 1){
//            builder.up();
            //健康
            builder.status(Status.UP);
            map.put("count",1);
            map.put("err","连接超时");
        }else {
            builder.status(Status.OUT_OF_SERVICE);
            map.put("err","连接超时");
        }

        builder.withDetail("code",100)
                .withDetails(map);
    }
}
```

```
management:
	health:
		enabled: true
			show-details: always       #总是显示详细信息。可显示每个模块的状态信息
```



### 2.5.3 定制info信息

常用两种方式

1、编写配置文件

```
info:
	appName: boot-admin
	version: 2.8.1
	mavenProjectName: @project.artifactId@   #使用@@可以获取maven的pom文件值
	mavenProjectVersion: @project.version@
```



2、编写InfoContributor

```java
package com.example.bootdemoexercise.acutuator.info;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;

import java.util.Collections;

/**
 * @author 林继源
 */
public class AppInfo implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("msg","你好")
                .withDetail("hello","atguigu")
                .withDetails(Collections.singletonMap("world","6666666"));
    }
}
```



### 2.5.4 定制Metrics信息

1、SpringBoot支持自动适配

* JVM metrics, report utilization of:

  * Various memory and buffer pools
  * Statistics related to garbage collection
  * Threads utilization
  * Number of classes loaded/unloaded
* CPU metrics
* File descriptor metrics
* Kafka consumer and producer metrics
* Log4j2 metrics: record the number of events logged to Log4j2 at each level
* Logback metrics: record the number of events logged to Logback at each level
* Uptime metrics: report a gauge for uptime and a fixed gauge representing the application’s absolute start timelomcat metrics (server,tomcat .mbeanregistry.enabled must be set to true for all 
* Tomcat metrics to be registered)
* Spring Integration metrics

```java
public class CityServiceImpl implements CityServiceImpl{
	@Autowired
	CityMappercityMapper;
	
	Counter counter;
	
public CityServiceImp1(MeterRegistrymeterRegistry){
counter = meterRegistry.counter("cityService.saveCity.count");
}

public City getById(Long id) { 
	return cityMapper.getById(id); 
}

public void saveCity(City city) {
	counter.increment();
	cityMapper.insert(city);
}
}
```



### 2.5.5 定制Endpoint

```java
@Component
@Endpoint(id = "container")
public class DockerEndpoint {
		@ReadOperation
		public Map getDockerInfo(){
		return 	Collections.singletonMap("info","docker started...");
		}

@WriteOperation
private void restartDocker(){
System.out.printIn("docker restarted....");
}
}
```

场景：开发ReadinessEndpoint来管理程序是否就绪，或者LivenessEndpoint来管理程序是否存活当然，这个也可以直接使用 https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready:features.html#production-ready-kubernetes-probes

```
package com.example.bootdemoexercise.acutuator.endpoint;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

/**
 * @author 林继源
 */
@Component
@Endpoint(id = "myservice")
public class MyServiceEndpoint {

    @ReadOperation
    public Map getDockerInfo(){
        return Collections.singletonMap("dockerInfo","docker started....");
    }

    @WriteOperation
    public void stopDocker(){
        System.out.println("docker stopped ....");
    }

}
```



## 2.6 原理解析

### 2.6.1 Profile功能

为了方便多环境适配，springboot简化了profile功能

1、application-profile功能

* 默认配置文件application.yaml; 何时候都会加载
* 指定环境配置文件 application-{envyaml
* 激活指定环境
  * 配置文件激活
  * 命令行激活：java -jar xxx.jar --spring.profiles.active=test --person.name=haha
    * 修改配置文件的任意值，命令行优先
* 默认配置与环境配置同时生效
* 同名配置项，profile配置优先



2、@Profile条件装配功能

```
@Configuration(proxyBeanMethods = false
@Profile("production")
public class ProductionConfiguration {
	//....
	}
```



3、profile分组

```
spring.profiles.group.production[0]=proddb
spring.profiles.group.production[1]=prodmq

使用：--spring.profiles.active=production    激活
```





### 2.6.2 外部化配置

https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.htm!#boot-feature-external-config



1、外部配置源

常用：Java属性文件、YAML文件、环境变量、命令行参数



2、配置文件查找位置

(1)classpath 根路径
(2)classpath 根路径下config目录
(3)jar包当前目录
(4)jar包当前目录的config目录
(5)/config子目录的直接子目录



3、配置文件加载顺序、

1. 当前jar包内部的application.properties和application.yml
2. 当前jar包内部的application-{profile}.properties和application-{profile}.yml
3. 引用的外部iar包的application.properties和application.yml
4. 引用的外部jar包的application-{profile).properties和application-{profile}.yml



4、指定环境优先，外部优先，后面的可以覆盖前面的同名配置项





2.6.3 自定义starter

1、starter启动原理

* starter-pom引入autoconfigurer 包

![image-20230810162421947](C:\Users\15019\AppData\Roaming\Typora\typora-user-images\image-20230810162421947.png)

* autoconfigure包中配置使用 META-INF/spring.factories 中 EnableAutoConfiguration 的值，使得项目启动加载指定的自
  动配置类
* 编写自动配置类xxxAutoConfiguration ->xxxxProperties
  * @Configuration
  * @Conditional
  * @EnableConfigurationProperties
  * @Bean
  * ......

引入starter --- xxAutoonfiguration --- 容器中放入组件 ---- 绑定xxxProperties ---- 配置项



2、自定义starter

auguigu-hello-spring-boot-starter（启动器）

auguigu-hello-spring-boot-AutoConfiguration（配置器）



### 2.6.3 SpringBoot原理

Spring原理【Spring注解】、SpringMVC原理、自动配置原理、SpringBoot原理



1、SpringBoot启动过程

* 创建SpringApplication

  * 保存一些信息。ClassUtils。Servlet
  * 判定当前应用的类型
  * bootstrappers：初始启动引导器（List<Bootstrapper>）：去spring.factories文件中找org.spring.framework.boot.Bootstrapper
  * 找ApplicationContextInitializer；去spring.factories找ApplicationContextInitializer
    * List<ApplicationContextInitializer<?>> initializers
  * 找ApplicationListener；应用监听器。去spring.factories找ApplicationListener
    * List<ApplicationListener<?>> listeners

* 运行SpringApplication

  * StopWatch

  * 记录应用的启动时间

  * 创建引导上下文(Context环境) createBootstrapContext

    * 获取到所有之前的 bootstappers 挨个执行 initialize() 来完成对引导启动器上下文环境设置

  * 让当前应用进入headless模式。java.awt.headless

  * 获取所有RunListener (运行监听器) 【为了方便所有Listener进行事件感知】

    * getSpringFactorieslnstances 去spring.factories找 SpringApplicationRunListener.class

  * 遍历 SpringApplicationRunListener 调用 starting 方法

    * 相当于通知所有感兴趣系统正在启动过程的人，项目正在starting

  * 保存命令行参数：ApplicationArguments

  * 准备环境 prepareEnvironment（）；

    * 返回或者创建基础环境信息。 StandardServletEnvironment
    * 配置环境信息对象
      * 读取所有的配置源的配置属性值
    * 绑定环境信息
    * 监听器调用 listener.environmentPrepared()；通知所有的监听器当前环境准备完成

  * 创建IOC容器(（createApplicationContext）)

    * 根据项目类型(Servlet) 创建容器
    * 当前会创建AnnotationConfigServletWebServerApplicationContext

  * 准备ApplicationContext IOC容器的基本信息 prepareContext()

    * 保存环境信息

    * IOC容器的后置处理流程

    * 应用初始化器；applyInitializers；

      * 遍历所有的 ApplicationContextInitializer。调用 Initialize。来对IOC容器进行初始化扩展功能

      * 遍历所有的listener调用 contextPrepared。EvenPublishListener；通知所有的监听器contextPrepared

    * 所有的监听器 调用 contextLoaded。通知所有的监听器

  * 刷新IOC容器 refreshContext

    * 创建容器中的所有组件（Spring注解）

  * 容器刷新完成后工作？afterRefresh

  * 所有监听器 调用 listener.started(context);

  * 调用所有runners；callRunners()

    * 获取容器中的 ApplicationRunner
    * 获取容器中的 CommandLineRunner

    * 合并所有runner并且按照@Order进行排序
    * 遍历所有的runner.调用 run 方法

  * 如果以上有异常

    * 调用Listener 的 failed

  * 调用所有监听器的 running 方法  listeners.running(context);通知所有的监听器 running

  * running 如果有问题。继续通知 failed。调用所有 Listener 的 failed；通知所有的监听器 failed





```java
@FunctionalInterface
public interface ApplicationRunner{}
/**
* Callback used to run the bean.
* @param args incoming application arguments
* @throws Exception on error
*/
void run(ApplicationArguments args) throws Exception;
}
```







```java
public interface Bootstrapper {
/**
*Initialize the given {@link BootstrapRegistry} with any required registrations
* @param registry the registry to initialize
*/
void intitialize(BootstrapRegistry registry);
}
```





2、Application Events and Listeners

https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-application-eventsand-listeners



ApplicationContextinitializer
ApplicationListener
SpringApplicationRunListener



3、ApplicationRunner 与 CommandLineRunner

