

# 							MySQL学习笔记

<<<<<<< HEAD

=======
## 基础：
>>>>>>> 52c7e70 (学习笔记)

## 1 MySQL的概述：

* #### 数据库相关概念：

  * 数据库：存储数据的仓库，数据是有组织得进行存储，简称：DataBase(DB)

  * 数据库管理系统：操纵和管理数据库的大型软件，简称：DataBase Management System (DBMS)

  * SQL：操作关系型数据库的编程语言，定义了一套操作关系型数据库统一标准，简称：Structured Query Language(SQL)

    ![image-20230202232247876](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230202232247876.png)

  * 主流的关系型数据管理系统

  * ![image-20230202233116143](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230202233116143.png)



总结：

* 1.数据库
  * 数据存储的仓库
* 2.数据管理系统
  * 操纵和管理数据库的大型软件
* 3.SQL
  * 操作关系型数据库的编程语言，是一套标准



* #### MySQL数据库：

  * 启动与停止

    * 启动

      net start mysql80

    * 停止

      net stop mysql80

  * 客户端链接

    * 方式一：MySQL提供的客户端命令行工具

    * 方式二：系统自带的命令行工具执行指令

      mysql [-h 127.0.01] [-P 3306] -u root -p
      
      -h指定连接地址，-P指定连接端口，-u指定连接用户名，-p指定密码



* 关系型数据库（RDBMS）

  * 概念：建立在关系模型基础上，由多张相互连接的二维表组成的数据库
  * 特点：
    * 使用表存储数据，格式统一，便于维护
    * 使用SQL语言操作，标准统一，使用方便

* 数据模型

  ![image-20230203122504051](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230203122504051.png)



## 2 SQL

### 2.1 SQL通用语法

* SQL语句可以单行或多行书写，以分号结尾
* SQL语句可以使用 空格/缩进 来增强语句的可读性
* MySQL数据库的SQL语句不区分大小写，关键字建议使用大写
* 注释：
  * 单行注释：--注释内容 或 #注释内容（MySQL特有）
  * 多行注释：/* 注释内容 */



### 2.2 SQL分类

| 分类 | 全称                       | 说明                                                   |
| ---- | -------------------------- | ------------------------------------------------------ |
| DDL  | Data Definition Language   | 数据定义语言，用来定义数据库对象（数据库，表，字段）   |
| DML  | Data Manipulation Language | 数据操作语言，用来对数据库表中的数据进行增删改         |
| DQL  | Data Query Language        | 数据查询语言，用来查询数据库中表的记录                 |
| DCL  | Data Control Language      | 数据控制语言，用来创建数据库用户、控制数据库的访问权限 |



#### 2.2.1 DDL

##### DDL-数据库操作

* 查询

  * 查询所有数据库

    ```sql
    SHOW DATABASES;
    ```

  * 查询当前数据库

    ```sql
    SELECT DATABASE();
    ```

* 创建

  ```sql
  CREATE DATABASE [IF NOT EXISTS] 数据库名 [DEFAULT CHARSET 字符集] [COLLATE 排序规则];
  ```

* 删除

  ```sql
  DROP DATABASE [IF EXIST] 数据库名;
  ```

* 使用

  ```sql
  USE 数据库名;
  ```



##### DDL-表操作

###### DDL-表操作-查询

* 查询当前数据库所有表

  ```sql
  SHOW TABLES;
  ```

* 查询表结构

  ```sql
  DESC 表名;
  ```

* 查询指定表的建表语句

  ```sql
  SHOW CREATE TABLE 表名;
  ```



###### DDL-表操作-创建

```sql
CREATE TABLE 表名(
	字段1 字段1类型[COMMENT 字段1注释],
	字段2 字段2类型[COMMENT 字段2注释],
	字段3 字段3类型[COMMENT 字段3注释],
	......
	字段n 字段n类型[COMMENT 字段n注释]
)[COMMENT 表注释];
```

**注意：[......]为可选参数，最后一个字段后面没有逗号**

字符串类型：varchar(字符串长度)



###### DDL-表操作-数据类型

MySQL中的数据类型有很多，主要分为三类：数值类型，字符串类型，日期时间类型。

数值类型：

<img src="C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230203134353485.png" alt="image-20230203134353485" style="zoom:150%;" />

使用decimal要指定精度和标度，例：123.45

精度是整个数字长度为5，标度是指小数部分为2

例：age TINYINT UNSIGNED

score double(4,1)表名精度为5，小数位数为1



字符串类型：

![image-20230203135730819](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230203135730819.png)

char 和 varchar 后都跟(指定最多能存储的字符串长度)，char一个字符也会占用是个字符的空间，未占用的字符，其他空间用空格补位，而varchar，存储一个字符就占用一个字符空间，2个字符就占用2个字符空间，根据你所存储的内容去计算当前所占用的空间是多少。

**char性能高，varchar相对于char性能较差，原因：varchar需要根据你所存储的内容去计算当前所占用的空间**



日期时间类型：

![image-20230203140445456](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230203140445456.png)



案例：根据需求创建表（要求设计合理的数据类型、长度）

设计一张员工表，要求：

1.编号（纯数字）

2.员工工号(字符串类型，长度不超过10位)

3.员工姓名(字符串类型，长度不超过10位)

4.性别(男/女，存储一个汉字)

5.年龄(正常人年龄，不可能存储负数)

6.身份证号(二代身份证号均为18位，身份证中有X这样的字符)

7.入职时间(取值年月日即可)

![image-20230203141709798](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230203141709798.png)



###### DDL-表操作-修改

* 添加字段

  ```sql
  ALTER TABLE 表名 ADD 字段名 类型(长度) [COMMENT 注释] [约束];
  ```

* 修改数据类型

  ```sql
  ALTER TABLE 表名 modify 字段名 新数据类型(长度);
  ```

* 修改字段名和字段类型

  ```sql
  ALTER TABLE 表名 CHANGE 旧字段名 新字段名 类型(长度) [COMMENT 注释] [约束];
  ```

* 删除字段

  ```sql
  ALTER TABLE 表名 DROP 字段名;
  ```

* 修改表名

  ```sql
  ALTER TABLE 表名 RENAME TO 新表名;
  ```



###### DDL-表操作-删除

* 删除表

  ```sql
  DROP TABLE [IF EXISTS] 表名;
  ```

* 删除指定表，并重新创建该表

  ```sql
  TRUNCATE TEBLE 表名;
  ```

注意：在删除表时，表中的全部数据也会被删除。



#### 2.2.2 DML

* 介绍：

  DML 英文全称是Data Manipulation Language(数据操作语言)，用来对数据库表中的数据进行增删改操作。

* 增加数据(INSERT)

* 修改数据(UPDATE)

* 删除数据(DELETE)



##### DML-添加数据

* 给指定字段添加数据

  ```sql
  INSERT INTO 表名 (字段名1，字段名2，...) VALUES (值1，值2，...);
  ```

* 给全部字段添加数据

  ```sql
  INSERT INTO 表名 VALUES (值1，值2，...);
  ```

* 批量添加数据

  ```sql
  INSERT INTO 表名 (字段名1，字段名2，...) VALUES(值1，值2，...),(值1，值2，...),(值1，值2，...);
  ```

  ```sql
  INSERT INTO 表名 VALUES (值1，值2，...),(值1，值2，...),(值1，值2，...);
  ```

**注意：**

* **插入数据时，指定的字段顺序需要与值的顺序是一一对应的。**
* **字符串和日期类型数据应该包含在引号中**
* **插入的数据大小，应该在字段的规定范围内。**



##### DML-修改数据

* 修改数据

  ```sql
  UPDATE 表名 SET 字段名1 = 值1,字段名2 = 值2, ...[WHERE 条件]; 
  ```

  **注意：修改语句的条件可以有，也可以没有，如果没有条件，则会修改整张表的所有数据。**



##### DML-删除数据

* 删除数据

  ```sql
  DELETE FROM 表名 [WHERE 条件];
  ```

注意：

* DELETE语句的条件可以有，也可以没有，如果没有则会删除整张表的所有数据
* DELETE语句不能删除某一个字段的值(可以使用UPDATE)。



#### 2.2.3 DQL

* 介绍：

  DQL英文全称是Data Query Language(数据查询语言)，数据查询语言，用来查询数据库中表的记录。

  

  查询关键字：SELECT

  

* DQL-语法（编写顺序）

  ```sql
  SELECT
  		字段列表
  		
  FROM 
  		表名列表
  
  WHERE 
  		条件列表
  		
  GROUP BY
  		分组字段列表
  	
  HAVING
  		分组后条件列表
  		
  ORDER BY
  		排序字段列表
  		
  LIMIT 
  		分页参数
  ```



##### 基本查询

* 查询多个字段

  ```sql
  SELECT 字段1,字段2,字段3 ... FROM 表名;
  ```

  ```sql
  SELECT * FROM 表名;
  ```

* 设置别名

  ```sql
  SELECT 字段1[AS 别名1],字段2 [AS 别名2] ... From 表名;
  ```

* 去除重复记录

  ```sql
  SELECT DISTINCT 字段列表 FROM 表名;
  ```



##### 条件查询(WHERE)

* 语法

  ```sql
  SELECT 字段列表 FROM 表名 WHERE 条件列表;
  ```

* 条件

  | 比较运算符         | 功能                                     |
  | ------------------ | ---------------------------------------- |
  | >                  | 大于                                     |
  | >=                 | 大于等于                                 |
  | <                  | 小于                                     |
  | <=                 | 小于等于                                 |
  | =                  | 等于                                     |
  | <> 或 !=           | 不等于                                   |
  | BETWEEN ...AND ... | 在某个范围之内(含最小、最大值)           |
  | IN(...)            | 在in之后的列表中的值，多选一             |
  | LIKE 占位符        | 模糊匹配(_匹配单个字符，%匹配任意个字符) |
  | IS NULL            | 是NULL                                   |

  | 逻辑运算符 | 功能                       |
  | ---------- | -------------------------- |
  | AND 或 &&  | 并且(多个条件同时成立)     |
  | OR 或 \|\| | 或者(多个条件任意一个成立) |
  | NOT 或 !   | 非，不是                   |

  

##### 聚合函数(count、max、min、avg、sum)

* 介绍：将一列数据作为整体，进行纵向计算



* 常见聚合函数

  | 函数  | 功能     |
  | ----- | -------- |
  | count | 统计数量 |
  | max   | 最大值   |
  | min   | 最小值   |
  | avg   | 平均值   |
  | sum   | 求和     |



* 语法：

  ```sql
  SELECT 聚合函数(字段列表) FROM 表名;
  ```

**注意：null值不参与所有聚合函数运算。**



##### 分组查询(GROUP BY)

* 语法：

  ```sql
  SELECT 字段列表 FROM 表名 [WHERE 条件] GROUP BY 分组字段名 [HAVING 分组后过滤条件];
  ```

* where 和 having区别

  * 执行时机不同：where是分组之前进行过滤，不满足where条件，不参与分组；而having是分组之后对结果进行过滤。
  * 判断条件不同：where 不能对聚合函数进行判断，而having可以。



注意：

* 执行顺序：where > 聚合函数 > having
* 分组之后，查询的字段一般为聚合函数和分组字段，查询其他字段无任何意义。



##### 排序查询(ORDER BY)

* 语法：

  ```sql
  SELECT 字段列表 FROM 表名 ORDER BY 字段1 排序方式1，字段2 排序方式2;
  ```

* 排序方式：

  * ASC：升序（默认值）
  * DESC：降序

注意：如果是多字段排序，当第一个字段值相同时，才会根据第二个字段进行排序



##### 分页查询(LIMIT)

* 语法：

  ```sql
  SELECT 字段列表 FROM 表名 LIMIT 起始索引，查询记录数
  ```

* 注意：

  * 起始索引从0开始，起始索引 = (查询页码 - 1) * 每页显示记录数
  * 分页查询是数据库的方言，不同的数据库有不同的实现，MySQL中是LIMIT
  * 如果查询的是第一页数据，起始索引可以省略，直接简写为limit 10.



##### DQL-执行顺序

```sql
FROM 					
		表名列表

WHERE 					
		条件列表
		
GROUP BY				
		分组字段列表
		
HAVING					
		分组后条件列表
		
SELECT					
		字段列表
	
ORDER BY				
		排序字段列表
		
LIMIT 					
		分页参数
```

​																					

#### 2.2.4 DCL

* 介绍：DCL英文全称是Data Control Language(数据控制语言)，用来创建数据库用户、控制数据库的访问权限



##### DCL-管理用户

* 查询用户

  ```sql
  USE mysql;
  ```

  ```sql
  SELECT * FROM user;
  ```

* 创建用户

  ```sql
  CREATE USER '用户名'@'主机名' IDENTIFIED BY '密码';
  ```

* 修改用户密码

  ```sql
  ALTER USER '用户名'@'主机名' INENTIFIED WITH mysql_native_password BY '新密码';
  ```

* 删除用户

  ```sql
  DROP USER '用户名'@'主机名';
  ```



##### DCL-权限控制

* MySQL中定义了很多种权限，但是常用的就以下几种：

  | 权限               | 说明               |
  | ------------------ | ------------------ |
  | ALL,ALL PRIVILEGES | 所有权限           |
  | SELECT             | 查询数据           |
  | INSERT             | 插入数据           |
  | UPDATE             | 修改数据           |
  | DELETE             | 删除数据           |
  | ALTER              | 修改表             |
  | DROP               | 删除数据库/表/视图 |
  | CREATE             | 创建数据库/表      |



* 查询权限：

  ```sql
  SHOW GRANTS FOR '用户名'@'主机名';
  ```

* 授予权限：

  ```sql
  GRANT 权限列表 ON 数据库名.表名 TO '用户名'@'主机名';
  ```

* 撤销权限：

  ```sql
  REVOKE 权限列表 ON 数据库名.表名 FROM '用户名'@'主机名';
  ```



注意：

* 多个权限之间，使用逗号分隔
* 授权时，数据库名和表名可以使用*进行通配，代表所有





## 3 函数

函数：是指一段可以直接被另一程序调用的程序或代码。



### 3.1 字符串函数

MySQL中内置了很多字符串函数，常用的几个如下：

| 函数                     | 功能                                                      |
| ------------------------ | --------------------------------------------------------- |
| CONCAT(S1,S2,...Sn)      | 字符串拼接，将S1,S2,...Sn拼接成一个字符串                 |
| LOWER(str)               | 将字符串str全部转为小写                                   |
| UPPER(str)               | 将字符串str全部转为大写                                   |
| LAPD(str,n,pad)          | 左填充，用字符串pad对str的左边进行填充，达到n个字符串长度 |
| RPAD(str,n,pad)          | 右填充，用字符串pad对str的右边进行填充，达到n个字符串长度 |
| TRIM(str)                | 去掉字符串头部和尾部的空格                                |
| SUBSTRING(str,start,len) | 返回从字符串str从start位置起的len个长度的字符串           |

代码演示：

```sql
select concat('hello','MySQL');

select upper('Hello');

select lpad('01',5,'-');

select rpad('01',5,'-');

select trim(' Hello MySQL ');

select substring('Hello MySQL',7,11);

select lpad('01',5,'0');

update employee set workno = lpad(workno,5,'0');
```



### 3.2 数值函数

常见的数值函数如下：

| 函数       | 功能                               |
| ---------- | ---------------------------------- |
| CEIL(x)    | 向上取整                           |
| FLOOR(x)   | 向下取整                           |
| MOD(x,y)   | 返回x/y的模                        |
| RAND()     | 返回0~1内的随机数                  |
| ROUND(x,y) | 求参数x的四舍五入的值，保留y位小数 |

代码演示：

```sql
select ceil(1.5);

select floor(1.5);

select mod(7,4);

select rand();

select round(2.344,2);

select lpad(round(rand() * 1000000,0),6,'0');
```



### 3.3 日期函数

常见的日期函数如下：

| 函数                              | 功能                                              |
| --------------------------------- | ------------------------------------------------- |
| CURDATE()                         | 返回当前日期                                      |
| CURTIME()                         | 返回当前时间                                      |
| NOW()                             | 返回当前日期和时间                                |
| YEAR(date)                        | 获取指定的date的年份                              |
| MONTH(date)                       | 获取指定的data的月份                              |
| DAY(date)                         | 获取指定的data的日期                              |
| DATE_ADD(date,INTERVAL expr type) | 返回一个日期/时间值加上一个时间间隔expr后的时间值 |
| DATEDIFF(date1,date2)             | 返回起始时间 date1 和 结束时间 date2 之间的天数   |



### 3.4 流程函数

流程函数也是很常用的一类函数，可以在SQL语句中实现条件筛选，从而提高语句的效率

| 函数                                                      | 功能                                                        |
| --------------------------------------------------------- | ----------------------------------------------------------- |
| IF(value,t,f)                                             | 如果value为true，则返回t，否则返回f                         |
| IFNULL(value1,value2)                                     | 如果value不为空，返回value1，否则返回value2                 |
| CASE WHEN [val1] THEN [res1] ...ELSE [default] END        | 如果 val1 为 true，返回res1，...否则返回default默认值       |
| CASE [expr] WHEN [val1] THEN [res1] ...ELSE [default] END | 如果 expr 的值等于 val1，返回res1，...否则返回default默认值 |



## 4 约束

### 4.1 概述

* 概念：约束是作用于表中字段上的规则，用于限制存储在表中的数据

* 目的：保证数据库中数据的正确、有效性和完整性

* 分类：

  | 约束                     | 描述                                                     | 关键字      |
  | ------------------------ | -------------------------------------------------------- | ----------- |
  | 非空约束                 | 限制该字段的数据不能为null                               | NOT NULL    |
  | 唯一约束                 | 保证该字段的所有数据都是唯一、不重复的                   | UNIQUE      |
  | 主键约束                 | 主键是一行数据的唯一标识，要求非空且唯一                 | PRIMARY KEY |
  | 默认约束                 | 保存数据时，如果未指定该字段的值，则采用默认值           | DEFAULT     |
  | 检查约束(8.0.16版本之后) | 保证字段值满足某一个条件                                 | CHECK       |
  | 外键约束                 | 用来让两张表的数据之间建立连接，保证数据的一致性和完整性 | FOREIGN KEY |



### 4.2 约束演示

```sql
create table user (
    id int primary key auto_increment comment '主键',
    name varchar(10) not null unique comment '姓名',
    age int check ( age > 0 && age <= 120 ) comment '年龄',
    status char(1) default '1' comment '状态',
    gender char(1) comment '性别'
)comment '用户表';

insert into user (name, age, status, gender) values ('Tom1',19,'1','男'),('Tom2',25,'0','男');

insert into user (name, age, status, gender) values ('Tom3',19,'1','男');

insert into user (name, age, status, gender) values ('Tom4',80,'1','男');

insert into user (name, age, gender) values ('Tom5',120,'男');
```



### 4.3 外键约束

* 概念：

  外键用来让两张表的数据之间建立连接，从而保证数据的一致性和完整性。

* 语法

  * 添加外键

  ```sql
  CREATE TABLE 表名(
  		字段名 数据类型
  		...
  		[CONSTRAINT] [外键名称] FOREIGN KEY(外键字段名) REFERENCES 主表 (主表列明);
  )
  ```

  ```sql
  ALTER TABLE 表名 ADD CONSTRAINT 外键名称 KEY(外键字段名) REFERENCES 主表 (主表列表);
  ```

  代码演示：

  ```sql
  alter table emp add constraint fk_emp_dept_id foreign key (dept_id) references depy(id);
  ```

  * 删除外键

    ```sql
    ALTER TABLE 表名 DROP FOREIGN KEY 外键名称
    ```

    代码演示：

    ```sql
    ALTER TABLE emp DROP FOREIGN KEY fk_emp_dept_id;
    ```

 

* 删除/更新行为

  | 行为        | 说明                                                         |
  | ----------- | ------------------------------------------------------------ |
  | NO ACTION   | 当在父表中删除/更新对应记录时，首先检查该记录是否有对应外键，如果有则不允许删除/更新。(与RESTRAIN一致) |
  | RESTRAIN    | 当在父表中删除/更新对应记录时，首先检查该记录是否有对应外键，如果有则不允许删除/更新。(与NO ACTION一致) |
  | CASCASE     | 当在父表中删除/更新对应记录时，首先检查该记录是否有对应外键，如果有，则也删除/更新外键在子表中的记录 |
  | SET NULL    | 当在父表中删除对应记录时，首先检查该记录是否有对应外键，如果有则设置子表中该外键值为null(这就要求该外键允许取null) |
  | SET DEFAULT | 父表有变更时，子表将外键列设置成一个默认值(Innodb不支持)     |

```sql
ALTER TABLE 表名 ADD CONSTRAINT 外键名称 FOREIGN KEY (外键字段) REFERENCES 主表名 (猪表字段名) ON UPDATE CASCADE ON DELETE CASCADE;
```



## 5 多表查询

### 5.1 多表关系

* 概述：项目开发中，在进行数据库表结构设计时，会根据业务需求及业务模块之间的关系，分析并设计表结构，由于业务之间相互关联，所以各个表结构之间也存在着各种联系，基本上分为3种 ：

  * 一对多(多对一)
  * 多对多
  * 一对一

* 一对多(多对一)

  * 案例：部门与员工的关系
  * 关系：一个部门对应多个员工，一个员工对应一个部门
  * 实现：在多的一方建立外键，指向一的一方的主键

  ![image-20230204191148879](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204191148879.png)

* 多对多

  * 案例：学生与课程的关系

  * 关系：一个学生可以选修多门课程，一门课程也可以供多个学生选择

  * 实现：建立第三张中间表，中间表至少包含两个外键，分别关联两方主键

    ![image-20230204191434965](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204191434965.png)

![image-20230204192802162](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204192802162.png)



* 一对一
  * 案例：用户与用户详情的关系
  * 关系：一对一关系，多用于单表拆分，将一张表的基础字段放在一张表中，其他详情字段放在另一张表中，以提升操作效率
  * 实现：在任意一方加入外键，关联另一方的主键，并且设置外键为唯一的(UNIQUE)

![image-20230204193441164](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204193441164.png)



### 5.2 多表查询概述

* 概述：指从多张表中查询数据
* 笛卡尔积：笛卡尔乘积是指在数学中，两个集合 A集合和B集合的所有组合情况。(在多表查询时，需要消除无效的笛卡尔积)

* 多表查询分类
  * 内连接：相当于查询A、B交集部分数据
  * 外连接：
    * 左外连接：查询左表所有数据，以及两张表交集部分数据
    * 右外连接：查询右表所有数据，以及两张表交集部分数据
  * 自连接：当前表与自身的连接查询，自连接必须使用表别名

![image-20230204213110436](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204213110436.png)

* 子查询



### 5.3 内连接

* 语法：

  * 隐式内连接

  ```sql
  SELECT 字段列表 FROM 表1，表2 WHERE 条件...;
  ```

  * 显式内连接

  ```sql
  SELECT 字段列表 FROM 表1 [INNER] JOIN 表2 ON 连接条件...;
  ```

* 内连接查询的是两张表交集的部分

![image-20230204213110436](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230204213110436.png)

### 5.4 外连接

* 语法

  * 左外连接：查询左表所有数据，以及两张表交集部分数据

    ```sql
    SELECT 字段列表 FROM 表1 LEFT [OUTER] JOIN 表2 ON 条件...;
    ```

    相当于查询表1(左表)的所有数据 包含 表1和表2交集部分的数据

  * 右外连接：查询右表所有数据，以及两张表交集部分数据

    ```sql
    SELECT 字段列表 FROM 表1 RIGHT [OUTER] JOIN 表2 ON 条件...;
    ```

    相当于查询表2(右表)的所有数据 包含 表1和表2交集部分的数据

  

### 5.5 自连接

* 语法：

  ```sql
  SELECT 字段列表 FROM 表A 别名A JOIN 表A 别名B ON 条件...;
  ```

  自连接查询，可以是内连接查询，也可以是外连接查询



### 5.6 联合查询

* 联合查询-union，union all

  对于union查询，就是把多次查询的结果合并起来，形成一个新的查询结果集。

  ```sql
  SELECT 字段列表 FROM 表A ...
  UNION [ALL]
  SELECT 字段列表 FROM 表B;
  ```

  * 对于联合查询的多张表的列数必须保持一致，字段类型也需要保持一致。

  * union all 会将全部的数据直接合并在一起，union 会对合并之后的数据去重。



### 5.7 子查询

* 概念：SQL语句中嵌套SELECT语句，称为嵌套查询，又称子查询

```sql
SELECT * FROM t1 WHERE column1 = (SELECT column1 FROM t2);
```

子查询外部的语句可以是INSERT / UPDATE / DELETE / SELECT 的任何一个

* 根据子查询结果不同，分为：
  * 标量子查询(子查询结果为单个值)
  * 列子查询(子查询结果为一列)
  * 行子查询(子查询结果为一行)
  * 表子查询(子查询结果为多行多列)

* 根据子查询位置，分为：WHERE 之后、FROM之后、SELECT之后。



* 标量子查询：

  * 子查询返回的结果是单个值(数字、字符串、日期等)，最简单的形式，这种子查询称为标量子查询
  * 常用的操作符：=     <>   >   >=   <   <=

* 列子查询

  * 子查询返回的结果是一列(可以是多行)，这种子查询称为列子查询

  * 常用操作符：IN、NOT IN、ANY、SOME、ALL

    | 操作符 | 描述                                   |
    | ------ | -------------------------------------- |
    | IN     | 在指定的集合范围之内，多选一           |
    | NOT IN | 不在指定的集合范围之内                 |
    | ANY    | 子查询返回列表中，有任意一个满足即可   |
    | SOME   | 与ANY等同，使用SOME的地方都可以使用ANY |
    | ALL    | 子查询返回列表的所有值都必须满足       |

  演示：

  ```sql
  select * from emp where salary > any (select salary from emp where dept_id = (select id from depy where name = '研发部'));
  ```

* 行子查询

  * 子查询返回的结果是一行(可以是多列)，这种子查询称为行子查询
  * 常用的操作符： = 、<>、IN、NOT IN

  演示：

  ```sql
  select * from emp where (salary,managerid) = (select salary,managerid from emp where name = '张无忌');
  ```

* 表子查询

  * 子查询返回的结果是多行多列，这种子查询称为表子查询
  * 常用操作符：IN

  ```sql
  select e.*,depy.* from (select * from emp where entrydate > '2006-01-01') e left join depy on e.dept_id = depy.id;
  ```



## 6 事务

### 6.1 事务简介

事务是一组操作的集合，他是一个不可分割的工作单位，事务会把所有的操作作为一个整体一起向系统提交或撤销操作请求，即这些操作要么同时成功，要么同时失败。

![image-20230205154159967](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230205154159967.png)

默认MySQL的事务是自动提交的，也就是说，当执行一条DML语句，MySQL会立即隐式地提交事务。



### 6.1 事务操作

* 查看/设置事务提交方式

  ```sql
  SELECT @@autocommit;
  ```

  ```sql
  SET @@autocommit = 0;
  ```

* 提交事务

  ```sql
  COMMIT;
  ```

* 回滚事务

  ```sql
  ROLLBACK;
  ```



* 开启事务

  ```sql
  START TRANSACTION 或 GEGIN;
  ```

* 提交事务

  ```sql
  COMMIT;
  ```

* 回滚事务

  ```sql
  ROLLBACK;
  ```



事务控制可以有两种方式：

* 关闭事务的自动提交，改为手动，通过commit提交以及rollback回滚
* 通过指令start transaction 或 begin 显示地来开启事务，执行成功就commit，失败则rollback.



### 6.2 事务四大特性(ACID)

*  原子性(Atomicity)：事务是不可分割的最小操作单元，要么全部成功，要么全部失败。
* 一致性(Consistency)：事务完成时，必须使所有的数据都保持一致状态。
* 隔离性(Isolation)：数据库系统提供的隔离机制，保证事务在不受外部并发操作影响的独立环境下运行。
* 持久性(Durability)：事务一旦提交或回滚，它对数据库中的数据的改变就是永久的。



### 6.3 并发事务问题

| 问题       | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| 脏读       | 一个事务读到另一个事务还没有提交的数据                       |
| 不可重复读 | 一个事务先后读取同一条记录，但两次读取的数据不同，称之为不可重复读 |
| 幻读       | 一个事务按照条件查询数据时，没有对应的数据行，但是在插入数据时，又发现这行数据已经存在，好像出现了”幻影“ |

![image-20230205162048453](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230205162048453.png)

![image-20230205162511773](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230205162511773.png)

![image-20230205162745180](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230205162745180.png)



### 6.4 事务隔离级别

| 隔离级别              | 脏读 | 不可重复读 | 幻读 |
| --------------------- | ---- | ---------- | ---- |
| Read uncommited       | √    | √          | √    |
| Read commited         | ×    | √          | √    |
| Repeatable Read(默认) | ×    | ×          | √    |
| Serializable          | ×    | ×          | ×    |

* 查看事务隔离级别

  ```sql
  SELECT @@TRANSACTION_ISOLATION;
  ```

* 设置事务隔离级别

  ```sql
  SET [SESSION|GLOBAL] TRANSACTION ISOLATION {READ UNCOMMITTED | READ COMMITTED | REPEATABLE READ | SERIALIZABLE}
<<<<<<< HEAD
  ```
=======
  ```



## 进阶：

## 1 存储引擎

### 1.1 MySQL体系结构

<img src="C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230507211614814.png" alt="image-20230507211614814" style="zoom:150%;" />



* 连接层

最上层是一些客户端和链接服务，主要完成一些类似于连接处理、授权认证、及相关的安全方案。服务器也会为安全接入的每个客户端验证它所具有的操作权限。

* 服务层

第二层架构主要完成大多数的核心服务功能，如SQL接口，并完成缓存的查询，SOL的分析和优化，部分内置函数的执行。所有跨存储引擎的功能也在这一层实现，如 过程、函数等。

* 引擎层

存储引擎真正的负责了MySOL中数据的存储和提取，服务器通过AP和存储引擎进行通信。不同的存储引擎具有不同的功能，这样我们可以根据自己的需要，来选取合适的存储引擎。

* 存储层

主要是将数据存储在文件系统之上，并完成与存储引擎的交互。



### 1.2 存储引擎简介

存储引擎就是存储数据、建立索引、更新/查询数据等技术的实现方式。存储引擎是基于表的，而不是基于库的，所以存储引擎也可被称为表类型。



* 在创建表时，指定存储引擎

```
CREATE TABLE 表名(
    字段1 字段1类型 [COMMENT 字段1注释],
    字段n 字段n类型 [COMMENT 字段n注释]
)ENGINE = INNODB[COMMENT 表注释];
```

* 查看当前数据库支持的存储引擎

```
SHOW ENGINES;
show engines;
```



### 1.3 存储引擎特点

* InnoDB

  * 介绍：

  InnoDB是一种兼顾高可靠性和高性能的通用存储引擎，在MySQL5.5之后，InnoDB是默认的MySQL存储引擎

* 特点：

  * DML操作遵循ACID模型，支持事务；
  * 行级锁，提高并发访问性能；
  * 支持外键FOREIGN KEY约束，保证数据的完整性和正确性；

* 文件

  * xxx.ibd：xxx代表的是表名，innoDB引擎的每张表都会对应这样一个表空间文件，存储该表的表结构（frm、sdi）、数据和索引
  * 参数：innoDB_file_per_table

* InnoDB逻辑存储结构

<img src="C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230508205231151.png" alt="image-20230508205231151" style="zoom:150%;" />



* MyISAM

  * 介绍：

  MyISAM是MySQL早期的默认存储引擎

  * 特点：
    * 不支持事务，不支持外键
    * 支持表锁，不支持行锁
    * 访问速度快
  * 文件：
    * xxx.sdi：存储表结构信息
    * xxx.MYD：存储数据
    * xxx.MYI：存储索引



* Memory

  * 介绍：

  Memory引擎的表数据是存储在内存中的，由于受到硬件问题、或断电问题，只能将这些表作为临时表或缓存使用。

  * 特点：
    * 内存存放
    * hash索引(默认)
  * 文件：
    * xxx.sdi：存储表结构信息



<img src="C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230508210209249.png" alt="image-20230508210209249" style="zoom: 150%;" />



### 1.4 存储引擎选择

在选择存储引擎时，应该根据应用系统的特点选择合适的存储引擎。对于复杂的应用系统，还可以根据实际情况选择多种存储引擎进行组合

* InnoDB：是Mysql的默认存储引擎，支持事务、外键。如果应用对事务的完整性有比较高的要求，在并发条件下要求数据的一致在，数据操作除了插入和查询之外，还包含很多的更新、删除操作，那么InnoDB存储引擎是比较合适的选择。
* MyISAM ：如果应用是以读操作和插入操作为主，只有很少的更新和删除操作，并且对事务的完整性、并发性要求不是很高，那么选择这个存储引擎是非常合适的。（例如：评论，日志）
* MEMORY：将所有数据保存在内存中，访问速度快，通常用于临时表及缓存。MEMORY的缺陷就是对表的大小有限制，太大的表无法缓存在内存中，而且无法保障数据的安全性。



## 2 索引

### 2.1 索引概述

* 介绍

索引（index）是帮助MVSOL 高效获取数据的数据结构有房。在数据之外，数据库系统还维护着满足特定查找算法的数据结构，这些数据结构以某种方式引用（指向）数据， 这样就可以在这些数据结构上实现高级查找算法，这种数据结构就是索引。

* 优缺点：

| 优势                                                         | 劣势                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 提高数据检索的效率，降低数据库的IO成本                       | 索引列也是要占用空间的                                       |
| 通过索引列对数据进行排序，降低数据排序的成本，降低CPU的消耗。 | 索引大大提高了查询效率，同时却也降低更新表的速度，如对表进行INSERT、UPDATE、DELETE时，效率降低。 |



### 2.2 索引结构

MySQL的索引实在存储引擎层实现的，不同的存储引擎有不同的结构，主要包含以下几种：

| 索引结构            | 描述                                                         |
| ------------------- | ------------------------------------------------------------ |
| B+Tree索引          | 最常见的索引类型，大部分引擎都支持B+树索引                   |
| Hash索引            | 底层数据结构是用哈希表实现的,只有精确匹配索引列的查询才有效不支持范围查询 |
| R-tree(空间索引)    | 空间索引是MySAM引擎的一个特殊索引类型，主要用于地理空间数据类型，通常使用较少 |
| Full-text(全文索引) | 是一种通过建立倒排索引快速匹配文档的方式。类似于Lucene，Solr，ES |



| 索引       | InnoDB        | MyISAM | Memory |
| ---------- | ------------- | ------ | ------ |
| B+tree索引 | 支持          | 支持   | 支持   |
| Hash索引   | 不支持        | 不支持 | 支持   |
| R-tree索引 | 不支持        | 支持   | 不支持 |
| Full-text  | 5.6版本后支持 | 支持   | 不支持 |



* 二叉树

![image-20230508232501483](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230508232501483.png)

二叉树缺点：顺序插入时，会形成一个链表，查询性能大大降低。大数据量情况下，层级较深，检索速度慢。

红黑树：大数据量情况下，层级较深，检索速度慢。



* B-Tree（多路平衡查找树）

以一颗最大度数（max-degree）为5（5阶）的b-tree为例（每个节点最多存储4个key，5个指针）：

![image-20230508232803116](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230508232803116.png)

树的度数指的是一个节点的子节点个数

![image-20230508233428037](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230508233428037.png)

具体动态变化过程参考网站：https://www.cs.usfca.edu/~galles/visualization/BTree.html



* B+Tree

MySQL索引数据结构对经典的B+Tree进行了优化。在原B+Tree的基础上，增加一个指向相邻叶子节点的链表指针，就形成了带有顺序指针的B+Tree，提高区间访问的性能

<img src="C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509150537075.png" alt="image-20230509150537075" style="zoom:150%;" />

![image-20230509150839237](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509150839237.png)

相对于B-Tree区别：

* 所有的数据都会出现在叶子节点
* 叶子节点形成一个单向链表



* Hash

哈希索引就是采用一定的hash算法，将键值换算成新的hash值，映射到对应的槽位上，然后存储在hash表中

如果两个（或多个）键值，映射到一个相同的槽位上，他们就产生了hash冲突（也称为hash碰撞），可以通过链表来解决。

![image-20230509151624743](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509151624743.png)

* Hash索引特点：
  * Hash索引只能用于对等比较(=，in)，不支持范围查询(between，>，<，...)
  * 无法利用索引完成排序操作
  * 查询效率高，通常只需要一次检索就可以了，效率通常要高于B+Tree索引
* 存储引擎支持：

在MySQL中，支持hash索引的是Memory引擎，而InnoDB中具有自适应hash功能，hash索引是存储引擎根据B+Tree索引在指定条件下自动构建的。



为什么InnoDB存储引擎选择使用B+Tree索引结构?

* 相比于二叉树，层级更少，搜索效率高；
* 对于B-Tree，无论是叶子节点还是非叶子节点，都会保存数据，这样导致一页中存储的键值减少，指针跟着减少，要同样保存大量数据，只能增加树的高度，导致性能降低；
* 相对Hash索引，B+Tree支持范围匹配及排序操作



### 2.3 索引分类

| 分类     | 含义                                                 | 特点                     | 关键字   |
| -------- | ---------------------------------------------------- | ------------------------ | -------- |
| 主键索引 | 针对表中主键创建的索引                               | 默认自动创建，只能有一个 | PRIMARY  |
| 唯一索引 | 避免同一个表中某数据列中的值重复                     | 可以有多个               | UNIQUE   |
| 常规索引 | 快速定位特定数据                                     | 可以有多个               |          |
| 全文索引 | 全文索引查找的是文本中的关键字，而不是比较索引中的值 | 可以有多个               | FULLTEXT |

在InnoDB存储引擎中，根据索引的存储形式，又可以分为以下两种：

| 分类                      | 含义                                                         |
| ------------------------- | ------------------------------------------------------------ |
| 聚集索引(Clustered Index) | 将数据存储与索引放到了一块，索引结构的叶子节点保存了行数据   |
| 二级索引(Secondary Index) | 将数据与索引分开存储不，索引结构的叶子节点关联的是对应的主键 |

聚集索引选取规则：

* 如果存在主键，主键索引就是聚集索引
* 如果不存在主键，将使用第一个唯一(UNIQUE)索引作为聚集索引
* 如果表没有主键，或没有合适的唯一索引，则InnoDB会自动生成一个rowid作为隐藏的聚集索引

<img src="C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509154622214.png" alt="image-20230509154622214" style="zoom:200%;" />

回表查询：先通过二级索引查找到主键值，再通过主键值通过聚集索引拿到该行数据

![image-20230509155545365](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509155545365.png)



### 2.4 索引语法

* 创建索引

```
CREATE [UNIQUE][FULLTEXT] INDEX index_name ON table_name (index_col_name,...);
```

* 查看索引

```
SHOW INDEX FROM table_name;
```

* 删除索引

```
DROP INDEX index_name ON table_name;
```



### 2.5 SQL性能分析

* SQL执行频率

MySQL客户端连接成功后，通过show [session|global] status命令可以提供服务器状态信息。通过如下指令，可以查看当前数据库的INSERT、UPDATE、DELETE、SELECT的访问频次

```
SHOW GLOBAL STATUS LIKE 'Com_______';
```



* 慢查询日志

慢查询日志记录了所有执行时间超过指定参数（long_query_time，单位：秒，默认10秒）的所有SQL语句的日志

通过该语句查看：

```sql
show variables like 'slow_query_log'
```

MySQL的慢查询日志默认没有开启，需要在MySQL的配置文件（/etc/my.cnf）中配置如下信息：

```sql
#开启MySQL慢日志查询开关
slow_query_log=1

#设置慢日志的时间为2秒，SQL语句执行时间超过2秒，就会出现慢查询，记录查询日志
long_query_time=2
```



* profile详情

show profiles能够在做SQL优化时帮助我们了解时间都耗费到哪里去了。通过have_profiling参数，能够看到当前MySQL是否支持profile操作：

```
SELECT @@have_profiling;
```

默认profiling是关闭的，可以通过set语句在session/global级别开启profiling:

```
SET profiling = 1;

//查看：
SELECT @@profiling;
```

执行一系列的业务SQL的操作，然后通过如下指令查看指令的执行耗时：

```
#查看每一条SQL的耗时基本情况
show profiles;

#查看指定query_id的SQL语句各个阶段的耗时情况
show profile for query_id;

#查看指定query_id的SQL语句CPU的使用情况
show profile cpu for query_id;
```



* explain执行计划

EXPLAIN 或者 DESC命令获取MySQL如何执行 SELECT 语句的信息，包括在SELECT 语句执行过程中表如何连接和连接的顺序

语法：

```
#直接在select语句之前加上关键字explain/desc
EXPLAIN SELECT 字段列表 FROM 表名 WHERE 条件；
```



EXPLAIN执行计划各字段含义：

* id

  select查询的序列号，表示查询中执行select子句或者是操作表的顺序(id相同，执行顺序从上到下；id不同，值越大，越先执行)

* select_type

表示SELECT的类型，常见的取值有SIMPLE（简单表，即不使用表连接或者子查询）、PRIMARY（主查询，即外层的查询）、UNION（UNION中的第二个或者后面的查询语句）、SUBQUERY（SELECT / WHERE之后包含了子查询）等

* type

表示连接类型，性能由好到差的连接类型为null、system、const、eq_ref、ref、range、index、all

* possible_key

  显示可能应用在这张表上的索引，一个或多个

* key

  实际使用的索引，如果为NULL，则没有使用索引

* Key_len

  表示索引中使用的字节数，该值为索引字段最大可能长度，并非实际使用长度，在不损失精确性的前提下，长度越短越好

* rows

  MySQL认为必须要执行查询的行数，在innoDB引擎的表中，是一个估计值，可能并不总是准确的

* filtered

  表示返回结果的行数占需读取行数的百分比，filtered的值越大越好

  

### 2.6 索引使用

* 最左前缀法则

如果索引了多列（联合索引），要遵守最左前缀法则。最左前缀法则指的是查询从索引的最左列开始，并且不跳过索引中的列。如果跳过某一列，索引将部分失效（后面的字段索引失效）

![image-20230509190544425](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509190544425.png)



* 范围查询

联合索引中，出现范围查询（>，<）,范围查询右侧的列索引失效。若业务允许尽量使用（>=，<=）



* 索引列运算

  不要在索引列上进行运算操作，索引将失效



* 字符串不加引号

  字符串类型字段使用时，不加引号，索引将失效



* 模糊查询

  如果仅仅是尾部模糊匹配，索引不会失效。如果是头部模糊匹配，索引失效

![image-20230509192652889](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509192652889.png)



* or连接的条件

  用or分割开得条件，如果or前的条件中的列有索引，而后面的列中没有索引，那么涉及的索引都不会被用到

![image-20230509193043096](C:%5CUsers%5CAdministrator%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20230509193043096.png)

由于age没有索引，所以即使id、phone有索引，索引也会失效。所以需要针对于age也要建立索引。 



* 数据分布影响

如果MySQL评估使用索引比全表更慢，则不使用索引



### 2.7 索引设计原则







## 3 SQL优化















## 4 视图/存储过程/触发器













## 5 锁









## 6 InnoDB引擎











## 7 MySQL管理
>>>>>>> 52c7e70 (学习笔记)
