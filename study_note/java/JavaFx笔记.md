# 							JavaFx笔记

# JavaFX Stage

* 简介：

JavaFX Stagejavafx.stage.Stage表示JavaFX 桌面应用程序中的窗口。在 JavaFX 内部，Stage您可以插入一个JavaFX Scene

它表示在窗口内显示的内容- 在Stage.当JavaFX 应用程序启动时，它会创建一个根Stage对象，该对象将传递给 start(Stage primaryStage)JavaFX应用程序的根类的方法。此Stage 对象代表 JavaFX 应用程序的主窗口。您可以在应用程序生命周期的后期创建新stage对象，以防应用程序需要打开更多窗口。



* 创建舞台

您可以像创建任何其他Java 对象一样创建 JavaFXStage对象: 使用new命令和Stage构造函数。下面是创建JavaFXStage对象的示例。

```
Stage stage = new stage(); 
```



* 展示舞台

简单地创建 JavaFxStage对象不会显示它。为了使Stage 可见，您必须调用它的show()或showAndWait()方法.这是显示JavaFX 的示例Stage:

```java
Stage stage = new stage(); 
stage.show();
```



* 在舞台上设置场景

为了在JavaFX 巾显示任何内容Stage,您必须Scene 在Stage.的内容Scene将在显示 Stage时显示任Stage中.Scene 这是在JavaFX 上设置a的示例Stage:

```java
VBox vbox = new VBox();

VBox VBox = new VBox(new Label("A JavaFx Label"));
Scene scene = new Scene(vBox);
stage stage = new stage();stage.setscene(scene) ;
```



* 舞台名称

您可以通过该方法 设置 JavaFXStage标题。Stage setTitle(标题Stage显示在Stage窗口的标题栏中。这是设置JavaFX 标题的示例Stage:

```java
stage.setTitle("JavaFx Stage Window Title");
```



* 舞台位置

Stage您可以通过其setX0和setY0 方法设置 JavaFX 的位置(X,Y。setX0和setY0方法设置由表示的窗口左角的位置stage。下面是设置 JavaFxStage对象的X和Y 位置的示例:

```
stage stage = new stage();

stage.setX(50);
stage.setY(50);
```

Stage请注意，如果设置X和Y 位置，可能还需要设置宽度和高度，否则舞台窗口可能会变得非常小。有关设置a 的宽度和高度的更多信息，请参阅下一节Stage。



* 舞台宽度和高度

Stage您可以通过其setWidth0和setHeight0 方法设置JvaFX 的宽度和。这是设置 JavaFX 的宽度和高度的示例Stage:

```
Stage stage = new stage();

stage.setwidth(600);
stage.setHeight(300);
```



* 舞台风格

stage您可以通过其initStyle0方法 设置JavaFX 的样式。您可以选择一组不同的样式

* DECORATED

* UNDECORATED

* TRANSPARENT

* UNIFIED

* UTILITY

  装饰Stage是带有操作系统装饰 (标题栏和最小化/最大化/关闭按钮)和白色背景的标准窗口未装饰Stage是没有操作系统装饰的标准窗口，但仍具有白色背景透明Stage是具有透明背景的未装饰窗口。





设置页面:

```java
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("InsertExam.fxml"));
        Scene scene = new Scene(root, 700, 600);//建立一个与初始化界面一样大的界面
//        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.setTitle("添加考试表");
        primaryStage.setScene(scene);
        primaryStage.show();//展示
    }
```



实现页面切换:

```java
Platform.runLater(() -> {
    //获取按钮所在窗口
    Stage primaryStage = (Stage) Return.getScene().getWindow();
    //当前窗口隐藏
    primaryStage.hide();
    //加载目标窗口
    try{	/*目标窗口的控制*/
        new ExamController().start(primaryStage);
    }catch (Exception e){
        e.printStackTrace();
    }
});
```



新建界面:

```java
Login.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent actionEvent) {
        URL url = getClass().getResource("Login.fxml");
        Parent root = null;
        try{
            root = FXMLLoader.load(url);
        }catch (IOException e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initStyle(StageStyle.DECORATED);//可以移动
        stage.setScene(scene);
        stage.show();//展示
    }
});
```